// Test your GSL installation

/** \file test_gsl.cpp
 * \brief Unit test for checking the GSL library
 * \author Ramsharan Rangarajan
 * \date July 23, 2020.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */


#include <gsl/gsl_sf_bessel.h>
#include <cassert>

int main ()
{
  assert( gsl_sf_bessel_J0(5.0)+0.177597 < 1.e-3 );
}
