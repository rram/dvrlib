
/** \file test_openmp.cpp
 * \brief Unit test for checking usage with OpenMP
 * \author Ramsharan Rangarajan
 * \date July 23, 2020.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 *
 *   OpenMP Example - Hello World - C/C++ Version
 *   In this simple example, the master thread forks a parallel region.
 *   All threads in the team obtain their unique thread number and print it.
 *   The master thread only prints the total number of threads.  Two OpenMP
 *   library routines are used to obtain the number of threads and each
 *   thread's number.
 * Adapted from: https://computing.llnl.gov/tutorials/openMP/samples/C/omp_hello.c
 *
 */

#include <omp.h>
#include <iostream>

int main ()
{
  int nthreads, tid;

  // Fork a team of threads giving them their own copies of variables
#pragma omp parallel private(nthreads, tid)
  {
    // Obtain thread number 
    tid = omp_get_thread_num();
    std::cout<<"\nHello World from thread = "<<tid<<"\n";

    // Only master thread does this
    if (tid == 0) 
      { nthreads = omp_get_num_threads();
	std::cout<<"\nNumber of threads = "<<nthreads<<"\n"; }
  }  // All threads join master thread and disband
}
