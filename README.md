# About DVRlib 

<!--![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/rram/dvrlib/joss) -->
<!--[![Bitbucket issues](https://img.shields.io/bitbucket/issues/rram/dvrlib.svg)](https://Bitbucket.org/rram/dvrlib/issues/)-->
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![DOI](https://joss.theoj.org/papers/10.21105/joss.02372/status.svg)](https://doi.org/10.21105/joss.02372)

<!--## DVRlib: Directional vertex relaxation for geometric mesh optimization-->
*DVRlib* is a C++ library providing an implementation of the *directional vertex relaxation* algorithm
(dvr) introduced in [Rangarajan R and Lew A. Provably robust
directional vertex relaxation for geometric mesh optimization. SIAM
Journal on Scientific Computing, 39(6):A2438–A2471,
2017](https://epubs.siam.org/doi/10.1137/16M1089101) to improve qualities of triangle and tetrahedral meshes.

**Authors** <br />
__[Ramsharan Rangarajan](https://mecheng.iisc.ac.in/~rram)__,  Indian
Institute of Science Bangalore, India.  <br /> 
   __[Adrian Lew](https://stanford.edu/~lewa)__, Stanford University,
USA. 

## [Library documentation](https://rram.bitbucket.io/dvr-docs/html/index.html)

## Why a new mesh improvement library? 
Mesh improvement is an essential part of mesh generation.
Well-shaped elements are crucial in scientific computations using
unstructured meshes based on a variety of considerations (conditioning of systems of equations,
choice of time step, accuracy of numerical approximation).
Notable features of dvr that make it useful in a broad range of scientific computing applications
using unstructured meshes include the following:

- dvr is a provably robust algorithm. The quality of a mesh,
  defined in a specific but practically meaningful sense,  is guaranteed to improve with each computation in the algorithm.
  In particular, the poorest element quality in a mesh is guaranteed to be monotonically non-decreasing with each iteration
  of the algorithm. A precise statement of the guarantees underlying dvr
  is provided by Theorem 4.2 of the [paper](https://epubs.siam.org/doi/10.1137/16M1089101).
  For this reason, dvr stands out among a host of ad hoc techniques which often provide no guarantee of mesh improvement. 

- dvr is a connectivity-preserving algorithm. Mesh improvement is achieved by altering coordinates of a
 specified list of vertices, while
 element connectivities remain unchanged. It is therefore well suited for use
 in applications where preserving sparsity of data structures is desirable
 (e.g., stiffness and mass matrices in finite element calculations). 

- dvr is computationally inexpensive. Each iteration of the algorithm involves updating a specified set of vertices by resolving one-dimensional max-min problems.
 Each vertex perturbation is a local operation that depends only on the qualities of elements around it.
 In particular, mesh improvement does not require resolving large systems of equations.

- dvr is a naturally parallelizable algorithm.

- dvr is well-suited for simulating a challenging class of moving boundary problems.
 Finite element computations for problems in which the domain can change
 frequently (e.g., at each time-step in a fluid-structure interaction problem, or at each iteration in a shape optimization problem)
 require robust techniques for mesh improvement. User-assisted mesh improvement is impractical in such simulations,
 and deterioration in mesh quality can have catastrophic consequences, often leading to termination of the simulation.
 With guarantees for mesh improvement, and by retaining mesh connectivity unaltered, dvr is a useful tool
 in such simulations. 

- dvr is extendable to a broad range of mesh types and  element quality metrics, both in two- and three-dimensions. 


## What DVRlib does 

- DVRlib provides a robust and efficient functionalities for improving element qualities in triangle and tetrahedral meshes by perturbing locations of a specified set of vertices along a prescribed set of directions.

- Despite relying solely on geometric mesh optimization, DVRlib can provide significant improvement in mesh quality, see the examples in Figures 1 and 2. 

- DVRlib achieves mesh improvement by optimally and iteratively perturbing locations of a specified set of vertices along prescribed directions.
- Element connectivities are retained unaltered during mesh improvement. In particular, the library does not rely on any topology-altering operations such as edge/face swapping and vertex insertion/removal.
- The DVR algorithm implemented by DVRlib is provably robust. The mesh quality, defined in a specific sense, is guaranteed to improve monotonically with each vertex perturbation at each iteration.
- DVRlib provides vertex-level and mesh-level optimization routines.
- DVRlib provides a lock-free thread-parallel mesh optimization routine.
- DVRlib provides functionalities to relax vertices constrained to lie on curvilinear manifolds.
- DVRlib provides flexibility in the choice of mesh data structures. All classes and functionalities requiring access to meshes are templated by the mesh type.

<br />
<br />
<img src="docs/imgs/spot-composite.jpg" alt="image" width="750" height="auto">

<br />
The image above shows the result of a representative result using DVRlib to improve a tetrahedral mesh generated by TetGen.
The [paper](https://epubs.siam.org/doi/10.1137/16M1089101) and the [documentation pages](https://rram.bitbucket.io/dvr-docs/html/index.html) contain details of a large set of examples demonstrating improvement in mesh quality achieved with DVRlib for triangle/tetrahedral
 meshes generated by a number of commonly used mesh generation libraries, including
 [Gmsh](http://gmsh.info), [TetGen](http://wias-berlin.de/software/index.jsp?id=TetGen&lang=1/), [Triangle](https://www.cs.cmu.edu/~quake/triangle.html), [CGAL](https://doc.cgal.org/latest/Mesh_3/index.html) and [Hypermesh](https://www.altair.com/hypermesh/).

## What DVRlib does not do
- DVRlib is *not* a mesh generator, but can be coupled to one. 
- DVRlib is *not* a tool for adaptive mesh refinement, but can improve an adaptively refined mesh. 
- DVRlib is *not* a mesh untangling tool, but can improve an untangled mesh. 
- DVRlib is *not* a mesh simplification tool.

 A large number of software libraries provide a variety of tools for generating, improving
 and manipulating unstructured meshes. In this context, it is
 important to note that DVRlib is neither intended to be, nor is
 it a comprehensive mesh improvement toolbox.  DVRlib performs a
 very specific type of mesh improvement, 
 commonly termed as geometric mesh optimization, to improve mesh
 quality. 
 It can be coupled with a broader suite of operations, including topological operations such as face swapping and vertex insertion/removal.


## Intended applications
- DVRlib is well-suited for integration with unstructured mesh generators to improve element qualities through post-processing operations.
- DVRlib is designed to help simulate moving boundary problems, which require computing numerical solutions over domains that evolve with time (e.g., fluid-structure interaction) or domains that change with each iteration (e.g., shape optimization).

## Projects using DVRlib
- Simulating mechanical contact as a shape optimization problem:<br>
[A Sharma, R Rangarajan. A shape optimization approach for simulating contact of elastic membranes with rigid obstacles.
International Journal for Numerical Methods in Engineering 117 (4), 371-404, 2019.](https://onlinelibrary.wiley.com/doi/full/10.1002/nme.5960)

- Meshing evolving 3D domains immersed in nonconforming tetrahedral meshes:<br>
[R Rangarajan, H Kabaria, A Lew. An algorithm for triangulating smooth three‐dimensional domains immersed in universal meshes. International Journal for Numerical Methods in Engineering 117 (1), 84-117, 2019.](https://onlinelibrary.wiley.com/doi/abs/10.1002/nme.5949)

## More about DVR
The [documentation
pages](https://rram.bitbucket.io/dvr-docs/html/index.html) provide a
concise summary of the algorithm.

- For a detailed description and analysis of the DVR algorithm: <br />
[Rangarajan R and Lew A. Provably robust directional vertex relaxation for geometric mesh optimization. SIAM Journal on Scientific Computing, 39(6):A2438–A2471, 2017](https://epubs.siam.org/doi/10.1137/16M1089101).

- For a description and analysis of the algorithm used to resolve non
smooth max-min problems defining vertex perturbations in DVRlib: <br />
[Rangarajan R. On the resolution of certain discrete univariate max–min problems. Computational Optimization and Applications, 68(1):163–192, 2017.](https://link.springer.com/article/10.1007/s10589-017-9903-z)


## Acknowledge
If you use DVRlib in your research, please <a href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=DVRlib" target="_top">let us know</a> and acknowledge its use as 

```
@article{rangarajan2020dvrlib,
  title={DVRlib: A C++ library for geometric mesh improvement using Directional Vertex Relaxation},
  author={Rangarajan, Ramsharan and Lew, Adrian},
  journal={Journal of Open Source Software},
  volume={5},
  number={52},
  pages={2372},
  year={2020}
}
```

Bibtex entries of research articles on the DVR algorithm is [here](https://rram.bitbucket.io/dvr-docs/dvr-bibtex.bib).

For supporting the research on DVR and the development of DVRlib, we thank:
 
 - The Defence Research and Development Organization's (DRDO, India) research grant <tt>JATP/P-VIII/P-2019/164</tt> through the Joint Advanced Technology Program with the Indian Institute of Science Bangalore. 
 
 - Science and Engineering Research Board's (SERB, India) early career award <tt>ECR/2017/000346</tt> 
 
 - Varshini Subash for assistance with code profiling and testing as part of her undergraduate thesis.


## Contributing to DVRlib
Please <a href="mailto:rram@iisc.ac.in,lewa@stanford.edu?Subject=DVRlib"
 target="_top">get in touch</a> if you would like to contribute to the
development of DVRlib.

Found a bug? Please raise an issue on [bitbucket](https://bitbucket.org/rram/dvrlib/src/) page of the library.

## License

MIT License

Copyright (c) [2020] [Ramsharan Rangarajan]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
