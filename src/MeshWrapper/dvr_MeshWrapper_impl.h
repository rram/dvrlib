
/** \file dvr_MeshWrapper_impl.h
 * \brief Implementation of class dvr::MeshWrapper
 * \author Ramsharan Rangarajan
 * \date Aug 08, 2023.
 */

#pragma once

#include <set>

namespace dvr
{
  // Setup 1-rings and vertex valencies
  template<typename MeshType>
    void MeshWrapper<MeshType>::Setup()
    {
      const int spatial_dimension = MD.spatial_dimension();
      dvr_assert(spatial_dimension==2 || spatial_dimension==3);
      const int nodes_element = spatial_dimension+1;
      const int elements = MD.n_elements();
            
      // node count
      nodes = MD.n_nodes();
      
      // Guesses for sizing vectors
      const int valencyGuess = (spatial_dimension==2) ? 12 : 30; // initial guesses for reserving memory
      Elm1Rings.resize(nodes);
      for(int n=0; n<nodes; ++n)
	{
	  Elm1Rings[n].clear(); 
	  Elm1Rings[n].reserve(valencyGuess);
	}
      
      // vertex 1-rings as sets
      std::vector<std::set<int>> Vert1RingSets(nodes, std::set<int>{});
    
      // Loop over connectivites and update 1-ring information
      for(int e=0; e<elements; ++e)
	{
	  const auto* elmconn = MD.connectivity(e);
	  for(int a=0; a<nodes_element; ++a)
	    {
	      const int& vert = elmconn[a];
	      Elm1Rings[vert].push_back(e);
	      for(int k=1; k<nodes_element; ++k)
		{
		  const int& nbvert = elmconn[(a+k)%nodes_element];
		  Vert1RingSets[vert].insert( nbvert );
		}
	    }
	}

      // Shink sizes    
      for(auto& it:Elm1Rings) it.shrink_to_fit();

      // Convert vertex 1-rings from sets to vectors
      Vert1Rings.resize(nodes);
      for(int i=0; i<nodes; ++i)
	Vert1Rings[i] = std::vector<int>(Vert1RingSets[i].begin(), Vert1RingSets[i].end());
    
      // Maximum vertex valency
      nMaxVertValency = 0;
      for(auto& it:Vert1Rings)
	{
	  const int nverts = static_cast<int>(it.size());
	  if(nMaxVertValency<nverts) nMaxVertValency = nverts;
	}

      // Maximum element valency
      nMaxElmValency = 0;
      for(auto& it:Elm1Rings)
	{
	  const int nElms = static_cast<int>(it.size());
	  if(nMaxElmValency<nElms) nMaxElmValency = nElms;
	}
    
      // done
      return;
    }
}
