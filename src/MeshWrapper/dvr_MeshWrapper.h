
/** \file dvr_MeshWrapper.h
 * \brief Class to compute the 1-ring information for a mesh
 * \author Ramsharan Rangarajan
 * \date Aug 08, 2023.
 */

#pragma once

#include <vector>
#include <dvr_MeshTraits.h>
#include <dvr_assert.h>

namespace dvr
{
  template<typename MeshType>
  class MeshWrapper
    {
    public:
      //! Constructor
      //! \param[in] md Mesh object, referred to
      inline MeshWrapper(MeshType& md)
	:MD(md)
      {
	static_assert(CheckMeshTraits<MeshType>(), "MeshType failed to satisfy requirements");
	Setup();
      }

      //! Disable copy
      MeshWrapper(const MeshWrapper<MeshType>&) = delete;
      
      //! Destructor
      inline virtual ~MeshWrapper() {}

      //! Access the mesh
      inline MeshType& GetMesh() 
      { return MD; }

      //! Const access to the mesh
      inline const MeshType& GetConstMesh() const
      { return MD; }
      
      //! Returns the list of vertices in the 1-ring of a vertex
      //! \param[in] vert_index Vertex number. Should be a vertex in Ir
      //! \return Reference to a vector containing the list of vertices in the 1-ring
      inline const std::vector<int>& Get1RingVertices(const int vert_index) const
      {
	assert(vert_index>=0 && vert_index<nodes);
	return Vert1Rings[vert_index];
      }

      //! Returns the list of elements in the 1-ring of a vertex
      //! \param[in] vert_index Vertex number. Should be a vertex in Ir
      //! \return Reference to a vector containing the list of elements in the 1-ring
      inline const std::vector<int>& Get1RingElements(const int vert_index) const
      {
	assert(vert_index>=0 && vert_index<nodes);
	return Elm1Rings[vert_index];
      }

      //! Returns the maximum vertex valency
      inline int GetMaxVertexValency() const
      { return nMaxVertValency; }

      //! Returns the maximum element valency
      inline int GetMaxElementValency() const
      { return nMaxElmValency; }

    private:
      void Setup();
      MeshType                      &MD;             //!< Reference to the mesh object
      int                           nodes;           //!< Number of nodes in the mesh
      std::vector<std::vector<int>> Vert1Rings;      //! Vert1Rings[indx] = List of vertices in the 1-ring of vertex indx
      std::vector<std::vector<int>> Elm1Rings;       //! Elm1Rings[indx] = List of elements in the 1-ring of element indx
      int                           nMaxVertValency; //! maximum vertex valency in a 1-ring
      int                           nMaxElmValency;  //! maximum element valency in a 1-ring
    };
}

#include <dvr_MeshWrapper_impl.h>
