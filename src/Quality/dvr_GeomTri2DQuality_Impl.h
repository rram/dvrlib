
/** \file dvr_GeomTri2DQuality_Impl.h
 * \brief Implementation of the class dvr::GeomTri2DQuality.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#pragma once

#include <cmath>
#include <iostream>
#include <dvr_assert.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
   //Access to local data structures
template<typename MeshType>
  detail::Geom2Ddata& GeomTri2DQuality<MeshType>::GetLocalData() const
  {
    int ThrNum = 0;
#ifdef _OPENMP
    ThrNum = omp_get_thread_num();
#endif
    assert(ThrNum<nMaxThreads && "GeomTri2DQuality::GetLocalData- Unexpected thread number.");
    return ThrMyData[ThrNum];
  }
      
  // Main functionality: compute extreme points of the quality of an element by perturbing its local
  // node along a prescribe direction
  template<typename MeshType>
    void GeomTri2DQuality<MeshType>::Extremize(const int elm, const int locnode, 
					       const double* dir,
					       double* lambda, double* qvals,
					        int& nlambda) const
    {
      // Get local data structure
      auto& data = GetLocalData();
      auto& ACoef = data.ACoef;
      auto& SCoef = data.L2Coef;
      auto& coefs = data.coefs;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Coordinates of nodes
      const auto* elmconn = MD.connectivity(elm);
      const double* Coord[] = {MD.coordinates(elmconn[0]),
			       MD.coordinates(elmconn[1]),
			       MD.coordinates(elmconn[2])};

      // Coefficients of area = A0 + lambda A1
      AreaCoef(Coord, locnode, dir, ACoef);

      // Coefficients of perimeter^2 = S0 + lambda S1 + lambda^2 S2
      EdgeLen2Coef(Coord, locnode, dir, SCoef);

      // Lengthscale
      lscale = sqrt(SCoef[0]/3.);

      // Solve for parameter 't', where lambda = lscale*t, lscale is O(h).
      // This keeps the polynomial better conditioned since t is O(1)
      ACoef[1] *= lscale;
      SCoef[1] *= lscale; SCoef[2] *= lscale*lscale;

      // Normalize by the area at t = 0
      norm = ACoef[0];
      ACoef[0] /= norm; ACoef[1] /= norm;
      SCoef[0] /= norm; SCoef[1] /= norm; SCoef[2] /= norm;
      
      // Extremize Area/perimeter^2: yields a quadratic equation in 't'
      coefs[0] = ACoef[0]*SCoef[1]-ACoef[1]*SCoef[0];
      coefs[1] = 2.*ACoef[0]*SCoef[2];
      coefs[2] = ACoef[1]*SCoef[2];
      
      // Solver
      data.PolySolver.Solve(2, coefs, EPS, lambda, nlambda);
      dvr_assert(nlambda>0 &&
	     "GeomTri2DQuality::Extremize()- Could not find real roots of quadratic polynomial.\n");

      // Rescale roots, and compute qualities
      for( int i=0; i<2; ++i)
	{
	  qvals[i] = Qnorm*
	    (ACoef[0]+ACoef[1]*lambda[i])/
	    (SCoef[0]+SCoef[1]*lambda[i]+SCoef[2]*lambda[i]*lambda[i]);
	  lambda[i] *= lscale;
	}

      return;
    }
  

  // Main functionality: compute the intersections of a pair of quality curves
  // while perturbing a common vertex
  template<typename MeshType>
    void GeomTri2DQuality<MeshType>::Intersect(const int elm1, const int optNode1, 
					       const int elm2, const int optNode2, 
					       const double* dir, double* lambda, double* qvals,
					        int& nlambda, 
					       void* usrparams) const
    {
      // Cast the user parameters
      detail::ParamInterval* param =
	(usrparams==nullptr) ? nullptr : static_cast<detail::ParamInterval*>(usrparams);
      
      // Switch off this flag if returning without inverting
      if(param!=nullptr) param->flag = true;

      // Access local data
      auto& data = GetLocalData();
      auto& ACoefs = data.ACoefs;
      auto& SCoefs = data.L2Coefs;
      auto& coefs = data.coefs;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Coordinates of nodes in the two elements
      const auto* conn1 = MD.connectivity(elm1);
      const double* Coord1[] = {MD.coordinates(conn1[0]), MD.coordinates(conn1[1]), MD.coordinates(conn1[2])};
      const auto* conn2 = MD.connectivity(elm2);
      const double* Coord2[] = {MD.coordinates(conn2[0]), MD.coordinates(conn2[1]),MD.coordinates(conn2[2])};
      
      // Area and perimeters^2 of the two triangles
      AreaCoef(Coord1, optNode1, dir, ACoefs[0]);
      EdgeLen2Coef(Coord1, optNode1, dir, SCoefs[0]);
      AreaCoef(Coord2, optNode2, dir, ACoefs[1]);
      EdgeLen2Coef(Coord2, optNode2, dir, SCoefs[1]);

      // Length scale O(h)
      lscale = 0.5*(sqrt(SCoefs[0][0]/3.)+sqrt(SCoefs[1][0]/3.));

      // Introduce parameter 't', where lambda = lscale*t, lscale is O(h).
      // This makes the polynomials better conditioned as t remains O(1).
      ACoefs[0][1] *= lscale; ACoefs[1][1] *= lscale;
      SCoefs[0][1] *= lscale; SCoefs[1][1] *= lscale;
      SCoefs[0][2] *= lscale*lscale; SCoefs[1][2] *= lscale*lscale;

      // Normalize by the area term
      norm = ACoefs[0][0];
      ACoefs[0][0] /= norm; ACoefs[0][1] /= norm;
      SCoefs[0][0] /= norm; SCoefs[0][1] /= norm; SCoefs[0][2] /= norm;
      norm = ACoefs[1][1];
      ACoefs[1][0] /= norm; ACoefs[1][1] /= norm;
      SCoefs[1][0] /= norm; SCoefs[1][1] /= norm; SCoefs[1][2] /= norm;
      
      // Solve for equality of the qualities
      coefs[0] = coefs[1] = coefs[2] = coefs[3] = 0.;
      double sign = +1.;
      for( int i=0; i<2; ++i)
	{
	  auto& A = ACoefs[i];
	  auto& S = SCoefs[(i+1)%2];
	  coefs[0] += sign*(A[0]*S[0]);
	  coefs[1] += sign*(A[1]*S[0]+A[0]*S[1]);
	  coefs[2] += sign*(A[1]*S[1]+A[0]*S[2]);
	  coefs[3] += sign*(A[1]*S[2]);
	  sign *= -1.;
	}

      // Solve
      nlambda = 0;
      if(std::abs(coefs[3])>EPS) // Cubic polynomial
	{
	  // Check if real roots exist
	  if(param!=nullptr)
	    if(false==data.RootCounter.template QueryRoot<3>(param->tinterval[0]/lscale, param->tinterval[1]/lscale, coefs, EPS, 4))
	      { nlambda = 0; param->flag = false; return; }
	  
	  // Otherwise, solve
	  data.PolySolver.Solve(3, coefs, EPS, lambda, nlambda);
	}
      else if(std::abs(coefs[2])>EPS) // Quadratic polynomial
	data.PolySolver.Solve(2, coefs, EPS, lambda, nlambda);
      else if(std::abs(coefs[1])>EPS) // Linear polynomial
	{ lambda[0] = -coefs[0]/coefs[1]; nlambda = 1; }
      else if(std::abs(coefs[0])>EPS) // The two quality curves should be identical
	{
	  std::cout<<"\n"<<coefs[3]<<", "<<coefs[2]<<", "<<coefs[1]<<", "<<coefs[0]; std::fflush( stdout );
	  //assert(std::abs(coefs[0])<EPS && "\ndvr::GeomTri2DQuality::Intersect()- Unexpected polynomial encountered.\n");
	}

      // Rescale computed roots and compute qualities
      for( int i=0; i<nlambda; ++i)
	{
	  // Use elm1
	  qvals[i] = Qnorm*
	    (ACoefs[0][0] + ACoefs[0][1]*lambda[i])/
	    (SCoefs[0][0] + SCoefs[0][1]*lambda[i] + SCoefs[0][2]*lambda[i]*lambda[i]);
	  lambda[i] *= lscale;
	}

      return;
    }
  
  // Main functionality: compute the derivative of the quality of
  // an element when a vertex is perturbed
  template<typename MeshType> double
    GeomTri2DQuality<MeshType>::dCompute(const int elm, const int locnode,
					 const double* rdir, const double lambda) const
    {
      // Access local data
      auto& data = GetLocalData();
      auto& ACoef = data.ACoef;
      auto& SCoef = data.L2Coef;
      auto& Area = data.Area;
      auto& Len2 = data.Len2;

      // Nodal coordinates
      const auto* elmconn = MD.connectivity(elm);
      const double* Coord[] = { MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
				MD.coordinates(elmconn[2]) };

      // Area and perimeter^2 expressions
      AreaCoef(Coord, locnode, rdir, ACoef);
      EdgeLen2Coef(Coord, locnode, rdir, SCoef);

      // Area and perimeter^2 at given value of lambda
      Area = ACoef[0] + ACoef[1]*lambda;
      Len2 = SCoef[0] + SCoef[1]*lambda + SCoef[2]*lambda*lambda;

      // Evaluate derivative
      return Qnorm*(ACoef[1]/Len2 - Area*(SCoef[1]+2.*SCoef[2]*lambda)/(Len2*Len2));
    }


  // Main functionality: compute inverse of the quality metric
  // along a prescribed direction
  template<typename MeshType> void GeomTri2DQuality<MeshType>::
    Invert(const int elm, const int locnode,
	   const double* rdir, const double qval,
	   double* lambda,  int& nlambda,
	   void* usrparams) const
    {
      dvr_assert(qval>0.);
      
      // We are always solving for the root here
      if(usrparams!=nullptr)
	static_cast<detail::ParamInterval*>(usrparams)->flag = true;
      
      // Local data structures
      auto& data = GetLocalData();
      auto& ACoef = data.ACoef;
      auto& SCoef = data.L2Coef;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Coordinates of nodes
      const auto* elmconn = MD.connectivity(elm);
      const double *Coord[] = {MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
			       MD.coordinates(elmconn[2])};

      // Area A = A0 + lambda A1
      AreaCoef(Coord, locnode, rdir, ACoef);

      // If given quality is small enough, a linear solution is enough
      if(std::abs(qval)<EPS)
	{
	  dvr_assert(std::abs(ACoef[1])>EPS && "GeomTri2DQuality::Invert()- Could not invert quality metric.");
	  lambda[0] = -ACoef[0]/ACoef[1];
	  nlambda = 1; return;
	}
      
      // Perimeter^2 S = S0 + S1 lambda + S2 lambda^2
      EdgeLen2Coef(Coord, locnode, rdir, SCoef);

      // Introduce parameter 't', where lambda = lscale*t, lscale is O(h).
      // This ensures that 't' remains O(1) and the polynomial is well conditioned.
      lscale = sqrt(SCoef[0]/3.);
      ACoef[1] *= lscale;
      SCoef[1] *= lscale; SCoef[2] *= lscale*lscale;

      // Normalize.
      norm = ACoef[0];
      ACoef[0] /= norm; ACoef[1] /= norm;
      SCoef[0] /= norm; SCoef[1] /= norm; SCoef[2] /= norm;
      
      // Solve (A0 + A1 t)/(S0 + S1 t + S2 t^2) = qval/Qnorm
      SCoef[0] -= ACoef[0]*Qnorm/qval;
      SCoef[1] -= ACoef[1]*Qnorm/qval;
      
      // Solve
      data.PolySolver.Solve(2, SCoef, EPS, lambda, nlambda);

      // Rescale roots
      for( int i=0; i<nlambda; ++i)
	lambda[i] *= lscale;
      
      return;
    }
  
  // Helper function: Express area as a function of perturbation coordinate
  // A(lambda) = coef_0 + coef_1 lambda.
  template<typename MeshType> void
    GeomTri2DQuality<MeshType>::AreaCoef(const double* Coord[3], const int locnode,
					 const double* dir, double* coef) const
    {
      // In triangle ABC, make vertex C the perturbed vertex.
      auto& A = Coord[(locnode+1)%3];
      auto& B = Coord[(locnode+2)%3];
      auto& C = Coord[locnode];

      // Area = 0.5 (B-A) x (C-B)
      coef[0] = 0.5*( (B[0]-A[0])*(C[1]-B[1]) - (B[1]-A[1])*(C[0]-B[0]) );
      coef[1] = -0.5*( dir[0]*(B[1]-A[1]) - dir[1]*(B[0]-A[0]) );
      return;
    }
 
  // Helper function: Express perimeter^2 are a function of perturbation coordinate.
  template<typename MeshType> void
    GeomTri2DQuality<MeshType>::EdgeLen2Coef(const double* Coord[3], const int locnode,
					     const double* dir, double* coef) const
    {
      // In triangle ABC, make vertex C the perturbed vertex.
      auto& A = Coord[(locnode+1)%3];
      auto& B = Coord[(locnode+2)%3];
      auto& C = Coord[locnode];
    
      // Perimeter-squared
      coef[0] = coef[1] = coef[2] = 0.;
      for( int i=0; i<2; ++i)
	{
	  coef[0] += (A[i]-B[i])*(A[i]-B[i]) + (A[i]-C[i])*(A[i]-C[i]) + (C[i]-B[i])*(C[i]-B[i]);
	  coef[1] += 2.*dir[i]*(2.*C[i]-A[i]-B[i]);
	  coef[2] += 2.*dir[i]*dir[i];
	}
      return;
    }
}


