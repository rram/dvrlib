
/** \file dvr_ParamInterval.h
 * \brief Definition of the helper struct dvr::detail::ParamInterval.
 * \author Ramsharan Rangarajan
 * \date March 22, 2020.
 */

#pragma once

namespace dvr
{
  namespace detail
  {
    //! \brief Helper struct encapsulating information about an interval
    struct ParamInterval
    {
      double tinterval[2]; //!< Left and right endpoints of an interval
      bool flag; //!< Boolean variable, for appropriate use.
    }; 
  }
}

