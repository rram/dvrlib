
/** \file dvr_GeomTet3DQuality_Impl.h
 * \brief Implementation of the class dvr::GeomTet3DQuality.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <cmath>
#include <iostream>
#include <dvr_assert.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{

  // Access to local data structure
  template<typename MeshType>
    detail::Geom3Ddata& GeomTet3DQuality<MeshType>::GetLocalData() const
    {
      int ThrNum = 0;
#ifdef _OPENMP
      ThrNum = omp_get_thread_num();
#endif	  
      assert(ThrNum<nMaxThreads &&
	     "GeomTet3DQuality::GetLocalData()- Unexpected thread number.");
      return ThrMyData[ThrNum];
    }

      
  // Main functionality: compute the quality of a tetrahedron
  template<typename MeshType> double
    GeomTet3DQuality<MeshType>::ComputeTetQuality(const double* A, const double* B,
						  const double* C, const double* D) const
    {
      // Get local data
      auto& data = GetLocalData();
      auto& AD = data.AD;
      auto& BD = data.BD;
      auto& CD = data.CD;
      auto& Vol = data.Vol;
      auto& Len2 = data.Len2;
	
      // Shift the origin to the last vertex
      for(int k=0; k<3; ++k)
	{
	  AD[k] = A[k]-D[k];
	  BD[k] = B[k]-D[k];
	  CD[k] = C[k]-D[k];
	}

      // Compute volume
      Vol = -(
	      AD[0]*(BD[1]*CD[2] - BD[2]*CD[1]) +
	      AD[1]*(BD[2]*CD[0] - BD[0]*CD[2]) +
	      AD[2]*(BD[0]*CD[1] - BD[1]*CD[0]) )*ONEbySIX;

      // Compute the squared perimeter
      Len2 = 0.;
      for(int i=0; i<3; ++i)
	Len2 +=
	  AD[i]*AD[i] + BD[i]*BD[i] + CD[i]*CD[i] +
	  (AD[i]-BD[i])*(AD[i]-BD[i]) +
	  (AD[i]-CD[i])*(AD[i]-CD[i]) +
	  (BD[i]-CD[i])*(BD[i]-CD[i]);
      
      return Qnorm*Vol/(Len2*std::sqrt(Len2));
    }


  // Main functionality: compute the quality of an element when a vertice is pertubed
  template<typename MeshType> double
    GeomTet3DQuality<MeshType>::Compute(const int elm, const int locnode,
					const double* rdir, const double lambda) const
    {
      const auto* elmconn = MD.connectivity(elm);
      const double* coord[] = {MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
			       MD.coordinates(elmconn[2]), MD.coordinates(elmconn[3])};
      
      const auto* A = MD.coordinates(elmconn[locnode]);
      double X[3] = {A[0]+lambda*rdir[0], A[1]+lambda*rdir[1], A[2]+lambda*rdir[2]};

      switch(locnode)
	{
	case 0: return ComputeTetQuality(X, coord[1], coord[2], coord[3]);

	case 1: return ComputeTetQuality(coord[0], X, coord[2], coord[3]);
	  
	case 2: return ComputeTetQuality(coord[0], coord[1], X, coord[3]);
	  
	case 3: return ComputeTetQuality(coord[0], coord[1], coord[2], X);
	  
	default: assert(false && "dvr::GeomTet3DQuality::Compute- Unexpected local node number");
	}
      
      return 0.;
    }


  // Main functionality: compute the derivative of the quality of an element when a vertex is perturbed
  template<typename MeshType> double
    GeomTet3DQuality<MeshType>::dCompute(const int elm, const int locnode,
					 const double* rdir, const double lambda) const
    {
      // Access local data
      auto& data = GetLocalData();
      auto& VCoef = data.VCoef;
      auto& SCoef = data.L2Coef;
      
      // Coordinates of this element
      const auto* elmconn = MD.connectivity(elm);
      const double* coord[] = {MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
			       MD.coordinates(elmconn[2]), MD.coordinates(elmconn[3])};

      // Volume and perimeter^2
      VolumeCoef(coord, locnode, rdir, VCoef);
      data.Vol = VCoef[0] + lambda*VCoef[1];
      data.dVol = VCoef[1];
      
      EdgeLen2Coef(coord, locnode, rdir, SCoef);
      data.Len2 = SCoef[0] + SCoef[1]*lambda + SCoef[2]*lambda*lambda;
      data.dLen2 = SCoef[1] + 2.*SCoef[2]*lambda;
      return Qnorm*(data.dVol*data.Len2 - 1.5*data.Vol*data.dLen2)/std::pow(data.Len2,2.5);
    }
  
  // Get coefficients for sum of squares of edge lengths L2(lambda) = a + b lambda + c lambda^2
  template<typename MeshType> void
    GeomTet3DQuality<MeshType>::EdgeLen2Coef(const double* coord[4],
					     const int locnode,
					     const double* dir,
					     double* coef) const
    {
      // Cyclic ordering of indices, starting from locnode
      const int b = (locnode+1)%4;
      const int c = (locnode+2)%4;
      const int d = (locnode+3)%4;
      
      // Compute coefficients
      coef[0] = coef[1] = coef[2] = 0.;
      for(int k=0; k<3; ++k)
	{
	  // constant term: perimeter^2 
	  coef[0] +=
	    (coord[0][k]-coord[1][k])*(coord[0][k]-coord[1][k]) + // AB.AB
	    (coord[0][k]-coord[2][k])*(coord[0][k]-coord[2][k]) + // AC.AC
	    (coord[0][k]-coord[3][k])*(coord[0][k]-coord[3][k]) + // AD.AD
	    (coord[1][k]-coord[2][k])*(coord[1][k]-coord[2][k]) + // BC.BC
	    (coord[1][k]-coord[3][k])*(coord[1][k]-coord[3][k]) + // BD.BD
	    (coord[2][k]-coord[3][k])*(coord[2][k]-coord[3][k]);  // CD.CD

	  // linear term:  2 dir.(AB+AC+AD), where A = locnode
	  coef[1] += 2.*dir[k]*(3.*coord[locnode][k]-coord[b][k]-coord[c][k]-coord[d][k]);

	  // quadratic term: 3 dir.dir
	  coef[2] += 3.*dir[k]*dir[k];
	}
      return;
    }

  
  // Get coefficients for volume of tet: V(lambda) = a + b \lambda
  template<typename MeshType> void GeomTet3DQuality<MeshType>::
    VolumeCoef(const double* coord[4], const int locnode,
	       const double* dir, double* coef) const
    {
      coef[0] = coef[1] = 0.;
      
      int i, j;
      const int indx_next[] = {1,2,0};
      const int indx_nnext[] = {2,0,1};
      
      // Constant term = volume (with lambda = 0) = AD.(CD x BD)
      // Linear term depends on the local term
      switch(locnode)
	{
	case 0: // V(t) = V0 + t dir.(CD x BD)
	  {
	    for(int k=0; k<3; ++k)
	      {
		i = indx_next[k]; j = indx_nnext[k];
		coef[0] += (coord[0][k]-coord[3][k])*( (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
						       (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]) ); 
		coef[1] += dir[k]*( (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
				    (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]));
	      }
	    break;
	  }

	case 1: // V(t) = V0 + t dir.(AD x CD)
	  {
	    for(int k=0; k<3; ++k)
	      {
		i = indx_next[k]; j = indx_nnext[k];
		coef[0] += (coord[0][k]-coord[3][k])*( (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
						       (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]) ); 
		coef[1] += dir[k]*((coord[0][i]-coord[3][i])*(coord[2][j]-coord[3][j])-
				   (coord[0][j]-coord[3][j])*(coord[2][i]-coord[3][i]) );
	      }
	    break;
	  }

	case 2: // V(t) = V0 + t dir.(BD x AD)
	  {
	    for(int k=0; k<3; ++k)
	      {
		i = indx_next[k]; j = indx_nnext[k];
		coef[0] += (coord[0][k]-coord[3][k])*( (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
						       (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]) ); 
		coef[1] += dir[k]*((coord[1][i]-coord[3][i])*(coord[0][j]-coord[3][j])-
				   (coord[1][j]-coord[3][j])*(coord[0][i]-coord[3][i]));
	      }
	    break;
	  }

	case 3: // V(t) = V0 - t dir.(CD x BD + AD x CB))
	  {
	    for(int k=0; k<3; ++k)
	      {
		i = indx_next[k]; j = indx_nnext[k];
		coef[0] += (coord[0][k]-coord[3][k])*( (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
						       (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]) ); 
		coef[1] -= dir[k]*(
				   (coord[2][i]-coord[3][i])*(coord[1][j]-coord[3][j])-
				   (coord[2][j]-coord[3][j])*(coord[1][i]-coord[3][i]) + // CD x BD
				   (coord[0][i]-coord[3][i])*(coord[2][j]-coord[1][j])-
				   (coord[0][j]-coord[3][j])*(coord[2][i]-coord[1][i])); // AD x CB)
	      }
	    break;
	  }
	  
	default: assert(false && "dvr::GeomTet3DQuality::VolumeCoef()- Unexpected local node number provided.");
	}

      // Missing a factor of 1/6
      coef[0] *= ONEbySIX;
      coef[1] *= ONEbySIX;

      return;
    }
      
  
  // Main functionality: compute extreme points of the quality of an element by perturbing
  // its local node along a prescribe direction
  template<typename MeshType> void
    GeomTet3DQuality<MeshType>::Extremize(const int elm, const int locnode, 
					  const double* rdir, double* lambda, double* qvals,
					  int& nlambda) const
    {
      // Access local data
      auto& data = GetLocalData();
      auto& VCoef = data.VCoef;
      auto& SCoef = data.L2Coef;
      auto& coefs = data.coefs;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Nodal coordinates
      const auto* elmconn = MD.connectivity(elm);
      const double* coord[4] = {MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
				MD.coordinates(elmconn[2]), MD.coordinates(elmconn[3])};

      // Get the coefficients of volume(lambda) = a + b lambda
      VolumeCoef(coord, locnode, rdir, VCoef);
      
      // Get the coefficients of the sum of edge lengths squared: p + q lambda + r lambda^2
      EdgeLen2Coef(coord, locnode, rdir, SCoef);

      // Replace lambda = lscale*t, where lscale is O(h).
      // Keeps the polynomial well conditioned
      lscale = std::sqrt(SCoef[0]*ONEbySIX);
      VCoef[1] *= lscale;
      SCoef[1] *= lscale; SCoef[2] *= lscale*lscale;

      // Normalize by the volume at t = 0
      norm = VCoef[0];
      VCoef[0] /= norm; VCoef[1] /= norm;
      norm = std::cbrt(norm*norm);
      SCoef[0] /= norm; SCoef[1] /= norm; SCoef[2] /= norm;
      
      // Maximize: V(t)/S(t)^1.5 -> yields  quadratic
      coefs[2] = 2.*VCoef[1]*SCoef[2];
      coefs[1] = 3.*VCoef[0]*SCoef[2]+0.5*VCoef[1]*SCoef[1];
      coefs[0] = 1.5*VCoef[0]*SCoef[1]-VCoef[1]*SCoef[0];
      
      // Solve
      data.PolySolver.Solve(2, coefs, EPS, lambda, nlambda);

      // We always expect extrema.
      dvr_assert(nlambda>=1 && "dvr::GeomTet3DQuality::Extremize()- Could not find extrema.");

      // Rescale roots and compute qualities
      for(int i=0; i<nlambda; ++i)
	{
	  qvals[i] = Qnorm*
	    (VCoef[0] + VCoef[1]*lambda[i])/
	    std::pow(SCoef[0] + SCoef[1]*lambda[i] + SCoef[2]*lambda[i]*lambda[i],1.5);
	  lambda[i] *= lscale;
	}

      return;
    }


  
  
  // Main functionality: compute the intersection of a pair of quality curved
  // while perturbing a common vertex
  template<typename MeshType> void
    GeomTet3DQuality<MeshType>::Intersect(const int elm1, const int locnode1, 
					  const int elm2, const int locnode2, 
					  const double* rdir, double* lambda, double* qvals,
					  int& nlambda,
					  void* usrparams) const
    {
      // Read defined parameter
      detail::ParamInterval* param =
	(usrparams==nullptr) ? nullptr : static_cast<detail::ParamInterval*>(usrparams);
	
      // Get local data
      auto& data = GetLocalData();
      auto& VCoefs = data.VCoefs;
      auto& SCoefs = data.L2Coefs;
      auto& coefs = data.coefs;
      auto& nroots = data.nroots;
      auto& roots = data.roots;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Nodal coordinates
      const auto* elmconn1 = MD.connectivity(elm1);
      const double* coord1[] = {MD.coordinates(elmconn1[0]), MD.coordinates(elmconn1[1]),
				MD.coordinates(elmconn1[2]), MD.coordinates(elmconn1[3])};
      const auto* elmconn2 = MD.connectivity(elm2);
      const double* coord2[] = {MD.coordinates(elmconn2[0]), MD.coordinates(elmconn2[1]),
				MD.coordinates(elmconn2[2]), MD.coordinates(elmconn2[3])};

      // Volume terms
      VolumeCoef(coord1, locnode1, rdir, VCoefs[0]);
      VolumeCoef(coord2, locnode2, rdir, VCoefs[1]);
      
      // Perimeter^2 terms
      EdgeLen2Coef(coord1, locnode1, rdir, SCoefs[0]);
      EdgeLen2Coef(coord2, locnode2, rdir, SCoefs[1]);

      // Length scale that is O(h). Compute using perimeter^2
      lscale = 0.5*(std::sqrt(SCoefs[0][0]*ONEbySIX) +
		    std::sqrt(SCoefs[1][0]*ONEbySIX));

      // Rather than solving for lambda, which is O(h),
      // solve for t = lambda/lscale, where lscale is O(h).
      // Consequently, t is O(1) and the coefficients are reasonably well conditioned.
      // Scale coefficients of polynomials by the lengthscale.
      VCoefs[0][1] *= lscale; VCoefs[1][1] *= lscale;
      SCoefs[0][1] *= lscale; SCoefs[0][2] *= lscale*lscale;
      SCoefs[1][1] *= lscale; SCoefs[1][2] *= lscale*lscale;

      // We are solving Q1(t) = Q2(t), where Q(t) = V(t)/S^2(t)^{3/2}.
      // Normalize polynomials in each quality term by the respective volume.
      norm = VCoefs[0][0];
      VCoefs[0][0] /= norm; VCoefs[0][1] /= norm;
      norm = std::cbrt(norm*norm);
      SCoefs[0][0] /= norm; SCoefs[0][1] /= norm; SCoefs[0][2] /= norm;

      norm = VCoefs[1][0];
      VCoefs[1][0] /= norm; VCoefs[1][1] /= norm;
      norm = std::cbrt(norm*norm);
      SCoefs[1][0] /= norm; SCoefs[1][1] /= norm; SCoefs[1][2] /= norm;
      
      // Coefficients of 8th degree polynomial
      // Eq: (Vol 1)^2 (Len 2)^3 - (Vol 2)^2 (Len 1)^3
      // Get coefficients for volume of tet 1: V1(lambda) = a + b \lambda
      // Get coefficients for volume of tet 2: V2(lambda) = p + q \lambda
      // Get coefficients for L2 of tet 1: L2(lambda) = c + d lambda + e lambda^2
      // Get coefficients for L2 of tet 2: L2(lambda) = r + s lambda + t lambda^2
      {
	auto& a = VCoefs[0][0]; auto& b = VCoefs[0][1];
	auto& p = VCoefs[1][0]; auto& q = VCoefs[1][1];
	auto& c = SCoefs[0][0]; auto& d = SCoefs[0][1]; auto& e = SCoefs[0][2];
	auto& r = SCoefs[1][0]; auto& s = SCoefs[1][1]; auto& t = SCoefs[1][2];

	// coef[i] = coefficient of lambda^i
	coefs[0] = -c*c*c*p*p + a*a*r*r*r;
	coefs[1] = -c*c*p*(3*d*p + 2*c*q) + a*r*r*(2*b*r + 3*a*s);
	coefs[2] = -c*(3*(d*d + c*e)*p*p + 6*c*d*p*q + c*c*q*q) + r*(b*b*r*r + 6*a*b*r*s + 3*a*a*(s*s + r*t));
	coefs[3] = -d*(d*d + 6*c*e)*p*p - 6*c*(d*d + c*e)*p*q - 3*c*c*d*q*q +  3*b*b*r*r*s + 6*a*b*r*(s*s + r*t) + a*a*s*(s*s + 6*r*t);
	coefs[4] = -3*e*(d*d + c*e)*p*p - 2*d*(d*d + 6*c*e)*p*q - 3*c*(d*d + c*e)*q*q + 3*b*b*r*(s*s + r*t) + 3*a*a*t*(s*s + r*t) + 2*a*b*s*(s*s + 6*r*t);
	coefs[5] = -3*d*e*e*p*p - 6*e*(d*d + c*e)*p*q - d*(d*d + 6*c*e)*q*q + b*b*s*s*s + 6*b*s*(b*r + a*s)*t + 3*a*(2*b*r + a*s)*t*t;
	coefs[6] = -e*(e*e*p*p + 6*d*e*p*q + 3*(d*d + c*e)*q*q) + t*(6*a*b*s*t + a*a*t*t + 3*b*b*(s*s + r*t));
	coefs[7] = -e*e*q*(2*e*p + 3*d*q) + b*t*t*(3*b*s + 2*a*t);
	coefs[8] = -e*e*e*q*q + b*b*t*t*t;	
      }
      
      // Identify the leading order of the polynomial
      int DEGREE = 8;
      while(DEGREE>=0)
	if(std::abs(coefs[DEGREE])>EPS) break;
	else --DEGREE;
      
      // Should not have degree = 0, for this would mean constant != 0.
      dvr_assert(DEGREE!=0 && "dvr::GeomTet3DQuality::Intersect()- Unexpected polynomial degree.");

      // Set solution flag to true; toggle if returning without solving.
      if(param!=nullptr)
	param->flag = true;
      
      // Initialize the number of roots
      nroots = 0;
      nlambda = 0;
      
      // Examine degree       
      if(DEGREE<0)        // The two element quality curves are identical.
	{ nlambda = 0; } 
      else if(DEGREE==1)
	{ nroots = 1; roots[0] = -coefs[0]/coefs[1]; }
      else if(DEGREE==2)
	{ data.PolySolver.Solve(DEGREE, coefs, EPS, roots, nroots); }
      else
	{
	  // Check if there may be a root
	  if(param!=nullptr)
	    {
	      param->flag = false; // Toggle solution flag
	      int nsubdiv = 4; // Number of subdivisions for querying roots
	      auto* tinterval = param->tinterval; // Pass the scaled parameter, not the given one
	      switch(DEGREE) // If the query returns false, there is no root. We can return.
		{
		case 3:
		  if(!data.RootCounter.template QueryRoot<3>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		case 4:
		  if(!data.RootCounter.template QueryRoot<4>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		case 5:
		  if(!data.RootCounter.template QueryRoot<5>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		case 6:
		  if(!data.RootCounter.template QueryRoot<6>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		case 7:
		  if(!data.RootCounter.template QueryRoot<7>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		case 8:
		  if(!data.RootCounter.template QueryRoot<8>(tinterval[0]/lscale, tinterval[1]/lscale, coefs, EPS, nsubdiv)) return;
		}
	      param->flag = true; // Untoggle solution flag
	    }
	  
	  // Solve
	  data.PolySolver.Solve(DEGREE, coefs, EPS, roots, nroots);
	}

      // The computed roots are solutions to equality of squared qualities.
      // Examine the solutions of the actual qualities
      double qval1, qval2;
      for(int i=0; i<nroots; ++i)
	// Intersection points are the subsets of roots
	// for which qualities of the two elements are close enough
	{
	  // Volumes and perimeter^2s of the two elements
	  auto& tval = roots[i];
	  for(int j=0; j<2; ++j)
	    {
	      data.Vols[j]  = VCoefs[j][0] + VCoefs[j][1]*tval;
	      data.Len2s[j] = SCoefs[j][0] + SCoefs[j][1]*tval + SCoefs[j][2]*tval*tval;
	    }
	  // Compare qualities 
	  qval1 = data.Vols[0]/(data.Len2s[0]*std::sqrt(data.Len2s[0]));
	  qval2 = data.Vols[1]/(data.Len2s[1]*std::sqrt(data.Len2s[1]));
	  if(std::abs(qval1-qval2)< EPS)
	    {
	      lambda[nlambda] = tval*lscale; // Rescale root for return.
	      qvals[nlambda] = Qnorm*qval1;
	      ++nlambda;
	    }
	}
      
      return;
    }
  

  // Main functionality: compute inverse of the quality metric
  // along a prescribed direction
  template<typename MeshType>
    void GeomTet3DQuality<MeshType>::
    Invert(const int elm, const int locnode,
	   const double* rdir, const double qval,
	   double* lambda, int& nlambda,
	   void* usrparams) const
    {
      dvr_assert(qval>0.);
      
      // User defined parameter
      detail::ParamInterval* param =
	(usrparams==nullptr) ? nullptr : static_cast<detail::ParamInterval*>(usrparams);
      
      // Access local data
      auto& data = GetLocalData();
      auto& VCoef = data.VCoef;
      auto& SCoef = data.L2Coef;
      auto& coefs = data.coefs;
      auto& lscale = data.lscale;
      auto& norm = data.norm;
      
      // Access nodal coordinates
      const auto* elmconn = MD.connectivity(elm);
      const double* coord[] = {MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
			       MD.coordinates(elmconn[2]), MD.coordinates(elmconn[3])};

      // V(lambda) = V0 + V1 lambda
      // S(lambda) = S0 + S1 lambda + S2 lambda^2
      VolumeCoef(coord, locnode, rdir, VCoef);
      EdgeLen2Coef(coord, locnode, rdir, SCoef);
      
      // If the quality given is zero, we have a linear equation
      if(std::abs(qval)<EPS)
	{
	  lambda[0] = -VCoef[0]/VCoef[1];
	  nlambda = 1;
	  if(param!=nullptr) param->flag = true;
	  return;
	}

      // Solve the equation 72 Sqrt(3) V(lambda)/S(lambda)^(3/2) = qval.
      // Set lambda = lscale t, where lscale is O(h). This ensures that coefficients
      // are well conditioned and that t is O(1)
      lscale = std::sqrt(SCoef[0]*ONEbySIX);
      VCoef[1] *= lscale;
      SCoef[1] *= lscale; SCoef[2] *= lscale*lscale;

      // Factor out volume term
      norm = VCoef[0];
      VCoef[0] /= norm; VCoef[1] /= norm;
      norm = std::cbrt(norm*norm);
      SCoef[0] /= norm; SCoef[1] /= norm; SCoef[2] /= norm;

      // We now have to solve V(t)/S(t)^(3/2) = qval/Qnorm.
      // Absorb RHS into V(t)
      VCoef[0] *= Qnorm/qval;
      VCoef[1] *= Qnorm/qval;

      // Solve V(t)/S(t)^(3/2) = 1 by squaring both sides
      // This yields a 6th degree polynomial equation
      coefs[0] = SCoef[0]*SCoef[0]*SCoef[0]-VCoef[0]*VCoef[0];
      coefs[1] = 3.*SCoef[0]*SCoef[0]*SCoef[1]-2.*VCoef[0]*VCoef[1];
      coefs[2] = 3.*SCoef[0]*(SCoef[1]*SCoef[1]+SCoef[0]*SCoef[2])-VCoef[1]*VCoef[1];
      coefs[3] = SCoef[1]*SCoef[1]*SCoef[1]+6.*SCoef[0]*SCoef[1]*SCoef[2];
      coefs[4] = 3.*SCoef[0]*SCoef[2]*SCoef[2]+3.*SCoef[1]*SCoef[1]*SCoef[2];
      coefs[5] = 3.*SCoef[1]*SCoef[2]*SCoef[2];
      coefs[6] = SCoef[2]*SCoef[2]*SCoef[2];
      
      // The leading coefficient is necessarily non zero (norm^2 of relaxation direction)
      // The equation is therefore 6th degree.

      // Can real roots exist
      nlambda = 0;
      if(param!=nullptr)
	if(!data.RootCounter.template QueryRoot<6>(param->tinterval[0]/lscale, param->tinterval[1]/lscale,
						   coefs, EPS, 4))
	  { param->flag = false; return; }
      
      // Real roots may exist. Compute them.
      if(param!=nullptr) param->flag = true;
      data.PolySolver.Solve(6, coefs, EPS, data.roots, data.nroots);
      
      // The computed roots solve Q^2 = qval^2.
      // Identify the roots of Q = qval.
      for(int i=0; i<data.nroots; ++i)
	{
	  auto& tval = data.roots[i];
	  if(std::abs((VCoef[0]+VCoef[1]*tval)-
		      pow(SCoef[0]+SCoef[1]*tval+SCoef[2]*tval*tval,1.5))<EPS)
	    lambda[nlambda++] = tval*lscale; // Rescale roots for return
	}
      
      return;
    }
}

