
/** \file testGeomTri2DQuality.cpp
 * \brief Unit test for the class dvr::GeomTri2DQuality.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */

#include <dvr_GeomTri2DQuality.h>
#include <dvr_utils_SimpleMesh.h>
#include <iostream>
#include <cmath>

using namespace dvr::utils;
using namespace dvr;

int main()
{
  // Create a mesh with two elements
  const SimpleMesh MD({0.,0., 1.,-2., 1.25,1.75, -2.5,3., -1.75, -1.}, {0,1,2,0,3,4}, 2);
  
  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  const int locnode = 0;

  // Create qualtiy
  GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);

  // Compute the quality of elements (compare with values from mathematica)
  const double EPS = 1.e-6;
  assert(std::abs(Quality.Compute(0)-0.6198918679720192)<EPS);
  assert(std::abs(Quality.Compute(1)-0.7483425091935219)<EPS);
  
  // Perturbation
  const double rdir[] = {cos(M_PI/7), sin(M_PI/7)};
  double lambda = 0.5689;
  assert(std::abs(Quality.Compute(0,locnode,rdir,lambda)-0.3726372357298533)<EPS);
  assert(std::abs(Quality.Compute(1,locnode,rdir,lambda)-0.8671058745146214)<EPS);

  // Compute the maxima of quality curve for elements:
  double tmax[2], qmax[2];
   int nmax;
  Quality.Extremize(0, locnode, rdir, tmax, qmax, nmax);
  assert(nmax==2);
  assert(std::abs(tmax[0]+2.0275953101555473)<EPS || std::abs(tmax[1]+2.0275953101555473)<EPS );
  for( int i=0; i<2; ++i)
    {
      double qval  = Quality.Compute(0,locnode,rdir,tmax[i]);
      assert(std::abs(qmax[i]-qval)<EPS);
    }

  
  Quality.Extremize(1,locnode,rdir,tmax,qmax,nmax);
  assert(nmax==2);
  assert(std::abs(tmax[0]-2.025990103442721)<EPS || std::abs(tmax[1]-2.025990103442721)<EPS);
  for( int i=0; i<2; ++i)
    {
      double qval  = Quality.Compute(1,locnode,rdir,tmax[i]);
      assert(std::abs(qmax[i]-qval)<EPS);
    }
  
  // Compute the intersections of quality curves
  double tintersect[3], qintersect[3];
   int nintersect;
  Quality.Intersect(0, locnode, 1, locnode, rdir, tintersect, qintersect, nintersect);
  assert(nintersect==1);
  assert(std::abs(tintersect[0]+0.20513266942439132)<EPS);
  assert(std::abs(qintersect[0]-Quality.Compute(0,locnode,rdir,tintersect[0]))<EPS);
  assert(std::abs(qintersect[0]-Quality.Compute(1,locnode,rdir,tintersect[0]))<EPS);

  // Compute the derivative of the quality curve
  assert(std::abs(Quality.dCompute(0,locnode,rdir,0.)+0.3768164353450259)<EPS);
  assert(std::abs(Quality.dCompute(0,locnode,rdir,lambda)+0.4837531471904039)<EPS);

  // Invert the quality metric
  double tinverse[2];
   int nvals;
  double qval = 0.17;
  Quality.Invert(0, locnode, rdir, qval, tinverse, nvals);
  assert(nvals==2 && "Unexpected number of inverse values found.");
  assert(std::abs(Quality.Compute(0,locnode,rdir,tinverse[0])-qval)<EPS);
  assert(std::abs(Quality.Compute(0,locnode,rdir,tinverse[1])-qval)<EPS);
}
