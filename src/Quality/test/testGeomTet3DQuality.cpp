
/** \file testGeomTet3DQuality.cpp
 * \brief Unit test for the class dvr::GeomTet3DQuality.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */
#include <dvr_GeomTet3DQuality.h>
#include <dvr_utils_SimpleMesh.h>
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace dvr::utils;
using namespace dvr;

int main()
{
  // Create a mesh with two elements
  const std::vector<double> coords{-1.4142135,   0.0000000,  0.0000000,
      0.0000000, -1.0000000,  0.0000000,
      0.0000000,   1.0000000,  0.0000000,
      0.0000000,   0.0000000,  1.4142135,
      -1.4142135,   0.0000000,  0.0000000,
      -2.0000000, -1.0000000,  0.5000000,
      -4.0000000,   0.0000000,  1.4142135,
      -3.0000000,   1.0000000,  0.0000000};
  const std::vector<int> conn{0,1,2,3,4,5,6,7};
  const SimpleMesh MD(coords, conn, 3);
  
  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Create quality
  GeomTet3DQuality<decltype(MD)> Quality(mesh_wrapper);

  // Perturbation direction
  const double dir[] = {1./sqrt(2.),1./sqrt(3.),-1./sqrt(6.)};
  
  // Compute the quality of elements (compare with values from mathematica)
  const double EPS = 1.e-6;
  assert(std::abs(Quality.Compute(0)-0.9295159948906221)<EPS);
  assert(std::abs(Quality.Compute(1)-0.23813421080880962)<EPS);

  // Compute the derivative of the quality
  assert(std::abs(Quality.dCompute(0,0,dir,0.123)+0.19648278841035244)<EPS);
  assert(std::abs(Quality.dCompute(1,0,dir,0.123)-0.08088748214771431)<EPS);
  
  // Compute the maxima of quality curve for elements:
  double tmax[2], qmax[2];
   int nmax;
  Quality.Extremize(0, 0, dir, tmax, qmax, nmax);
  assert(nmax==2);
  for( int i=0; i<2; ++i)
    {
      double qval = Quality.Compute(0,0,dir,tmax[i]);
      assert(std::abs(qval-qmax[i])<1.e-6);
    }
  std::sort(tmax, tmax+2);
  assert(std::abs(tmax[0]+0.24929284816605599)<EPS);
  assert(std::abs(tmax[1]-3.6530676531795354)<EPS);
  
  Quality.Extremize(1,0,dir,tmax,qmax,nmax);
  assert(nmax==2);
  std::sort(tmax, tmax+nmax);
  assert(std::abs(tmax[0]+3.1899742032384406)<EPS);
  assert(std::abs(tmax[1]-0.7521899658499168)<EPS);

   // Compute the intersections of quality curves
  double tintersect[8], qintersect[8];
   int nintersect = 0;
  Quality.Intersect(0,0, 1,0, dir, tintersect, qintersect, nintersect);
  assert(nintersect==1);
  std::sort(tintersect, tintersect+nintersect);
  assert(std::abs(tintersect[0]-1.4769234423499273)<EPS);

  // Test inversion
  double qval = 0.23;
  double tinv[6];
   int ninv;
  Quality.Invert(0,0,dir,qval,tinv,ninv);
  assert(ninv>0 && "Found zero inversions.\n");
  for( int i=0; i<ninv; ++i)
    {
      double myqval = Quality.Compute(0,0,dir,tinv[i]);
      assert(std::abs(myqval-qval)<1.e-6);
    }
  Quality.Invert(1,0,dir,qval,tinv,ninv);
  assert(ninv>0 && "Found zero inversions.\n");
  for( int i=0; i<ninv; ++i)
    {
      double myqval = Quality.Compute(1,0,dir,tinv[i]);
      assert(std::abs(myqval-qval)<1.e-6);
    }
}
