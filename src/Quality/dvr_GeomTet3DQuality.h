
/** \file dvr_GeomTet3DQuality.h
 * \brief Definition of the class dvr::GeomTet3DQuality.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <dvr_DesCartesPolyRootCounter.h>
#include <dvr_GNUPolySolver.h>
#include <dvr_ParamInterval.h>
#include <dvr_MeshTraits.h>
#include <dvr_QualityTraits.h>
#include <dvr_MeshWrapper.h>
#include <vector>
#include <cassert>

namespace dvr{

  namespace detail
  {
    //! struct encapsulating data required for quality computations
    struct Geom3Ddata
    {
      const GNUPolySolver<8> PolySolver; //!< Polynomial solver, upto degree 8
      const DesCartesPolyRootCounter RootCounter; //!< Root counter in an interval

      double Vol; //!< For computing volume
      double Vols[2]; //!< For computing volumes
      double dVol; //!< For computing derivative of volumes

      double VCoef[2]; //!< V(t) = VCoef[0] + t VCoef[1]
      double VCoefs[2][2]; //!< V(t) = VCoef[0] + t VCoef[1], for a pair of elements
      
      double Len2; //!< For computing perimeter^2
      double Len2s[2]; //!< For computing a pair of perimeter^2
      double dLen2; //!< For computing derivative of perimeter

      double L2Coef[3]; //!< L2(t) = L2Coef[0] + t L2Coef[1] + t^2 L2Coef[2]
      double L2Coefs[2][3]; //!< L2(t) = L2Coef[0] + t L2Coef[1] + t^2 L2Coef[2], for a pair of elements

      double AD[3]; //!< Edge AD of a tet
      double BD[3]; //!< Edge BD of a tet
      double CD[3]; //!< Edge CD of a tet

      double coefs[9]; //!< For storing coefficients of polynomials, upto degree 8
      double lscale; //!< Length scale
      double norm; //!< Normalization factor, etc.
      int nroots; //!< Number of real roots of polynomials
      double roots[8]; //!< Roots of 8th degree polynomials
    };
  }


    /** \brief Class for evaluating qualities of 3D tetrahedra using the mean ratio metric.
   *
   * The quality of a tetrahedron \f$K\f$ is defined as 
   * \f[  {\rm Q}(K) = 72\sqrt{3}\,\frac{{\text{Signed volume}(K)}}{(\ell_{12}^2+\ell_{13}^2+\ell_{14}^2 + \ell_{23}^2 + \ell_{24}^2 + \ell_{34}^2)^{3/2}}. \f]
   * where \f$\ell_{pq}\f$ is the length of the edge joining the \f$p^{\rm th}\f$ and \f$q^{\rm th}\f$ vertices of \f$K\f$.
   *
   * The metric has the following properties:
   * <ul>
   * <li> Dimensionless </li>
   * <li> Independent of the size of the tet, i.e., invariant under homogeneous dilations</li>
   * <li> Invariant under isometric transformations (rigid body motions) </li>
   * <li> Has the same sign as the orientation of the tetrahedron.</li>
   * <li> Has a unique extremum, equal to a maximum, when the tetrahedron is regular. </li>
   * </ul>
   * The  normalization factor \f$72\sqrt{3}\f$ ensures that regular tetrahedra
   * are assigned a quality of 1.
   * 
   *  Detailed analysis of the metric, including its relationship with 
   * other commonly used metrics can be found in 
   * <br>
   * A. Liu and B. Joe, <em>On the shape of tetrahedra from bisection</em>, 
   * Math. Comp., 63 (1994), 141–154, 
   * <a href="https://doi.org/10.1090/S0025-5718-1994-1240660-4">(doi)</a>.
   *
   * Degenerate tets, by virtue of having zero volume, are assigned a quality of zero.
   * 
   * The class is equipped to compute:
   * <ul>
   * <li> The function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$, 
   * where \f$K^{\lambda}\f$ is the tetrahedron that results from perturbing one of the vertices
   * of \f$K\f$ along a prescribed direction. This evaluation is performed by the method
   * <br>
   * dvr::GeomTet3DQuality::Compute(const int elm, const int locnode, const double *rdir, const double lambda) const. </li>
   * <li> The derivatives of the element quality curve \f$d{\rm Q}(K^\lambda)/d\lambda\f$.</li>
   * <li> Pairwise intersections of element quality curves 
   * \f[\{\lambda\in{\mathbb R}\,:\,{\rm Q}(K_\alpha^\lambda)={\rm Q}(K_\beta^\lambda)\}~\text{for}~\alpha\neq \beta.\f] 
   * This is done by the method dvr::GeomTet3DQuality::Extremize. </li>
   * <li> Level sets of the quality curve: 
   * \f[ \{\lambda\in {\mathbb R}\,:\,{\rm Q}(K^\lambda)=Q_0\}\f]
   * This is done by the method dvr::GeomTet3DQuality::Invert.</li>
   * </ul>
   * To perform these calculations, 
   * the class exploits the fact that \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ is a rational function of the form .
   * \f${\rm Q}(K^\lambda) = f(\lambda)/\sqrt{g(\lambda)}\f$, where \f$f(\lambda)\f$ is  affine  and
   * \f$g(\lambda)\f$ is quadratic in \f$\lambda\f$. 
   * It is straightforward to verify from the definition of \f${\rm Q}\f$ that: 
   * <ul>
   * <li> Identifying extremizers of \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ requires computing roots of 
   * a quadratic polynomial of \f$\lambda\f$. See dvr::GeomTet3DQuality::Extremize. </li>
   * <li> Identifying intersections of \f$\lambda\mapsto {\rm Q}(K_\alpha^\lambda)\f$ and 
   * \f$\lambda\mapsto {\rm Q}(K_\beta^\lambda)\f$ requires computing roots of an octic polynomial in \f$\lambda\f$.
   * See dvr::GeomTet3DQuality::Intersect. </li>
   * <li> Identifying level sets of \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ requires computing roots of 
   * a sextic polynomial. See dvr::GeomTet3DQuality::Invert.</li>
   * </ul>
   * The class uses a local instance of dvr::GNUPolySolver for polynomial root finding. <b>In these calculations, 
   * ill-conditioning of polynomial root finding is a key issue</b>. A hard-coded non-dimensional tolerance 
   * <tt>EPS=1e-6</tt> is used to identify the leading order of the polynomial after non-dimensionalizing
   * the coefficients. This is not a robust fix for the issue. 
   *
   * The tolerance <tt>EPS</tt> is also used to determine 
   * whether roots are real or imaginary in the dvr::GNUPolyRootSolver. 
   *   
   * The methods to compute quality curves \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$,
   * their extrema, and their pairwise intersections is used by the max-min solvers
   * dvr::PartitionedMaxMinSovler and dvr::ReconstructiveMaxMinSolver classes.
   *
   * The class is thread-safe. At construction, the class creates
   * as many copies of 
   * local thread-safe data structures as the number of threads specified.
   * 
   * The class is templated by the type of the mesh data structure.
   * \tparam MeshType typename of the mesh data structure.
   * 
   * \see Unit test for the class: testGeomTet3DQuality.cpp
   *
   * \note Assertions in the dvr::GNUPolySolver::Solve method during mesh relaxation
   * is caused by ill conditioning of polynomial root finding in the methods dvr::Extremize,
   * dvr::Intersect, and dvr::Invert.
   * 
   * \ingroup dvr_quality_metrics
   */
  template<typename MeshType>
    class GeomTet3DQuality
    {
    public:
      //! Constructor
      //! \param[in] MW Mesh wrapper, referenced
      //! \param[in] nMaxThr Max number of threads, defaulted to 1, used for sizing thread-safe memory.
      inline GeomTet3DQuality(MeshWrapper<MeshType>& MW, const int nMaxThr=1)
	:mesh_wrapper(MW),
	MD(mesh_wrapper.GetMesh()),
	nMaxThreads(nMaxThr),
	ThrMyData(nMaxThr)
      {
	//static_assert(CheckMeshTraits<MeshType>(), "MeshType failed to satisfy requirements");
	dvr_assert(MD.spatial_dimension()==3 && "Expected tet mesh with spatial dimension = 3");
	CheckQualityTraits<GeomTet3DQuality<MeshType>, MeshType>();
      }
      
      //! Destructor
      inline virtual ~GeomTet3DQuality() {}

      //! Disable copy
      GeomTet3DQuality(const GeomTet3DQuality<MeshType>&) = delete;

      //! Return reference to the mesh wrapper
      inline MeshWrapper<MeshType>& GetMeshWrapper() const
      { return mesh_wrapper; }
      
      //! Returns reference to the mesh object
      inline const MeshType& GetMesh() const
      { return MD; }

      //! Returns the number of threads
      inline int GetMaxNumThreads() const
      { return nMaxThreads; }

      //! \brief Computes the quality of a given element in a mesh
      //! \param[in] elm Element index in the mesh
      //! \return Quality of element elm
      inline double Compute(const int elm) const
      { const auto* elmconn = MD.connectivity(elm);
	return ComputeTetQuality(MD.coordinates(elmconn[0]), MD.coordinates(elmconn[1]),
				 MD.coordinates(elmconn[2]), MD.coordinates(elmconn[3])); }

      /** \brief Compute the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       * \param[in] elm Element number in mesh. This identifies the element \f$K\f$.
       * \param[in] locnode Local node being perturbed. 
       * Should be 0, 1, 2, or 3 and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Perturbation direction.
       * \param[in] lambda Perturbation coordinate.
       * \return Quality of element elm when the local node <tt>locnode</tt>
       * is perturbed by coordinate <tt>lambda</tt> along direction <tt>rdir</tt>
       */
      double Compute(const int elm, const int locnode,
		     const double* rdir, const double lambda) const;
      
      /** \brief Compute the derivative \f$\lambda\mapsto d{\rm Q}(K^\lambda)/d\lambda\f$.
       * \param[in] elm Element number in mesh. This identifies the element \f$K\f$.
       * \param[in] locnode Local node being perturbed.
       * Should be 0, 1, 2 or 3, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Relaxation direction
       * \param[in] lambda Perturbation coordinate
       * \return Derivative of quality of element <tt>elm</tt> when its local node <tt>locnode</tt>
       * is perturbed along direction <tt>rdir</tt>, evaluated at coordinate <tt>lambda</tt>
       */
      double dCompute(const int elm, const int locnode,
		      const double* rdir, const double lambda) const;
      
      /** Computes extrema of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       *
       * \param[in] locnode Local node number in the element. 
       * Should be 0, 1, 2 or 3, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] elm Element number
       * \param[in] rdir Relaxation direction
       * \param[out] lambda Computed (local) extrema of the quality
       * \param[out] qvals Qualities at computed extrema
       * \param[out] nlambda Number of extrema computed
       */
      void Extremize(const int elm, const int locnode, 
		     const double* rdir, double* lambda, double* qvals,
		     int& nlambda) const;
      
       /** Returns an upper bound on the number of extrema of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       *
       * Used by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver
       * to size local data structures.
       */
      inline int GetNumExtrema() const
      { return 2; }

      /** Computes quality curve intersections: \f$\lambda\f$ such that \f${\rm Q}(K^\lambda_\alpha)={\rm Q}(K^\lambda_\beta)\f$ when perturbing a common node.
       * \param[in] elm1 Element 1. Determines \f$K_\alpha\f$.
       * \param[in] elm2 Element 2. Determines \f$K_\beta\f$.
       * \param[in] locnode1 Local node of element 1.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the element in the mesh.
       * locnode1 of elm1 should coincide with locnode2 of elm2.
       * \param[in] locnode2 Local node of element 2.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the element in the mesh.
       * locnode1 of elm1 should coincide with locnode2 of elm2.
       * \param[in] rdir Relaxation direction for the common node
       * \param[out] lambda Computed intersections of quality curves of elements 1 and 2
       * \param[out] nlambda Number of intersections computed
       * \param[out] qvals Qualities at computed intersections
       * \param[in,out] usrparams User parameters. Should be castable to dvr::detail::ParamInterval.
       * Is used to specify the parametric interval in which to look for roots. 
       * The routine checks for existence of roots within the interval
       * before attempting a solve. In case of no roots, it avoids the calculation entirely.
       * In these cases, the boolean flag, on return, is set to false.
       * \warning dvr::GeomTet3DQuality::EPS is used to determine the degree of the polynomial
       * to be resolved. The same tolerance is also used to determine if computed roots 
       * are real- or complex-valued. Failure of the method, by way of false assertions 
       * during execution of the method are due to ill conditioning of the polynomial
       * root finding problem.
       */
      void Intersect(const int elm1, const int locnode1, 
		     const int elm2, const int locnode2, 
		     const double* rdir, double* lambda, double* qvals,
		     int& nlambda, void* usrparams=nullptr) const;

      /** Returns an upper bound of the number of intersections of quality curves, i.e., solutions of \f${\rm Q}(K^\lambda_\alpha)={\rm Q}(K^\lambda_\beta)\f$ for \f$\alpha\neq \beta\f$.
       * Used by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver
       * for sizing local data structures.
       */
      inline int GetNumIntersections() const
      { return 8; }
      
      /** \brief level sets of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       * \param[in] elm Element number in mesh. This determines the element \f$K\f$.
       * \param[in] locnode Local node being perturbed.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Relaxation direction
       * \param[out] lambda Perturbation coordinates where quality equals prescribed value
       * \param[out] nlambda Number of coordinates found.
       * \param[in] qval Given value of quality. Should be positive.
       * \param[in,out] usrparams User parameters. Should be castable to dvr::detail::ParamInterval.
       * Is used to specify the parametric interval in which to look for roots. 
       * The routine checks for existence of roots within the interval
       * before attempting a solve. In case of no roots, it avoids the calculation entirely.
       * In these cases, the boolean flag, on return, is set to false.
       * \warning dvr::GeomTet3DQuality::EPS is used to decide if quality = 0.
       */
      void Invert(const int elm, const int locnode,
		  const double* rdir, const double qval,
		  double* lambda,  int& nlambda,
		  void* usrparams=nullptr) const;
      
    private:
      //! Compute the quality of a tet
      //! \param[in] P,Q,R,S Coordinates of the four vertices
      double ComputeTetQuality(const double* P, const double* Q,
			       const double* R, const double* S) const;

      //! Access to local data structure
      //! \return Data structure for local thread
      detail::Geom3Ddata& GetLocalData() const;

      
      const double EPS = 1.e-6; //!< Temporary tolerance for polynomial solves.
      MeshWrapper<MeshType> &mesh_wrapper; //!< Reference to mesh wrapper
      const MeshType &MD; //!< Reference to the mesh object
      const int nMaxThreads; //!< Number of threads
      mutable std::vector<detail::Geom3Ddata> ThrMyData; //!< Thread-safe data structure for intermediate computations.
      static constexpr double Qnorm = 124.7076581449591651339761365884228104198819782743474052200; //!< 72.*sqrt(3.) Normalization factor for quality

      static constexpr double ONEbySIX = 1./6.;  //! Useful constant

      //! Helper function: Express volume as a function of perturbation coordinate
      //! V(lambda) = a + b lambda
      //! \param[in] coord Vertex coordinates
      //! \param[in] locnode Local node to be perturbed
      //! \param[in] dir Direction coordinates
      //! \param[out] coef Coefficients in linear expression
      //! for \f$V(\lambda)=coef_0 + coef_1\lambda\f$.
      void VolumeCoef(const double* coord[4], const int locnode,
		      const double* dir, double* coef) const;

      //! Helper function: Express sum of edge length squared as a function of perturbation coordinate
      //! L2(lambda) + a + b lambda + c lambda^2
      //! \param[in] coord Vertex coordinates
      //! \param[in] locnode Local node to be perturbed
      //! \param[in] dir Direction coordinates
      //! \param[out] coef Coefficients in quadratic expression for
      //! \f$L2(\lambda)=coef_0 + coef_1\lambda + coef_2\lambda^2\f$.
      void EdgeLen2Coef(const double* coord[4], const int locnode, const double* dir,
			double* coef) const;
    };
}


// Implementation of class
#include <dvr_GeomTet3DQuality_Impl.h>


