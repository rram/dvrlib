
/** \file dvr_GeomTri2DQuality.h
 * \brief Definition of the class dvr::GeomTri2DQuality.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <dvr_ParamInterval.h>
#include <dvr_DesCartesPolyRootCounter.h>
#include <dvr_GNUPolySolver.h>
#include <dvr_MeshTraits.h>
#include <dvr_QualityTraits.h>
#include <dvr_MeshWrapper.h>
#include <vector>
#include <cassert>

namespace dvr{

  namespace detail
  {
    //! Struct encapsulating data required for triangle quality computations
    struct Geom2Ddata
    {
      const GNUPolySolver<3> PolySolver; //!< Polynomial solver, upto degree 3
      const DesCartesPolyRootCounter RootCounter; //!< Root counter in an interval

      double Area; //!< For computing areas
      double dArea;  //!< For computing derivative of areas

      double Len2; //!< For computing squared perimeters
      double dLen2;  //!< Derivative of squared perimeter

      double ACoef[2]; //!< A(t) = ACoef[0] + ACoef[1] t
      double ACoefs[2][2]; //!< A(t) = ACoef[0] + ACoef[1] t for a pair of triangles
      
      double L2Coef[3]; //!< S^2(t) = L2Coef[0] + L2Coef[1] t + L2Coef[2] t^2
      double L2Coefs[2][3]; //!< S^2(t) = L2Coef[0] + L2Coef[1] t + L2Coef[2] t^2 for a pair of triangles
      
      double coefs[4]; //!< For storing coefficients of polynomials, upto DEGREE 3.
      double lscale; //!< Length scale
      double norm; //!< For normalizing factors
      
      ParamInterval interval; //!< Interval of parametric coordinates
    };
  }
  

  /** \brief Class for evaluating qualities of planar triangles using the mean ratio metric.
   *
   * The quality of a triangle \f$K\f$ is defined as 
   * \f[ {\rm Q}(K) = 4\sqrt{3}\frac{\text{Signed Area}(K)}{\ell_{12}^2+\ell_{23}^2+\ell_{13}^2}, \f]
   * where \f$\ell_{pq}\f$ is the length of the edge joining the \f$p^{\rm th}\f$ and \f$q^{\rm th}\f$ vertices of \f$K\f$.
   *
   * The metric has the following properties:
   * <ul>
   * <li> Dimensionless </li>
   * <li> Independent of the size of the triangle, i.e., invariant under homogeneous dilations</li>
   * <li> Invariant under isometric transformations (rigid body motions) </li>
   * <li> Has the same sign as the orientation of the triangle</li>
   * <li> Has a unique extremum, equal to a maximum, when the triangle is equilateral.</li>
   * </ul>
   * The  normalization factor \f$4\sqrt{3}\f$ ensures that equilateral triangles 
   * are assigned a quality of 1.
   * 
   *  Element quality metrics
   * \f${\rm Q}_1\f$ and \f${\rm Q}_2\f$ are weakly equivalent of there exist 
   * constants \f$c_1, c_2, \alpha\f$ and \f$\beta\f$ such that 
   * \f[c_1{\rm Q}_1(K)^\alpha\leq {\rm Q}_2(K)\leq c_2{\rm Q}_2(K)^\beta\f]
   * The two metrics are said to be equivalent if \f$\alpha=\beta=1\f$. 
   *
   * In this sense, the mean ratio metric is related to other commonly used triangle quality metrics:
   * <ul>
   * <li> It is equivalent to the metric defined as the minimum interior angle of a triangle.</li>
   * <li> It is weakly equivalent to the aspect ratio metric, defined as the ratio of the inradius
   * to the circumradius of the triangle. </li>
   * </ul>
   * Degenerate triangles, by virtue of having zero area, are assigned a quality of zero.
   * 
   * The class is equipped to compute:
   * <ul>
   * <li> The function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$, 
   * where \f$K^{\lambda}\f$ is the triangle that results from perturbing one of the vertices
   * of \f$K\f$ along a prescribed direction. This evaluation is performed by the method
   * <br>
   * dvr::GeomTri2DQuality::Compute(const int elm, const int locnode, const double *rdir, const double lambda) const. </li>
   * <li> The derivatives of the element quality curve \f$d{\rm Q}(K^\lambda)/d\lambda\f$.</li>
   * <li> Pairwise intersections of element quality curves 
   * \f[\{\lambda\in{\mathbb R}\,:\,{\rm Q}(K_\alpha^\lambda)={\rm Q}(K_\beta^\lambda)\}~\text{for}~\alpha\neq \beta.\f] 
   * This is done by the method dvr::GeomTri2DQuality::Extremize. </li>
   * <li> Level sets of the quality curve: 
   * \f[ \{\lambda\in {\mathbb R}\,:\,{\rm Q}(K^\lambda)=Q_0\}\f]
   * This is done by the method dvr::GeomTri2DQuality::Invert.</li>
   * </ul>
   * To perform these calculations, 
   * the class exploits the fact that \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ is a rational polynomial function of the form .
   * \f${\rm Q}(K^\lambda) = f(\lambda)/{g(\lambda)}\f$, where \f$f(\lambda)\f$ is  affine  and
   * \f$g(\lambda)\f$ is quadratic in \f$\lambda\f$. 
   * It is straightforward to verify from the definition of \f${\rm Q}\f$ that: 
   * <ul>
   * <li> Identifying extremizers of \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ requires computing roots of 
   * a quadratic polynomial of \f$\lambda\f$. See dvr::GeomTri2DQuality::Extremize. </li>
   * <li> Identifying intersections of \f$\lambda\mapsto {\rm Q}(K_\alpha^\lambda)\f$ and 
   * \f$\lambda\mapsto {\rm Q}(K_\beta^\lambda)\f$ requires computing roots of a cubic polynomial in \f$\lambda\f$.
   * See dvr::GeomTri2DQuality::Intersect. </li>
   * <li> Identifying level sets of \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$ requires computing roots of 
   * a quadratic polynomial. See dvr::GeomTri2DQuality::Invert.</li>
   * </ul>
   * The class uses a local instance of dvr::GNUPolySolver for polynomial root finding. <b>In these calculations, 
   * ill-conditioning of polynomial root finding is a key issue</b>. A hard-coded non-dimensional tolerance 
   * <tt>EPS=1e-6</tt> is used to identify the leading order of the polynomial after non-dimensionalizing
   * the coefficients. This is not a robust fix for the issue. 
   *
   * It may appear that the tolerance <tt>EPS</tt> is also used to determine 
   * whether roots are real or imaginary in the dvr::GNUPolyRootSolver. Since
   * the degree of the polynomial is less than or equal to 3, special real-root
   * finding routines are invoked in the dvr::GNUPolyRootSolver::Solve method, which
   * do not use these tolerances.
   *   
   * The methods to compute quality curves \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$,
   * their extrema, and their pairwise intersections is used by the max-min solvers
   * dvr::PartitionedMaxMinSovler and dvr::ReconstructiveMaxMinSolver classes.
   *
   * The class is thread-safe. At construction, the class creates
   * as many copies of 
   * local thread-safe data structures as the number of threads specified.
   * 
   * The class is templated by the type of the mesh data structure.
   * \tparam MeshType typename of the mesh data structure.
   * 
   * \see Unit test for the class: testGeomTri2DQuality.cpp
   *
   * \note Assertions in the dvr::GNUPolySolver::Solve method during mesh relaxation
   * is caused by ill conditioning of polynomial root finding in the methods dvr::Extremize,
   * dvr::Intersect, and dvr::Invert.
   * 
   * \ingroup dvr_quality_metrics
   */
  template<typename MeshType>
    class GeomTri2DQuality
    {
    public:
      //! Constructor
      //! \param[in] MW mesh wrapper, referenced
      //! \param[in] numMaxThr Max number of threads, defaulted to 1, used for sizing thread-safe memory
      inline GeomTri2DQuality(MeshWrapper<MeshType>& MW, const int numMaxThr=1)
	:mesh_wrapper(MW),
	MD(mesh_wrapper.GetConstMesh()),
	nMaxThreads(numMaxThr),
	ThrMyData(numMaxThr)
      {
	dvr_assert(MD.spatial_dimension()==2 && "Expected triangle mesh with spatial dimension = 2");
	CheckQualityTraits<GeomTri2DQuality<MeshType>, MeshType>();
      }
      
      //! Destructor
      inline virtual ~GeomTri2DQuality() {} 
      
      //! Disable copy
      GeomTri2DQuality(const GeomTri2DQuality<MeshType>&) = delete;

      //! Return reference to the mesh wrapper
      inline MeshWrapper<MeshType>& GetMeshWrapper() const
      { return mesh_wrapper; }
      
      //! Returns reference to the mesh object
      inline const MeshType& GetMesh() const
      { return MD; }

      //! Returns the maximum number of threads
      inline int GetMaxNumThreads() const
      { return nMaxThreads; }
	
      //! \brief Computes the quality of a given element in a mesh
      //! \param[in] elm Element index in the mesh
      //! \return Quality of element elm
      inline double Compute(const int elm) const 
      {	const auto* elmconn = MD.connectivity(elm);
	return ComputeTriangleQuality(MD.coordinates(elmconn[0]),
				      MD.coordinates(elmconn[1]),
				      MD.coordinates(elmconn[2])); }
      
      /** \brief Compute the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       * \param[in] elm Element number in mesh. This identifies the element \f$K\f$.
       * \param[in] locnode Local node being perturbed. 
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Perturbation direction.
       * \param[in] lambda Perturbation coordinate.
       * \return Quality of element elm when the local node <tt>locnode</tt>
       * is perturbed by coordinate <tt>lambda</tt> along direction <tt>rdir</tt>
       */
      inline double Compute(const int elm, const int locnode,
			    const double* rdir, const double lambda) const
      {	const auto* conn = MD.connectivity(elm);
	const auto* A = MD.coordinates(conn[locnode]);
	double X[2] = {A[0]+lambda*rdir[0], A[1]+lambda*rdir[1]};
	return ComputeTriangleQuality(X,
				      MD.coordinates(conn[(locnode+1)%3]),
				      MD.coordinates(conn[(locnode+2)%3])); }
      

      /** \brief Compute the derivative \f$\lambda\mapsto d{\rm Q}(K^\lambda)/d\lambda\f$.
       * \param[in] elm Element number in mesh. This identifies the element \f$K\f$.
       * \param[in] locnode Local node being perturbed.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Relaxation direction
       * \param[in] lambda Perturbation coordinate
       * \return Derivative of quality of element <tt>elm</tt> when its local node <tt>locnode</tt>
       * is perturbed along direction <tt>rdir</tt>, evaluated at coordinate <tt>lambda</tt>
       */
      double dCompute(const int elm, const int locnode,
		      const double* rdir, const double lambda) const;
      
      /** \brief level sets of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       * \param[in] elm Element number in mesh. This determines the element \f$K\f$.
       * \param[in] locnode Local node being perturbed.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] rdir Relaxation direction
       * \param[out] lambda Perturbation coordinates where quality equals prescribed value
       * \param[out] nlambda Number of coordinates found.
       * \param[in] qval Given value of quality. Should be positive.
       * \param[in,out] usrparams User parameters. Should be castable to dvr::detail::ParamInterval.
       * Is used to specify the parametric interval in which to look for roots. 
       * The routine checks for existence of roots within the interval
       * before attempting a solve. In case of no roots, it avoids the calculation entirely.
       * In these cases, the boolean flag, on return, is set to false.
       * \warning dvr::GeomTri2DQuality::EPS is used to decide if quality = 0.
       */
      void Invert(const int elm, const int locnode,
		  const double* rdir, const double qval,
		  double* lambda,  int& nlambda,
		  void* usrparams=nullptr) const;
      
      /** Computes extrema of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       *
       * \param[in] locnode Local node number in the element. 
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the 
       * element in the mesh.
       * \param[in] elm Element number
       * \param[in] rdir Relaxation direction
       * \param[out] lambda Computed (local) extrema of the quality
       * \param[out] qvals Qualities at computed extrema
       * \param[out] nlambda Number of extrema computed
       */
      void Extremize(const int elm, const int locnode, 
		     const double* rdir, double* lambda, double* qvals,
		     int& nlambda) const;

      /** Returns an upper bound on the number of extrema of the function \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.
       *
       * Used by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver
       * to size local data structures.
       */
      inline int GetNumExtrema() const
      { return 2; }
      
      /** Computes quality curve intersections: \f$\lambda\f$ such that \f${\rm Q}(K^\lambda_\alpha)={\rm Q}(K^\lambda_\beta)\f$ when perturbing a common node.
       * \param[in] elm1 Element 1. Determines \f$K_\alpha\f$.
       * \param[in] elm2 Element 2. Determines \f$K_\beta\f$.
       * \param[in] locnode1 Local node of element 1.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the element in the mesh.
       * locnode1 of elm1 should coincide with locnode2 of elm2.
       * \param[in] locnode2 Local node of element 2.
       * Should be 0, 1 or 2, and consistent with the enumeration of nodes in the connectivity of the element in the mesh.
       * locnode1 of elm1 should coincide with locnode2 of elm2.
       * \param[in] rdir Relaxation direction for the common node
       * \param[out] lambda Computed intersections of quality curves of elements 1 and 2
       * \param[out] nlambda Number of intersections computed
       * \param[out] qvals Qualities at computed intersections
       * \param[in,out] usrparams User parameters. Should be castable to dvr::detail::ParamInterval.
       * Is used to specify the parametric interval in which to look for roots. 
       * The routine checks for existence of roots within the interval
       * before attempting a solve. In case of no roots, it avoids the calculation entirely.
       * In these cases, the boolean flag, on return, is set to false.
       * \warning dvr::GeomTri2DQuality::EPS is used to determine the degree of the polynomial
       * to be resolved. The same tolerance is also used to determine if computed roots 
       * are real- or complex-valued. Failure of the method, by way of false assertions 
       * during execution of the method are due to ill conditioning of the polynomial
       * root finding problem.
       */
      void Intersect(const int elm1, const int locnode1, 
		     const int elm2, const int locnode2, 
		     const double* rdir, double* lambda, double* qvals,
		     int& nlambda,
		     void* usrparams=nullptr) const;

      /** Returns an upper bound of the number of intersections of quality curves, i.e., solutions of \f${\rm Q}(K^\lambda_\alpha)={\rm Q}(K^\lambda_\beta)\f$ for \f$\alpha\neq \beta\f$.
       * Used by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver
       * for sizing local data structures.
       */
      inline int GetNumIntersections() const
      { return 3; }

    private:
      //! Compute the quality of a triangle
      //! \param[in] Coord Triangle coordinates
      inline double ComputeTriangleQuality(const double* P, const double* Q,
					   const double* R) const
      {
	return Qnorm*
	  0.5*((Q[0]-P[0])*(R[1]-P[1]) - (Q[1]-P[1])*(R[0]-P[0]))/ // Area
	  ((P[0]-Q[0])*(P[0]-Q[0]) + (P[1]-Q[1])*(P[1]-Q[1]) + // Perimeter^2
	   (Q[0]-R[0])*(Q[0]-R[0]) + (Q[1]-R[1])*(Q[1]-R[1]) +
	   (R[0]-P[0])*(R[0]-P[0]) + (R[1]-P[1])*(R[1]-P[1]));
      }
      
      
      //! Access to local data structures
      //! \return Data structure for the local thread.
      detail::Geom2Ddata& GetLocalData() const;
	
      const double EPS = 1.e-6; /*! < Non-dimensional tolerance.
				 * Used for determining the degree of polynomials to be resolve by examining
				 * non-dimensionalized polynomial coefficients.
				 * Also used to determine if computed roots are real- or complex-valued.
				 */
      MeshWrapper<MeshType> &mesh_wrapper; //!< Mesh wrapper
      const MeshType& MD;                  //!< Reference to the mesh object
      const int nMaxThreads;               //!< Max number of threads to be used
      mutable std::vector<detail::Geom2Ddata> ThrMyData; //!< Thread-safe data structures to use for intermediate computations
      static constexpr double Qnorm = 6.928203230275509174109785366023489467771221015241522512223; //!< 4*std::sqrt(3.) Normalizing factor for quality
      
      //! Helper function: Express area as a function of perturbation coordinate
      //! A(lambda) = coef_0 + coef_1 lambda.
      //! \param[in] Coord Vertex coordinates
      //! \param[in] locnode Local node to be pertubed
      //! \param[in] dir Direction coordinates
      //! \param[out] coef Coefficients in linear expression
      //! for \f$A(\lambda) = coef_0 + coef_1 \lambda\f$.
      void AreaCoef(const double* Coord[3], const int locnode,
		    const double* dir, double* coef) const;

      //! Helper function: Express perimeter^2 are a function of
      //! perturbation coordinate.
      //! \param[in] Coord Vertex coordinates
      //! \param[in] locnode Local node to be pertubed
      //! \param[in] dir Direction coordinates
      //! \param[out] coef Coefficients in quadratic expression
      //! for \f$S^2(\lambda) = coef_0 + coef_1 \lambda + coef_2 \lambda^2\f$.
      void EdgeLen2Coef(const double* Coord[3], const int locnode,
			const double* dir, double* coef) const;
      
    };
}
  

// Implementation
#include <dvr_GeomTri2DQuality_Impl.h>

