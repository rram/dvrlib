
/** \file dvr_DesCartesPolyRootCounter_Impl.h
 * \brief Implementation of the class dvr::DesCartesPolyRootCounter
 * \author Ramsharan Rangarajan
 * \date January 22, 2021. 
 */

#pragma once

namespace dvr
{
  // Main functionality: Returns an estimate for the number of real roots,
  // including their multiplicities, in a given interval of the real line.
  template<int DEGREE>  int
    DesCartesPolyRootCounter::Count(const double& a, const double& b,
				    const double* coef, const double EPS,
				    int nsubdiv)
    {
      assert(a<b && "dvr::DesCartesPolyRootCounter::Count()- Unexpected interval limits.");
      
      // Mobius transformation of the given polynomial
      double P[DEGREE+1];
      Transform<DEGREE>(a, b, coef, P);

      // Count the number of sign changes
      bool SmallCoefFlag = false;
      int var = DesCartesPolyRootCounter::CountSignChanges(DEGREE, P, EPS, SmallCoefFlag);

      if(var==0) // No roots
	return var;
      else if(var==1) // Only one root
	return var;
      else if(nsubdiv==0) // If number of subdivisions has exceeded, stop.
	return var;
      // More than one root appears to exist in this interval.
      else // Subdivide this interval into 2 branches. 
	// Examine number of roots in left branch and right branch
	return
	  Count<DEGREE>(a, (a+b)/2., coef, EPS, nsubdiv-1) + // Left branch
	  Count<DEGREE>((a+b)/2., b, coef, EPS, nsubdiv-1); // Right branch
    }


  
  // Main functionality: Queries if the given interval contains a root
  template<int DEGREE> bool
    DesCartesPolyRootCounter::QueryRoot(const double& a, const double& b,
					const double* coef, const double EPS,
					int nsubdiv)
    {
      assert(a<b && "dvr::DesCartesPolyRootCounter::QueryRoot()- Unexpected interval limits.");
      
      // Mobius transformation of the given polynomial
      double P[DEGREE+1];
      Transform<DEGREE>(a, b, coef, P);
      
      // Count the number of sign changes
      bool SmallCoefFlag = false;
      int var = CountSignChanges(DEGREE, P, EPS, SmallCoefFlag);

      if(var==0) // No roots in this interval
	return false;
      else if(SmallCoefFlag==false && var%2==1) // An odd number of roots were found
	return true;                            // Then there has to be a root
      else if(nsubdiv==0) // If number of subdivisions has exceeded, stop
	return true;      // This point is reached only if a root is assumed to exist
      else // Subdivide this interval into 2 branches
	// Examine if either branch contains a root
	return
	  QueryRoot<DEGREE>(a, (a+b)/2., coef, EPS, nsubdiv-1) || // Left branch
	  QueryRoot<DEGREE>((a+b)/2., b, coef, EPS, nsubdiv-1); // Right branch
    }
  
}

