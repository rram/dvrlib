
/** \file dvr_GNUPolySolver_Impl.h
 * \brief Implementation of the class dvr::GNUPolySolver
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <cassert>
#include <cmath>
#include <iostream>

namespace dvr
{
  // The cases MaxDegree > 3
  //==========================================
  
  // Constructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::GNUPolySolver()
    :Lambda(2*MaxDegree)
    { assert(MaxDegree>3);
      WorkSpcs.resize(MaxDegree-3);
      for( int i=0; i<MaxDegree-3; ++i)
	WorkSpcs[i] = gsl_poly_complex_workspace_alloc(i+5); }

  // Destructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::~GNUPolySolver()
    { for(auto& ws:WorkSpcs)
	gsl_poly_complex_workspace_free(ws); }

  // Copy constructor
  template<int MaxDegree>
    GNUPolySolver<MaxDegree>::GNUPolySolver(const GNUPolySolver<MaxDegree>& Obj)
    :Lambda(2*MaxDegree)
    { WorkSpcs.resize(MaxDegree-3);
      for( int i=0; i<MaxDegree-3; ++i)
	WorkSpcs[i] = gsl_poly_complex_workspace_alloc(i+5); }
  


  //==========
  // Generic solver
  template< int MaxDegree>
    void GNUPolySolver<MaxDegree>::Solve(const int Degree, const double* coefs,
					 const double EPS, double* realroots,  int& nroots) const
    {
      // Check the degree
      assert(Degree<=MaxDegree && "GNUPolySolver::Solve()- Unexpected degree.");
      
      switch (Degree)
	{
	  // Constant
	case 0 : assert(false && "GNUPolySolver::Solve()- Degree = 0.");

	  // Linear
	case 1:
	  { realroots[0] = -coefs[0]/coefs[1];
	    nroots = 1; return; }
	
	  // Quadratic
	case 2:
	  { nroots = gsl_poly_solve_quadratic(coefs[2], coefs[1], coefs[0], &realroots[0], &realroots[1]);
	    return; }

	  // Cubic
	case 3:
	  { nroots = gsl_poly_solve_cubic(coefs[2]/coefs[3], coefs[1]/coefs[3], coefs[0]/coefs[3],
					  &realroots[0], &realroots[1], &realroots[2]);
	    return; }
	  
	  // Higher degree
	default:
	  {
	    auto success = gsl_poly_complex_solve(coefs, Degree+1, WorkSpcs[Degree-4], &Lambda[0]);
	    if(success!=GSL_SUCCESS)
	      {
		std::cout<<"Could not find root of polynomial with coefficients: ";
		for( int i=0; i<Degree+1; ++i)
		  std::cout<<coefs[i]<<", ";
		std::fflush( stdout );
		dvr_assert(false && "GNUPloySolver::Solve()- Could not solve.");
	      }
	    
	    // Identify real roots
	    nroots = 0;
	    double l1norm = 0.;
	    for( int i=0; i<Degree; ++i)
	      {
		l1norm = std::abs(Lambda[2*i])+std::abs(Lambda[2*i+1]);
		if( (l1norm<EPS) || std::abs(Lambda[2*i+1])/l1norm<EPS )
		  realroots[nroots++] = Lambda[2*i];
	      }
	    return;
	  }
	}
      return;
    }

}

