
/** \file dvr_GNUPolySolver.h
 * \brief Definition of the class dvr::GNUPolySolver
 * \author Ramsharan Rangarajan
 * \date March 23, 2020. 
 */

#pragma once

#include <dvr_assert.h>
#include <vector>
#include "gsl/gsl_poly.h"
#include "gsl/gsl_errno.h"

namespace dvr
{
  /** \brief Wrapper for using GNU GSL's univariate polynomial solve routines.
   *
   * The class is provided so that memory allocated for 
   * workspaces and for (complex-valued) roots can be re-used by
   * an instance of the class.
   *
   * The class is templated by the maximum degree of the 
   * polynomial that an instance will request computing roots for.
   * This polynomial degree is used to allocate memory at construction.
   * 
   * The cases degree=1,2 and 3 are treated differently than the
   * reset since GSL provides special and simpler routines for
   * these cases. These cases are implemented as template specializations.
   * This helps to avoid if-then-else checks for the degree.
   *
   * \warning This class is NOT thread-safe. 
   * The reason for not making it thread-safe is that it is more sensible to create 
   * one instance of this class per thread.
   */
  template<int MaxDegree>
    class GNUPolySolver
    {
    public:
      //! Constructor
      GNUPolySolver();

      //! Destructor
      virtual ~GNUPolySolver();
      
      //! Copy constructor
      //! \param[in] Obj Object to be copied
      GNUPolySolver(const GNUPolySolver<MaxDegree>& Obj);
      
      //! Cloning
      inline virtual GNUPolySolver<MaxDegree>* Clone() const
      { return new GNUPolySolver<MaxDegree>(*this); }

            /** 
       * \param[in] Degree Degree of polynomial. Should be <= MaxDegree
       * Should be less than or equal to MaxDegree
       * \param[in] coefs Coefficients of polynomial with
       * the convention that coef[i]  equals to coefficient of x^i.
       * Assumes that coefs[Degree] is not zero so that the leading
       * coefficient can be normalized to 1.
       * \param[in] EPS Relative tolerance to use for determining
       * if a root is real or not. Not used in the cases MaxDegree = 1,2 or 3.
       * In the remaining cases, a root is deemed to be real if the imaginary part < EPS * (|real part| + |imaginary part|).
       * \param[out] realroots Real roots.
       * \param[out] nroots Number of real roots computed.
       */
      void Solve(const int Degree, const double* coefs,
		 const double EPS, double* realroots,  int& nroots) const;

    private:
      mutable std::vector<gsl_poly_complex_workspace*> WorkSpcs; //!<  Workspace, from degree = 4 to DEGREE.
      mutable std::vector<double> Lambda; //!< Space for complex roots
    };

  // Instantiate specializations defined in .cpp
  template<> GNUPolySolver<0>::GNUPolySolver();
  
  template<> GNUPolySolver<1>::GNUPolySolver();
  template<> GNUPolySolver<1>::GNUPolySolver(const GNUPolySolver<1>&);
  template<> GNUPolySolver<1>::~GNUPolySolver();
 
  template<> GNUPolySolver<2>::GNUPolySolver();
  template<> GNUPolySolver<2>::GNUPolySolver(const GNUPolySolver<2>&);
  template<> GNUPolySolver<2>::~GNUPolySolver();

  template<> GNUPolySolver<3>::GNUPolySolver();
  template<> GNUPolySolver<3>::GNUPolySolver(const GNUPolySolver<3>&);
  template<> GNUPolySolver<3>::~GNUPolySolver();
 
}


// Implementation with specializations
#include "dvr_GNUPolySolver_Impl.h"
