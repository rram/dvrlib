
/** \file dvr_GNUPolySolver.cpp
 * \brief Implementation of template specializations
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#include <cassert>
#include <dvr_assert.h>
#include <cmath>
#include <iostream>
#include <dvr_GNUPolySolver.h>

namespace dvr
{
  // Specialization of constructors + destructors for degree < 3
  //==========================================================

  // Disallow degree = 0
  //! Constructor specialization to disallow the case MaxDegree = 0
  template<> GNUPolySolver<0>::GNUPolySolver():Lambda{}
  { assert(false && "dvr::GNUPolySolver- Degree cannot be zero."); }
  
  // Degree = 1
  //! Constructor specialization for the case MaxDegree = 1. Workspace for solve is not allocated
  template<> GNUPolySolver<1>::GNUPolySolver() :Lambda(2) {}
  //! Destructor specialization for the case MaxDegree = 1
  template<> GNUPolySolver<1>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 1
  template<> GNUPolySolver<1>::GNUPolySolver(const GNUPolySolver<1>& obj):Lambda(2) {}

  // Degree = 2
  //! Constructor specialization for the case MaxDegree = 2. Workspace for solve is not allocated.
  template<> GNUPolySolver<2>::GNUPolySolver() :Lambda(4) {}
  //! Destructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<2>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<2>::GNUPolySolver(const GNUPolySolver<2>& obj):Lambda(4) {}

  // Degree = 3
  //! Constructor specialization for the case MaxDegree = 3. Workspace for solve is not allocated
  template<> GNUPolySolver<3>::GNUPolySolver() :Lambda(6) {}
  //! Destructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<3>::~GNUPolySolver() {}
  //! Copy constructor specialization for the case MaxDegree = 2
  template<> GNUPolySolver<3>::GNUPolySolver(const GNUPolySolver<3>& obj):Lambda(6) {}

}
