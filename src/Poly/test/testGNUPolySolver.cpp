
/** \file testGNUPolySolver.cpp
 * \brief Unit test for the class dvr::GNUPolySolver
 *
 * \author: Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */

#include <dvr_GNUPolySolver.h>
#include <random>
#include <algorithm>

using namespace dvr;

// Random number generator
std::random_device rd;  
std::mt19937 gen(rd()); 
std::uniform_real_distribution<> dis(-1., 1.);

// Test different degrees
void Linear();
void Quadratic();
void Cubic();
void Quartic();

int main()
{
  // Test linear solver
  Linear();
      
  // Test quadratic solver
  Quadratic();

  // Test cubic solver
  Cubic();

  // Test quartic solver
  Quartic();
}
  
void Linear()
{
  constexpr int degree = 1;
  
  // Create solver
  GNUPolySolver<degree> solver;

  // data
  double real_roots[degree];
  int nroots = 0;
  const double EPS = 1.e-6;
  double correct_roots[degree];
  double lead_coef;
  double coefs[degree+1];

  // Multiple solves
  for(int run=0; run<100; ++run)
    {
      // generate random roots
      for(int k=0; k<degree; ++k)
	correct_roots[k] = dis(gen);

      // generate random nonzero leading coefficient
      while(true)
	{ lead_coef = dis(gen);
	  if(std::abs(lead_coef)>1.e-3) break; }

      // Assign polynomial coefficients
      coefs[0] =-correct_roots[0];
      coefs[1] = 1.;
      for(int k=0; k<degree+1; ++k)
	coefs[k] *= lead_coef;
  
      // solve
      solver.Solve(degree, coefs, EPS, real_roots, nroots);

      // Check
      assert(nroots==degree);
      for(int k=0; k<degree; ++k)
	assert(std::abs(real_roots[0]-correct_roots[0])<EPS);
    }
  
  // done
}


void Quadratic()
{
  constexpr int degree = 2;
  
  // Create solver
  GNUPolySolver<degree> solver;

  // data
  double real_roots[degree];
  int nroots = 0;
  const double EPS = 1.e-6;
  double correct_roots[degree];
  double lead_coef;
  double coefs[degree+1];

  // Multiple solves
  for(int run=0; run<100; ++run)
    {
      // generate random roots
      for(int k=0; k<degree; ++k)
	correct_roots[k] = dis(gen);

      // generate random nonzero leading coefficient
      while(true)
	{ lead_coef = dis(gen);
	  if(std::abs(lead_coef)>1.e-3) break; }

      // Assign polynomial coefficients
      coefs[0] = std::pow(-1.,static_cast<double>(degree))*correct_roots[0]*correct_roots[1];
      coefs[1] = std::pow(-1.,static_cast<double>(degree-1))*(correct_roots[0]+correct_roots[1]);
      coefs[2] = 1.;
      for(int k=0; k<degree+1; ++k)
	coefs[k] *= lead_coef;
  
      // solve
      solver.Solve(degree, coefs, EPS, real_roots, nroots);

      // Check
      std::sort(correct_roots, correct_roots+degree);
      std::sort(real_roots, real_roots+nroots);
      assert(nroots==degree);
      for(int k=0; k<degree; ++k)
	assert(std::abs(real_roots[k]-correct_roots[k])<EPS);
    }
  
  // done
}


void Cubic()
{
  constexpr int degree = 3;
  
  // Create solver
  GNUPolySolver<degree> solver;

  // data
  double real_roots[degree];
  int nroots = 0;
  const double EPS = 1.e-6;
  double correct_roots[degree];
  double lead_coef;
  double coefs[degree+1];

  // Multiple solves
  for(int run=0; run<100; ++run)
    {
      // generate random roots
      for(int k=0; k<degree; ++k)
	correct_roots[k] = dis(gen);

      // generate random nonzero leading coefficient
      while(true)
	{ lead_coef = dis(gen);
	  if(std::abs(lead_coef)>1.e-3) break; }

      // Assign polynomial coefficients
      coefs[0] = std::pow(-1.,static_cast<double>(degree))*correct_roots[0]*correct_roots[1]*correct_roots[2];
      coefs[1] = std::pow(-1.,static_cast<double>(degree-1))*(correct_roots[0]*correct_roots[1] +
							      correct_roots[0]*correct_roots[2] +
							      correct_roots[1]*correct_roots[2]);
      coefs[2] = std::pow(-1.,static_cast<double>(degree-2))*(correct_roots[0]+correct_roots[1]+correct_roots[2]);
      coefs[3] = 1.;
      for(int k=0; k<degree+1; ++k)
	coefs[k] *= lead_coef;
  
      // solve
      solver.Solve(degree, coefs, EPS, real_roots, nroots);

      // Check
      std::sort(correct_roots, correct_roots+degree);
      std::sort(real_roots, real_roots+nroots);
      assert(nroots==degree);
      for(int k=0; k<degree; ++k)
	assert(std::abs(real_roots[k]-correct_roots[k])<EPS);
    }
  
  // done
}


void Quartic()
{
  constexpr int degree = 4;
  
  // Create solver
  GNUPolySolver<degree> solver;

  // data
  double real_roots[degree];
  int nroots = 0;
  const double EPS = 1.e-6;
  double correct_roots[degree];
  double lead_coef;
  double coefs[degree+1];

  // Multiple solves
  for(int run=0; run<1; ++run)
    {
      // generate random roots
      for(int k=0; k<degree; ++k)
	correct_roots[k] = dis(gen);

      // generate random nonzero leading coefficient
      while(true)
	{ lead_coef = dis(gen);
	  if(std::abs(lead_coef)>1.e-3) break; }

      // Assign polynomial coefficients
      coefs[0] = std::pow(-1.,static_cast<double>(degree))*correct_roots[0]*correct_roots[1]*correct_roots[2]*correct_roots[3];
      coefs[1] = std::pow(-1.,static_cast<double>(degree-1))*(correct_roots[0]*correct_roots[1]*correct_roots[2] +
							      correct_roots[0]*correct_roots[1]*correct_roots[3] +
							      correct_roots[0]*correct_roots[2]*correct_roots[3] +
							      correct_roots[1]*correct_roots[2]*correct_roots[3] );
      coefs[2] = std::pow(-1.,static_cast<double>(degree-2))*(correct_roots[0]*correct_roots[1]+
							      correct_roots[0]*correct_roots[2]+
							      correct_roots[0]*correct_roots[3]+
							      correct_roots[1]*correct_roots[2]+
							      correct_roots[1]*correct_roots[3]+
							      correct_roots[2]*correct_roots[3]);
      coefs[3] = std::pow(-1.,static_cast<double>(degree-3))*(correct_roots[0]+
									 correct_roots[1]+
									 correct_roots[2]+
									 correct_roots[3]);
      coefs[4] = 1.;
      for(int k=0; k<degree+1; ++k)
	coefs[k] *= lead_coef;
  
      // solve
      solver.Solve(degree, coefs, EPS, real_roots, nroots);

      // Check
      std::sort(correct_roots, correct_roots+degree);
      std::sort(real_roots, real_roots+nroots);
      assert(nroots==degree);
      for(int k=0; k<degree; ++k)
	assert(std::abs(real_roots[k]-correct_roots[k])<EPS);
    }
  
  // done
}
