
/** \file testDesCartes.cpp
 * \brief Unit test for the class dvr::DesCartesPolyRootCounter
 * \author: Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */

#include <dvr_DesCartesPolyRootCounter.h>
#include <random>
#include <algorithm>
#include <gsl/gsl_poly.h>
#include <gsl/gsl_errno.h>
#include <iostream>

using namespace dvr;

// Test for polynomial degree
template< int DEGREE>
void RandomPolynomialTests(const  int nRuns);

int main()
{
  const  int nRuns = 10000;
  RandomPolynomialTests<3>(nRuns);
  RandomPolynomialTests<4>(nRuns);
  RandomPolynomialTests<5>(nRuns);
  RandomPolynomialTests<6>(nRuns);
  RandomPolynomialTests<7>(nRuns);
  RandomPolynomialTests<8>(nRuns);
}



// Positive confirmation of roots
template< int DEGREE>
void PositivelyConfirmRoots(const double* coef, const double EPS,
			    const double* real_roots, const  int nreal,
			    const  int nsubdiv)
{
  // Root counter
  DesCartesPolyRootCounter dc;

  // Cross check with the number of real roots counted by DesCartes algorithm
  double a, b;
  int ndiv = nsubdiv;
  
  // No real roots
  if(nreal==0)
    return;

  // 1 real root
  if(nreal==1)
    {
      a = real_roots[0]-1.;
      b = real_roots[0] + 1.;
      int nroots = dc.Count<DEGREE>(a, b, coef, EPS, ndiv);
      bool flag = dc.QueryRoot<DEGREE>(a, b, coef, EPS, ndiv);
      assert((bool)nroots==flag);
      if(nroots<1)
	{
	  for( int i=0; i<DEGREE+1; ++i)
	    std::cout<<coef[i]<<" ";
	  std::cout<<"\nSituation failed.\n"; std::fflush( stdout );
	}
      assert(nroots>=1);
      return;
    }

  // Multiple real roots
  for( int j=0; j<nreal; ++j)
    {
      // Create an isolated interval around this root
      if(j==0)
	{
	  a = real_roots[0]-1.;
	  b = (real_roots[0]+real_roots[1])/2.;
	}
      else if(j==nreal-1)
	{
	  a = (real_roots[nreal-2]+real_roots[nreal-1])/2.;
	  b = real_roots[nreal-1]+1.;
	}
      else
	{
	  a = (real_roots[j-1]+real_roots[j])/2.;
	  b = (real_roots[j]+real_roots[j+1])/2.;
	}
      
      // Count the number of roots
      int nroots = dc.Count<DEGREE>(a, b, coef, EPS, ndiv);
      bool flag = dc.QueryRoot<DEGREE>(a,b,coef,EPS,ndiv);
      assert((bool)nroots==flag);
      // Should find at least one root
      if(nroots<1)
	{
	  std::cout<<"\n";
	  for( int i=0; i<DEGREE+1; ++i)
	    std::cout<<coef[i]<<" ";
	  std::cout<<"\nSituation failed.\n"; std::fflush( stdout );
	}
      assert(nroots>=1);
    }
  return;
}

// Confirm the absence of roots
template< int DEGREE>
 int NegativelyConfirmRoots(const double* coef, const double EPS,
			    const double* real_roots, const  int nreal,
			    const  int nsubdiv)
{
   int nfalsepos = 0;
  
  // Root counter
  DesCartesPolyRootCounter dc;

  // Create intervals with no roots
  // and check that the DesCartes algorithm identifies zero roots
  double a, b;
  int ndiv = nsubdiv;
  
  // Multiple real roots
  for( int j=0; j<nreal; ++j)
    {
      // Create an isolated interval not including any roots
      if(j==0)
	{
	  a = real_roots[0]-2.;
	  b = real_roots[0]-1.;
	}
      else if(j==nreal-1)
	{
	  a = real_roots[nreal-1]+1.;
	  b = real_roots[nreal-1]+2.;
	}
      else
	{
	  a = 0.9*real_roots[j-1]+0.1*real_roots[j];
	  b = 0.1*real_roots[j-1]+0.9*real_roots[j];
	}
      
      // Count the number of roots
      int nroots = dc.Count<DEGREE>(a, b, coef, EPS, ndiv);
      bool flag = dc.QueryRoot<DEGREE>(a,b,coef,EPS,ndiv);
      assert((bool)nroots==flag);

      // Should find either 0 or an even number of roots
      // This is however not guaranteed.
      if(nroots%2!=0)
	{
	  std::cout<<"\n";
	  for( int i=0; i<DEGREE+1; ++i)
	    std::cout<<coef[i]<<" ";
	  std::cout<<"\nSituation failed.\n"; std::fflush( stdout );
	}
      
      // Count false positives
      if(nroots!=0)
	++nfalsepos;
    }
  return nfalsepos;
}


// Test for polynomial degree
template< int DEGREE>
void RandomPolynomialTests(const  int nRuns)
{
  // Randomly generate coefficients
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.,1.);

  double coefs[DEGREE+1];
  coefs[DEGREE] = 1.;

  // Exact solver (using gsl)
  double complex_roots[2*DEGREE];
  double real_roots[DEGREE];
  gsl_poly_complex_workspace* ws = gsl_poly_complex_workspace_alloc(DEGREE+1);
  
  // Tolerances
  const double EPS = 1.e-6;
  const  int nsubdiv = 4;

  // Random testing
  int nfalsepos = 0;
  int ncases = 0;
  for( int run=0; run<nRuns; ++run)
    {
      // Generate coefficients
      for( int i=0; i<DEGREE; ++i)
	coefs[i] = dis(gen);
      
      // Compute exact real roots
      auto success = gsl_poly_complex_solve(coefs, DEGREE+1, ws, complex_roots);
      assert(success==GSL_SUCCESS);

      // Identify real roots
      int nreal = 0;
      for( int i=0; i<DEGREE; ++i)
	if(std::abs(complex_roots[2*i+1])<EPS)
	  real_roots[nreal++] = complex_roots[2*i];

      // Sort the real roots
      std::sort(real_roots, real_roots+nreal);
      
      // Positive confirmation of roots
      PositivelyConfirmRoots<DEGREE>(coefs, EPS, real_roots, nreal, nsubdiv);
      
      // Confirm the absence of roots
      ncases += nreal;
      nfalsepos += NegativelyConfirmRoots<DEGREE>(coefs, EPS, real_roots, nreal, nsubdiv);
    }
  assert(ncases>nfalsepos*10); // Don't expect more than 10% false positives.

  //std::cout<<"\nDegree "<<DEGREE<<": "
  //<<nfalsepos<<" false positives among "<<ncases<<" cases";
  //std::fflush( stdout );
  return;
}
