
/** \file dvr_DesCartesPolyRootCounter.cpp
 * \brief Implementation of class dvr::DesCartesPolyRootCounter
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#include <dvr_DesCartesPolyRootCounter.h>

namespace dvr
{
  // // Count the number of sign changes in the coefficient list of a polynomial
  // int DesCartesPolyRootCounter::
  // CountSignChanges(const  int DEGREE, const double* coef,
  // 		   const double EPS, bool& SmallCoefFlag)
  // {
  //   int var = 0;
  //   for( int i=0; i<DEGREE; ++i)
  //     {
  // 	if(std::abs(coef[i])<EPS || std::abs(coef[i+1])<EPS)
  // 	  // If either coefficient is small, be conservative
  // 	  { ++var; SmallCoefFlag = true; }
  // 	else if(coef[i]>0. && coef[i+1]<0.) // (+-)
  // 	  ++var;
  // 	else if(coef[i]<0. && coef[i+1]>0. ) // (-+)
  // 	  ++var;
  // 	// Otherwise, both coefficients have the same sign.
  //     }
  //   return var;
  // }


  // Template specializations
  // Degree 3
  template<> void
  DesCartesPolyRootCounter::Transform<3>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    // Coefficients of the transformed polynomial
    P[0] = coef[0] + b*coef[1] + b*b*coef[2] + b*b*b*coef[3];
    P[1] = 3*coef[0] + a*coef[1] + 2*b*coef[1] + 2*a*b*coef[2] + b*b*coef[2] + 3*a*b*b*coef[3];
    P[2] = 3*coef[0] + 2*a*coef[1] + b*coef[1] + a*a*coef[2] + 2*a*b*coef[2] + 3*a*a*b*coef[3];
    P[3] = coef[0] + a*coef[1] + a*a*coef[2] + a*a*a*coef[3];
    return;
  }


  // Degree 4
  template<> void
  DesCartesPolyRootCounter::Transform<4>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    // Coefficients of the transformed polynomial
    P[0] = coef[0] + b*(coef[1] + b*(coef[2] + b*(coef[3] + b*coef[4])));

    P[1] = 4*coef[0] + b*(3*coef[1] + 2*b*coef[2] + b*b*coef[3]) + a*(coef[1] + 2*b*coef[2] + 3*b*b*coef[3] + 4*b*b*b*coef[4]);
    
    P[2] = 6*coef[0] + b*(3*coef[1] + b*coef[2]) + a*(3*coef[1] + 4*b*coef[2] + 3*b*b*coef[3]) + a*a*(coef[2] + 3*b*(coef[3] + 2*b*coef[4]));
    
    P[3] = 4*coef[0] + b*coef[1] + a*(3*coef[1] + 2*b*coef[2]) + a*a*(2*coef[2] + 3*b*coef[3]) + a*a*a*(coef[3] + 4*b*coef[4]);
    
    P[4] = coef[0] + a*(coef[1] + a*(coef[2] + a*(coef[3] + a*coef[4])));

    return;
  }

  // Degree 5:
  template<> void
  DesCartesPolyRootCounter::Transform<5>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    // Intermediate quantities
    const double a2 = a*a;
    const double b2 = b*b;
      
    // Coefficients of the transformed polynomial
    P[0] = coef[0] + b*(coef[1] + b*(coef[2] + b*(coef[3] + b*(coef[4] + b*coef[5]))));

    P[1] = 5*coef[0] + b*(4*coef[1] + 3*b*coef[2] + 2*b2*coef[3] + b*b2*coef[4]) + 
      a*(coef[1] + 2*b*coef[2] + 3*b2*coef[3] + 4*b*b2*coef[4] + 5*b2*b2*coef[5]);

    P[2] = 10*coef[0] + b*(6*coef[1] + 3*b*coef[2] + b2*coef[3]) + 
      a*(4*coef[1] + 6*b*(coef[2] + b*coef[3]) + 4*b*b2*coef[4]) + 
      a2*(coef[2] + 3*b*coef[3] + 6*b2*coef[4] + 10*b*b2*coef[5]);
      
    P[3] = 10*coef[0] + b*(4*coef[1] + b*coef[2]) + 3*a*(2*coef[1] + 2*b*coef[2] + b2*coef[3]) + 
      3*a2*(coef[2] + 2*b*(coef[3] + b*coef[4])) + a*a2*(coef[3] + 4*b*coef[4] + 10*b2*coef[5]);
      
    P[4] = 5*coef[0] + b*coef[1] + 2*a*(2*coef[1] + b*coef[2]) + 3*a2*(coef[2] + b*coef[3]) + 
      2*a*a2*(coef[3] + 2*b*coef[4]) + a2*a2*(coef[4] + 5*b*coef[5]);

    P[5] = coef[0] + a*(coef[1] + a*(coef[2] + a*(coef[3] + a*(coef[4] + a*coef[5]))));

    return;
  }

  // Degree 6:
  // Implementation
  template<>  void
  DesCartesPolyRootCounter::Transform<6>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    // Intermediate computations
    const double a2 = a*a;
    const double a3 = a2*a;
    const double b2 = b*b;
    const double b3 = b*b2;

    P[0] = coef[0] + b*(coef[1] + b*(coef[2] + b*(coef[3] + b*(coef[4] + b*(coef[5] + b*coef[6])))));
    
    P[1] =
      6*coef[0] + b*(5*coef[1] + 4*b*coef[2] + 3*b2*coef[3] + 2*b3*coef[4] + b2*b2*coef[5]) + 
      a*(coef[1] + 2*b*coef[2] + 3*b2*coef[3] + 4*b3*coef[4] + 5*b2*b2*coef[5] + 6*b2*b3*coef[6]);

    P[2] =
      15*coef[0] + b*(10*coef[1] + 6*b*coef[2] + 3*b2*coef[3] + b3*coef[4]) + 
      a*(5*coef[1] + 8*b*coef[2] + 9*b2*coef[3] + 8*b3*coef[4] + 5*b2*b2*coef[5]) + 
      a2*(coef[2] + b*(3*coef[3] + b*(6*coef[4] + 5*b*(2*coef[5] + 3*b*coef[6]))));
    
    P[3] =
      20*coef[0] + b*(10*coef[1] + 4*b*coef[2] + b2*coef[3]) + 
      a*(10*coef[1] + 12*b*coef[2] + 9*b2*coef[3] + 4*b3*coef[4]) + 
      a2*(4*coef[2] + b*(9*coef[3] + 2*b*(6*coef[4] + 5*b*coef[5]))) + 
      a3*(coef[3] + 2*b*(2*coef[4] + 5*b*(coef[5] + 2*b*coef[6])));

    P[4] =
      15*coef[0] + b*(5*coef[1] + b*coef[2]) + a*(10*coef[1] + 8*b*coef[2] + 3*b2*coef[3]) + 
      a2*(6*coef[2] + 9*b*coef[3] + 6*b2*coef[4]) + a3*(3*coef[3] + 8*b*coef[4] + 10*b2*coef[5]) + 
      a2*a2*(coef[4] + 5*b*(coef[5] + 3*b*coef[6]));

    P[5] =
      6*coef[0] + b*coef[1] + a*(5*coef[1] + 2*b*coef[2]) + a2*(4*coef[2] + 3*b*coef[3]) + 
      a3*(3*coef[3] + 4*b*coef[4]) + a2*a2*(2*coef[4] + 5*b*coef[5]) + a2*a3*(coef[5] + 6*b*coef[6]);

    P[6] = coef[0] + a*(coef[1] + a*(coef[2] + a*(coef[3] + a*(coef[4] + a*(coef[5] + a*coef[6])))));

    return;
  }

  // Degree 7:
  // Implementation
  template<>  void
  DesCartesPolyRootCounter::Transform<7>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    const double a2 = a*a;
    const double a3 = a2*a;
    const double b2 = b*b;
    const double b3 = b2*b;
    
    // Coefficients of transformed polynomial
    P[0] = coef[0] + b*(coef[1] + b*(coef[2] + b*(coef[3] + b*(coef[4] + b*(coef[5] + b*(coef[6] + b*coef[7]))))));
    
    P[1] =
      7*coef[0] + b*(6*coef[1] + 5*b*coef[2] + 4*b2*coef[3] + 3*b3*coef[4] + 2*b2*b2*coef[5] + b2*b3*coef[6]) +
      a*(coef[1] + 2*b*coef[2] + 3*b2*coef[3] + 4*b3*coef[4] + 5*b2*b2*coef[5] + 6*b2*b3*coef[6] + 7*b3*b3*coef[7]);

    P[2] =
      21*coef[0] + b*(15*coef[1] + 10*b*coef[2] + 6*b2*coef[3] + 3*b3*coef[4] + b2*b2*coef[5]) + 
      2*a*(3*coef[1] + 5*b*coef[2] + 6*b2*coef[3] + 6*b3*coef[4] + 5*b2*b2*coef[5] + 3*b2*b3*coef[6]) + 
      a2*(coef[2] + b*(3*coef[3] + b*(6*coef[4] + b*(10*coef[5] + 3*b*(5*coef[6] + 7*b*coef[7])))));

    P[3] =
      35*coef[0] + b*(20*coef[1] + 10*b*coef[2] + 4*b2*coef[3] + b3*coef[4]) + 
      a*(15*coef[1] + 20*b*coef[2] + 18*b2*coef[3] + 12*b3*coef[4] + 5*b2*b2*coef[5]) + 
      a2*(5*coef[2] + b*(12*coef[3] + b*(18*coef[4] + 5*b*(4*coef[5] + 3*b*coef[6])))) + 
      a3*(coef[3] + b*(4*coef[4] + 5*b*(2*coef[5] + 4*b*coef[6] + 7*b2*coef[7])));
    
    P[4] =
      35*coef[0] + b*(15*coef[1] + 5*b*coef[2] + b2*coef[3]) + 4*a*(5*coef[1] + 5*b*coef[2] + 3*b2*coef[3] + b3*coef[4]) + 
      2*a2*(5*coef[2] + 9*b*(coef[3] + b*coef[4]) + 5*b3*coef[5]) + 4*a3*(coef[3] + b*(3*coef[4] + 5*b*(coef[5] + b*coef[6]))) + 
      a2*a2*(coef[4] + 5*b*(coef[5] + 3*b*coef[6] + 7*b2*coef[7]));
    
    P[5] =
      21*coef[0] + b*(6*coef[1] + b*coef[2]) + a*(15*coef[1] + 10*b*coef[2] + 3*b2*coef[3]) + 2*a2*(5*coef[2] + 6*b*coef[3] + 3*b2*coef[4]) + 
      2*a3*(3*coef[3] + 6*b*coef[4] + 5*b2*coef[5]) + a2*a2*(3*coef[4] + 5*b*(2*coef[5] + 3*b*coef[6])) + a2*a3*(coef[5] + 6*b*coef[6] + 21*b2*coef[7]);

    P[6] =
      7*coef[0] + b*coef[1] + 2*a*(3*coef[1] + b*coef[2]) + a2*(5*coef[2] + 3*b*coef[3]) + 4*a3*(coef[3] + b*coef[4]) +
      a2*a2*(3*coef[4] + 5*b*coef[5]) + 2*a2*a3*(coef[5] + 3*b*coef[6]) + a3*a3*(coef[6] + 7*b*coef[7]);
    
    P[7] = coef[0] + a*(coef[1] + a*(coef[2] + a*(coef[3] + a*(coef[4] + a*(coef[5] + a*(coef[6] + a*coef[7]))))));
    
    return;
  }

  // Degree 8:
  // Implementation
  template<>  void
  DesCartesPolyRootCounter::Transform<8>(const double& a, const double& b,
					 const double* coef, double* P)
  {
    // Intermediate computations
    const double a2 = a*a;
    const double a3 = a2*a;
    const double a4 = a2*a2;

    const double b2 = b*b;
    const double b3 = b2*b;
    const double b4 = b2*b2;

    // Coefficients of transformed polynomial
    P[0] = coef[0] + b*(coef[1] + b*(coef[2] + b*(coef[3] + b*(coef[4] + b*(coef[5] + b*(coef[6] + b*(coef[7] + b*coef[8])))))));

    P[1] =
      8*coef[0] + b*(7*coef[1] + 6*b*coef[2] + 5* b2*coef[3] + 4*b3*coef[4] + 3*b4*coef[5] + 2*b2*b3*coef[6] + b3*b3*coef[7]) + 
      a*(coef[1] + 2*b*coef[2] + 3*b2*coef[3] + 4*b3*coef[4] + 5*b4*coef[5] + 6*b2*b3*coef[6] + 7*b3*b3*coef[7] + 8*b3*b4*coef[8]);

    P[2] =
      28*coef[0] + b*(21*coef[1] + 15*b*coef[2] + 10*b2*coef[3] + 6*b3*coef[4] + 3*b4*coef[5] + b2*b3*coef[6]) + 
      a*(7*coef[1] + 12*b*coef[2] + 15*b2*coef[3] + 16*b3*coef[4] + 15*b4*coef[5] + 12*b2*b3*coef[6] + 7*b3*b3*coef[7]) + 
      a2*(coef[2] + b*(3*coef[3] + b*(6*coef[4] + b*(10*coef[5] + b*(15*coef[6] + 7*b*(3*coef[7] + 4*b*coef[8]))))));

    P[3] =
      56*coef[0] + b*(35*coef[1] + 20*b*coef[2] + 10*b2*coef[3] + 4*b3*coef[4] + b4*coef[5]) + 
      3*a*(7*coef[1] + 10*b*coef[2] + 10*b2*coef[3] + 8*b3*coef[4] + 5*b4*coef[5] + 2*b2*b3*coef[6]) + 
      3*a2*(2*coef[2] + b*(5*coef[3] + b*(8*coef[4] + 10*b*(coef[5] + b*coef[6]) + 7*b3*coef[7]))) + 
      a3*(coef[3] + b*(4*coef[4] + 5*b*(2*coef[5] + 4*b*coef[6] + 7*b2*coef[7]) + 56*b4*coef[8]));

    P[4] =
      70*coef[0] + 5*b*(7*coef[1] + 3*b*coef[2] + b2*coef[3]) + b4*coef[4] + 
      a*(35*coef[1] + 40*b*coef[2] + 30*b2*coef[3] + 16*b3*coef[4] + 5*b4*coef[5]) + 
      3*a2*(5*coef[2] + 2*b*(5*coef[3] + 6*b*coef[4] + 5*b2*coef[5]) + 5*b4*coef[6]) + 
      a3*(5*coef[3] + b*(16*coef[4] + 5*b*(6*coef[5] + 8*b*coef[6] + 7*b2*coef[7]))) + 
      a4*(coef[4] + 5*b*(coef[5] + b*(3*coef[6] + 7*b*(coef[7] + 2*b*coef[8]))));
      
    P[5] =
      56*coef[0] + b*(21*coef[1] + 6*b*coef[2] + b2*coef[3]) + 
      a*(35*coef[1] + 30*b*coef[2] + 15*b2*coef[3] + 4*b3*coef[4]) + 
      2*a2*(10*coef[2] + 15*b*coef[3] + 12*b2*coef[4] + 5*b3*coef[5]) + 
      2*a3*(5*coef[3] + b*(12*coef[4] + 5*b*(3*coef[5] + 2*b*coef[6]))) + 
      a4*(4*coef[4] + 5*b*(3*coef[5] + 6*b*coef[6] + 7*b2*coef[7])) + 
      a2*a3*(coef[5] + b*(6*coef[6] + 7*b*(3*coef[7] + 8*b*coef[8])));

    P[6] =
      28*coef[0] + b*(7*coef[1] + b*coef[2]) + 3*a*(7*coef[1] + 4*b*coef[2] + b2*coef[3]) + 
      3*a2*(5*coef[2] + 5*b*coef[3] + 2*b2*coef[4]) + 
      2*a3*(5*coef[3] + 8*b*coef[4] + 5*b2*coef[5]) + 3*a4*(2*coef[4] + 5*b*(coef[5] + b*coef[6])) + 
      3*a2*a3*(coef[5] + 4*b*coef[6] + 7*b2*coef[7]) + a3*a3*(coef[6] + 7*b*(coef[7] + 4*b*coef[8]));

    P[7] =
      8*coef[0] + b*coef[1] + a*(7*coef[1] + 2*b*coef[2]) + 3*a2*(2*coef[2] + b*coef[3]) + 
      a3*(5*coef[3] + 4*b*coef[4]) + a4*(4*coef[4] + 5*b*coef[5]) + 3*a2*a3*(coef[5] + 2*b*coef[6]) + 
      a3*a3*(2*coef[6] + 7*b*coef[7]) + a3*a4*(coef[7] + 8*b*coef[8]);

    P[8] = coef[0] + a*(coef[1] + a*(coef[2] + a*(coef[3] + a*(coef[4] + a*(coef[5] + a*(coef[6] + a*(coef[7] + a*coef[8])))))));

    return;
  }

}
