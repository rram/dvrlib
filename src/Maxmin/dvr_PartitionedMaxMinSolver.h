
/** \file dvr_PartitionedMaxMinSolver.h
 * \brief Definition of the class dvr::PartitionedMaxMinSolver.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <dvr_MaxMinSolverDetails.h>

namespace dvr{  
  /** \brief Class implementing a univariate max-min solver to compute optimal vertex perturbations.
   *
   * The main functionality of this class is to resolve problems of the form
   * \f[ \text{Find}~\lambda^\text{opt}\in \arg\max_{\lambda\in 
   * {\mathbb R}}\min\{{\rm Q}(K^\lambda)\,:\,K^\lambda\in \stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\},\f]
   * where:
   * <ul>
   * <li> \f$i\in {\rm I}_{\rm R}\f$ is a vertex in the triangulation \f${\cal T}\f$ to be relaxed</li>
   * <li> \f${\cal T}^{i,\lambda}\f$ is the mesh realized by perturbing vertex \f$i\f$ along a prescribed
   * direction by the coordinate \f$\lambda\f$ </li>
   * <li> \f$K^\lambda\in {\cal T}^{i,\lambda}\f$ is the element corresponding to \f$K\in {\cal T}\f$,
   * i.e., \f$K^\lambda\f$ is the image of \f$K\f$ when the vertex \f$i\f$ is perturbed by the 
   * coordinate \f$\lambda\f$
   * <li> \f$\stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\f$ denotes the list of elements 
   * incident at the vertex \f$i\f$</li>
   * <li> \f${\rm Q}\f$ is the element quality metric. </li>
   * </ul>
   * 
   * In the following, it is convenient to use the following notation:
   * \f[ f_\alpha(\lambda) = {\rm Q}(K_\alpha^\lambda)~\text{for}~\{K_1,K_2,\ldots,K_n\}\in \stackrel{\star}{e}(i,{\cal T}).\f]
   * \f[ F(\lambda) = \min\{f_1(\lambda),f_2(\lambda),\ldots,f_n(\lambda)\}\f]
   *
   * The class hence implements a solver for the problem 
   * \f[\text{Find}~\lambda_{\rm opt} = \arg\max_{\lambda\in{\mathbb R}} {\rm F}(\lambda).\f]
   * 
   * The main challenge in resolving the maximization problem above, despite its one-dimensional nature, 
   * stems from the non-smoothness of \f$F\f$. 
   * Specifically, the quality metric \f${\rm Q}\f$ is expected to be a smooth function of  
   * vertex coordinates. Consequently, each function \f$f_1,\ldots, f_n\f$ is expected to be a smooth function,.
   * However, \f$\lambda\mapsto F(\lambda)\f$, as a minimum among \f$n\f$ smooth functions, 
   * is in general only continuous, and fails to be differentiable everywhere. Hence, identifying
   * the maximizer \f$\lambda^{\rm opt}\f$ is a non trivial problem. Commonly used  Newton-type methods do not suffice.
   * 
   * This class implements Algorithm 1 from the article:
   * <br>
   * R Rangarajan, <em>On the resolution of certain discrete univariate max–min problems,</em>
   * Computational Optimization and Applications (2017), 68 (1), 163-192 <a href="https://doi.org/10.1007/s10589-017-9903-z">(doi)</a>.
   * <br>
   * A discussion of the algorithm can also be found in Section 3.2 of
   * <br>
   * R Rangarajan and A Lew, <em>Provably robust directional vertex relaxation for geometric mesh optimization</em>,
   * SIAM Journal on Scientific Computing 39 (6), A2438-A2471, 2017 
   * <a href="https://doi.org/10.1137/16M1089101">(doi)</a>.
   * <br>
   * The following conditions on the functions
   * \f$\{f_\alpha\}_{1\leq \alpha\leq n}\f$ suffice for the algorithm
   * to correctly determine \f$\lambda^{\rm opt}\f$:
   * <ul>
   * <li> (Smoothness) \f$f_\alpha\f$ is differentiable everywhere. </li>
   * <li> (Decay) \f$f_\alpha(\lambda)\rightarrow 0\f$ as \f$|\lambda|\rightarrow \infty\f$ </li>
   * <li> (Non-duplicity) \f$f_\alpha = f_\beta\f$ if and only if \f$\alpha=\beta\f$.</li>
   * <li> (Finite number of intersections) Each pair of distinct functions \f$f_\alpha\f$ and \f$f_\beta\f$ 
   * intersect at finitely many points. </li>
   * <li> (Finite number of local maxima) \f$f_\alpha\f$ has finitely many local maxima.</li>
   * <li> (Finiteness of local maximizers) Local maximizers of \f$f_\alpha\f$ are bounded away from \f$\pm \infty\f$. </li>
   * </ul>
   *
   * 
   * These conditions are guaranteed to be satisfied if:
   * <ul>
   * <li> \f${\rm Q}\f$ is chosen to be the mean ratio metric (for triangles and tets). This ensures
   * smoothness of functions, finitely many maxima, finitely many intersection of distinct functions
   * and decay at infinity. See Proposition 3 in the first article cited above. </li>
   * <li> All perturbable elements in the mesh, i.e., in the set 
   * \f$\cup_{i\in {\rm I}_{\rm R}}\stackrel{\star}{e}(i,{\cal T})\f$ have strictly positive
   * qualities. This ensures that maximizers do not include \f$\pm\infty\f$. </li>
   * <li> Functions in the list
   * \f$\{\lambda\mapsto f_\alpha(\lambda)\}_{\alpha}\f$ are uniquated. 
   * This is required to satisfy the non-duplicity condition.
   * <br>
   * Specifically, it is possible that \f$K_\alpha,K_\beta\f$ are distinct elements in the 
   * 1-ring of a vertex \f$i\in {\rm I}_{\rm R}\f$, but their quality curves coincide, i.e., 
   * \f$f_\alpha=f_\beta\f$  for \f$\lambda\in {\mathbb R}\f$.
   * This can happen, for instance, if the elements \f$K_\alpha\f$ and \f$K_\beta\f$
   * are congruent elements and the relaxation direction is chosen such that 
   * \f$K_\alpha^\lambda\f$ and \f$K_\beta^\lambda\f$ remain congruent as a common
   * vertex is perturbed. 
   * <br>
   * A "duplication" in the list of functions \f$\{f_\alpha\}_\alpha\f$
   * is inconsequential in the definition of the optimizer \f$\lambda^{\rm opt}\f$.
   * The algorithm, however, requires that only a unique copy of each function in the list
   * be retained.
   * </li>
   * </ul>
   *
   *   
   * The main idea behind te algorithm consists in 
   * restricting the search for \f$\lambda^{\rm opt}\f$
   * to the set
   * \f[ \Lambda = \Lambda_{\rm max} \cup \Lambda_{\rm int},\f]
   * where
   * \f$ \Lambda_{\rm max} = \cup_{1\leq \alpha\leq n}\{\arg\max_\lambda f_\alpha(\lambda)\}\f$
   * is the collection of all maximizers of the element quality curves, and
   * \f$ \Lambda_{\rm int} = \cup_{1\leq \alpha<\beta \leq n} 
   \{\lambda\in{\mathbb R}\,:\,f_\alpha(\lambda)=f_\beta(\lambda)\}\f$
   * is the set of all pairwise intersections of element quality curves.
   *
   *
   * Algorithmically, the main benefit of restricting the search for
   * \f$\lambda^{\rm opt}\f$ is realized when the set \f$\Lambda\f$ is
   * finte.  When using the mean ratio metric, it is straightforward
   * to verify that both \f$\Lambda_{\rm max}\f$ and \f$\Lambda_{\rm
   * int}\f$ are finite, with the latter additionally requiring that 
   * the functions \f$\{f_\alpha\}_\alpha\f$ are all distinct.
   *
   * The complexity of the algorithm is dominated by the determination
   * of the set \f$\Lambda_{\rm int}\f$, which is expected to scale as 
   * \f$n^2\f$, where \f$n\f$ represents the number of
   * elements in the 1-ring of the vertex being relaxed.
   * In comparison, the cost of determining
   * the set \f$\Lambda_{\rm max}\f$ is expected to scale with \f$n\f$.
   *  In the case
   * of tet meshes, the number \f$n\f$, which depends on the meshing
   * algorithm used, can be quite large (e.g., \f$n\f$ exceeds 35 in
   * most of the benchmarking examples used for this library). For
   * such values, the algorithm becomes quite expensive to use.  It
   * may even be beneficial to simple compute a suboptimal
   * perturbation by resorting to sampling-based strategies.
   *
   * The main functionality of the class is the dvr::PartitionedMaxMinSolver::Solve method,
   * which, for a given vertex and relaxation direction, 
   * uses detailed information from the quality metric to evaluate the functions
   * \f$f_\alpha\f$, their extrema (local maxima) and the set of pairwise intersections. 
   * Then, \f$\lambda^{\rm opt}\f$
   * is determined as one among the (finitely many) points in \f$\Lambda\f$ by evaluating the function 
   * \f$F\f$.
   *
   * The class is thread safe. It creates as many copies of local data structures as
   * the number of threads specified during construction.  The number of threads used
   * during mesh optimization cannot exceed this number.
   *
   * The class is templated by the type of element quality metric.
   * \tparam QType typename of the element quality metric.
   * 
   * \see dvr::ReconstructiveMaxMinSovler for an alternative and more efficient max-min solver.
   * \see testVertexOptimize.cpp: Unit test for this class.
   * \note Unlike the dvr::ReconstructiveMaxMinSolver class, this does not use tolerances.
   * \note It is expected that this solver will work for a broader class of 
   * quality metrics than the dvr::ReconstructiveMaxMinSolver.
   *
   * \ingroup dvr_maxmin_solvers
   */ 
  template<typename QType>
    class PartitionedMaxMinSolver
    {
    public:
      //! Constructor
      //! \param[in] quality Quality metric, referenced.
      //! \param[in] nthreads Number of threads. default: 1, Used to allocate thread-safe local data structures.
      PartitionedMaxMinSolver(QType& quality, const int nthreads=1);
      
      //! Copy constructor
      //! \param[in] Obj Object to be copied from
      PartitionedMaxMinSolver(const PartitionedMaxMinSolver<QType>& Obj);
      
      //! Destructor
      inline virtual ~PartitionedMaxMinSolver() {}

      //! Cloning
      inline virtual PartitionedMaxMinSolver<QType>* Clone() const
      { return new PartitionedMaxMinSolver<QType>(*this); }
      
      //! Returns a reference to the mesh quality
      inline QType& GetMeshQuality()
      { return Quality; }

      /** \brief Computes the optimal vertex perturbation \f$\lambda^\text{opt}\in \arg\max_{\lambda\in {\mathbb R}}\min\{{\rm Q}(K)\,:\,K\in \stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\}\f$.
       * \param[in] vert Index of vertex to be optimized
       * \param[in] rdir Relaxation direction for <tt>vert</tt>. Should have unit norm.
       * \param[in] usrparams User defined parameters specific to the implementation. Unused.
       * \return Computed optimal perturbation coordinate \f$\lambda^{\rm opt}\f$.
       */
      double Solve(const int vert,  double* rdir,
		   void* usrparams=nullptr) const;
      
      //! Returns the maximum number of threads set at initialization
      inline int GetMaxNumThreads() const
      { return Quality.GetMaxNumThreads(); }
      
    private:
      QType& Quality; //!< Mesh quality object
      const detail::MaxMinSolverSpecs SolSpecs; //! Solve specifications
      mutable std::vector<detail::PartitionData> ThrPartData; //!< Thread-safe access to reconstruction data
      
      //! Provides thread-safe access to reconstruction-data
      detail::PartitionData& GetLocalData() const;
    };
}


// Implementation
#include <dvr_PartitionedMaxMinSolver_Impl.h>


