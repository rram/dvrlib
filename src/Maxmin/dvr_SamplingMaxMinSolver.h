
/** \file dvr_SamplingMaxMinSolver.h
 * File defining the class dvr::SamplingMaxMinSolver
 * \author Ramsharan Rangarajan
 * \date April 02, 2020.
 */

#pragma once

#include <dvr_MaxMinSolverDetails.h>
#include <dvr_BoundaryRelaxation.h>
#include <functional>

namespace dvr{
  
  /** \brief Class implementing a univariate max-min solver based on sampling to compute suboptimal vertex perturbations.
   *
   * This class is provided to resolv problems of the form 
   * \f[ \text{Find}~\lambda^\text{opt}\in \arg\max_{\lambda\in 
   * {\mathbb R}}\min\{{\rm Q}(K^\lambda)\,:\,K^\lambda\in \stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\},\f]
   * where:
   * <ul>
   * <li> \f$i\in {\rm I}_{\rm R}\f$ is a vertex in the triangulation \f${\cal T}\f$ to be relaxed</li>
   * <li> \f${\cal T}^{i,\lambda}\f$ is the mesh realized by perturbing vertex \f$i\f$ along a prescribed
   * direction by the coordinate \f$\lambda\f$ </li>
   * <li> \f$K^\lambda\in {\cal T}^{i,\lambda}\f$ is the element corresponding to \f$K\in {\cal T}\f$,
   * i.e., \f$K^\lambda\f$ is the image of \f$K\f$ when the vertex \f$i\f$ is perturbed by the 
   * coordinate \f$\lambda\f$
   * <li> \f$\stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\f$ denotes the list of elements 
   * incident at the vertex \f$i\f$</li>
   * <li> \f${\rm Q}\f$ is the element quality metric. </li>
   * </ul>
   * 
   * In the following, it is convenient to use the following notation:
   * \f[ f_\alpha(\lambda) = {\rm Q}(K_\alpha^\lambda)~\text{for}~\{K_1,K_2,\ldots,K_n\}\in \stackrel{\star}{e}(i,{\cal T}).\f]
   * \f[ F(\lambda) = \min\{f_1(\lambda),f_2(\lambda),\ldots,f_n(\lambda)\}\f]
   *
   * The class hence implements a solver for the problem 
   * \f[\text{Find}~\lambda_{\rm opt} = \arg\max_{\lambda\in{\mathbb R}} {\rm F}(\lambda).\f]
   * 
   * The main challenge in resolving the maximization problem above, despite its one-dimensional nature, 
   * stems from the non-smoothness of \f$F\f$. 
   * Specifically, the quality metric \f${\rm Q}\f$ is expected to be a smooth function of  
   * vertex coordinates. Consequently, each function \f$f_1,\ldots, f_n\f$ is expected to be a smooth function,.
   * However, \f$\lambda\mapsto F(\lambda)\f$, as a minimum among \f$n\f$ smooth functions, 
   * is in general only continuous, and fails to be differentiable everywhere. Hence, identifying
   * the maximizer \f$\lambda^{\rm opt}\f$ is a non trivial problem. Commonly used  Newton-type methods do not suffice.
   * 
   * This class implements an algorithm that consists in computing a suboptimal
   * vertex perturbation by restricting the set of candidate optimizers to a 
   * discrete set. Specifically, it computes
   * \f[ \lambda^{\rm sub\,{\rm opt}} = \arg\max_{\lambda\in\Lambda}F(\lambda), \f]
   * where \f$\Lambda\f$ is a discrete set of points that uniformly samples the 
   * interval \f$[-h,h]\f$ using a specified number of sampling points, 
   * where \f$h\f$ denoting a measure of the local mesh size.
   *
   * The class is thread safe. It creates as many copies of local data structures as
   * the number of threads specified during construction.  The number of threads used
   * during mesh optimization cannot exceed this number.
   *
   * The class is templated by the type of element quality metric.
   * \tparam QType typename of the element quality metric.
   * 
   * \see dvr::PartitionedMaxMinSovler and dvr::ReconstructiveMaxMinSolver for alternative max-min solvers.
   *
   * \ingroup dvr_maxmin_solvers
   */

  template<typename QType>
    class SamplingMaxMinSolver
    {
    public:
      //! Constructor
      //! \param[in] quality Instance of a quality metric
      //! \param[in] nsamples Number of sampling points to evaluate 
      //! \param[in] nthreads Max number of omp threads. Used to size local data structures for thread-safe access. Default: 1
      SamplingMaxMinSolver(QType& quality,
			   const int nsamples,
			   const int nthreads=1);
      
      //! Destructor
      inline virtual ~SamplingMaxMinSolver() {}

      //! Copy constructor
      //! \param[in] Obj Object to be copied from
      //! \todo May have to perform a deep copy here
      SamplingMaxMinSolver(const SamplingMaxMinSolver<QType>& Obj);

      //! Cloning
      inline virtual SamplingMaxMinSolver<QType>* Clone() const
      { return new SamplingMaxMinSolver<QType>(*this); }

      //! Returns the max number of threads
      inline int GetMaxNumThreads() const
      { return nMaxThreads; }

      //! Returns the mesh quality
      inline QType& GetMeshQuality()
      { return Quality; }

      //! Main functionality
      //! Can be specialized for specific quality metrics
      //! \param[in] vert Vertex to be optimized
      //! \param[in,out] rdir Relaxation direction for vertex. Can be changed on return
      //! \param[in] usrparams User defined parameters specific to the implementation.
      //! \return Computed optimal perturbation coordinate
      double Solve(const int vert,  double* rdir, void* usrparams=nullptr) const;

    private:
      // Members
      QType& Quality; //!< Mesh object
      const int nSamples; //!< Number of samples to use
      const int nMaxThreads; //!< Max. number of threads
      mutable std::vector<detail::RingData> ThrRingData; //! Thread safe access to local data

      //! Provides thread-safe access to local data structures
      detail::RingData& GetLocalRingData() const;
     
    };  
}


// Implementation
#include "dvr_SamplingMaxMinSolver_Impl.h"
