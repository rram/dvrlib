
/** \file dvr_ReconstructiveMaxMinSolver_Impl.h
 * \brief Implementation of the class dvr::ReconstructiveMaxMinSolver.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <dvr_ParamInterval.h>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <dvr_assert.h>
#include <dvr_QualityTraits.h>
#include <dvr_MaxMinSolverTraits.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  // Constructor
  template<typename QType> ReconstructiveMaxMinSolver<QType>::
    ReconstructiveMaxMinSolver(QType& quality, const int nthreads)
    :Quality(quality),
    SolSpecs{.nMaxThreads=nthreads,
      .nExtrema=quality.GetNumExtrema(),
      .nIntersections=quality.GetNumIntersections()},
    ThrReconData(SolSpecs.nMaxThreads)
      {
	// Check quality trauts
	static_assert(CheckQualityTraits<QType, typename std::remove_reference<decltype(std::declval<QType>().GetMesh())>::type>(),
		      "Quality class failed to satisfy trait requirements");
	
	// Check max-min solver traits
	static_assert(CheckMaxMinSolverTraits<ReconstructiveMaxMinSolver<QType>>(), "Max-min solver failed to satisfy requirements");
      
	// Check solver specs
	assert((SolSpecs.nMaxThreads>0 && SolSpecs.nExtrema>0 && SolSpecs.nIntersections>0) &&
	       "dvr::ReconstructiveMaxMinSolver- Unexpected solver specs.");

	// Size local data
	const auto& MD = Quality.GetMesh();
	const auto& mesh_wrapper = Quality.GetMeshWrapper();
	const auto ElmValency = mesh_wrapper.GetMaxElementValency();
	for(int thr=0; thr<SolSpecs.nMaxThreads; ++thr)
	  {
	    auto& MyReconData = ThrReconData[thr];

	    // Resize RingData structures
	    auto& ringdata = MyReconData.RD;
	    ringdata.LocalNodeNums.resize(ElmValency);

	    // Each quality curve needs extremizers
	    auto& Extremizers = MyReconData.Extremizers;
	    Extremizers.resize(ElmValency);
	    for(auto& ext:Extremizers)
	      {
		ext.tcoord.resize(SolSpecs.nExtrema);
		ext.qvals.resize(SolSpecs.nExtrema);
	      }
	    MyReconData.InitExtremizers.resize(ElmValency);

	    // Data for pairs of intersections
	    auto& Intersections = MyReconData.Intersections;
	    Intersections.resize( ElmValency*ElmValency );
	    for(auto& it:Intersections)
	      {
		it.tcoord.resize(SolSpecs.nIntersections);
		it.qvals.resize(SolSpecs.nIntersections);
	      }
	    MyReconData.InitIntersections.resize(ElmValency*ElmValency);

	    // Data for inverse computations
	    MyReconData.InverseSet.resize(SolSpecs.nIntersections);
	  }
      }

  // Copy constructor
  template<typename QType>
    ReconstructiveMaxMinSolver<QType>::
    ReconstructiveMaxMinSolver(const ReconstructiveMaxMinSolver<QType>& Obj)
    :Quality(Obj.Quality),
    SolSpecs{.nMaxThreads=Obj.SolSpecs.nMaxThreads,
	.nExtrema=Obj.SolSpecs.nExtrema,
	.nIntersections=Obj.SolSpecs.nIntersections},
    ThrReconData(Obj.ThrReconData) {}
  
  // Provides thread-safe access to reconstruction data
    template<typename QType>
      detail::ReconData&
      ReconstructiveMaxMinSolver<QType>::GetLocalReconstructionData() const
      {
	int ThrNum = 0;
#ifdef _OPENMP
	ThrNum = omp_get_thread_num();
#endif
	assert(ThrNum<SolSpecs.nMaxThreads && 
	       "dvr::ReconstructiveMaxMinSolver::GetLocalReconstructionData- Exceeded max number of threads.");
	return ThrReconData[ThrNum];
      }
  
  namespace detail
  {
    // Level set of the min{Q} function
    // \param[in] RD 1-ring data for the vertex
    // \param[in] rdir Relaxation direction
    // \param[in] EPS Tolerance to use
    // \param[in] Quality Metric to be used
    // \param[in] qval Quality to use for bounding calculation
    // \param[out] tMin Lower bound for parametric coordinate
    // \param[out] tMax Upper bound for parametric coordinate
    // Assumes that all quality curves intersect the qval-level.
    template<typename QType>
      void ComputeQ0LevelSet(QType& Quality, const double* rdir,  
			     const detail::RingData& RD, 
			     const double EPS,
			     double* lambda,
			     double& tMin, double& tMax)
      {
	// Number of roots of quality curves
	int nlambda;

	// Intermediate computations
	double qplus, qminus;

	// Flags that track if bounds have been computed
	bool flagMin = false, flagMax = false;
      
	const auto& My1RingElms = Quality.GetMeshWrapper().Get1RingElements(RD.vertex);
	const int n1RingElms    = static_cast<int>(My1RingElms.size());
	const auto& LocalNodeNums = RD.LocalNodeNums;

	// Compute the quality Q(0)
	double Q0 = Quality.Compute(My1RingElms[0]);
	int indx = 0;
	for( int i=1; i<n1RingElms; ++i)
	  {
	    qplus = Quality.Compute(My1RingElms[i]); // qplus is just a dummy variable here
	    if(qplus<Q0)
	      { Q0 = qplus; indx = i; }
	  }
      
	// Compute the level set Q = Q0.
	// Exploit the fact that each quality curve necessarily intersects the level Q0.
      
	// Start from the element #indx, which automatically sets tMin or tMax to 0.
	detail::ParamInterval param;
	auto* tinterval = param.tinterval;
	for( int i=0; i<n1RingElms; ++i)
	  {
	    const  int elmindx = (i+indx)%n1RingElms; // This element index
	    param.flag = true;
	   
	    // Compute the level set of this element
	    if(flagMin==false || flagMax==false)
	      // Not possible to bracket. Compute the level set
	      Quality.Invert(My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, Q0, lambda, nlambda);
	    else 
	      {
		// Bracket available. Use it.
		tinterval[0] = tMin;
		tinterval[1] = tMax;
		Quality.Invert(My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, Q0, lambda, nlambda, &param);
		// param.flag is returned as false of roots were not computed.
	      }

	    // Update bounds only if roots have been computed.
	    if(param.flag==true) 
	      {
		//  There should be exactly 0, 1 or 2 roots.
		dvr_assert( (nlambda==0 || nlambda==1 || nlambda==2) &&
			    "ReconstructiveMaxMinSolver::ComputeQ0LevelSet()- Unexpected number of roots.");
	       
		// If no roots are found, it is because Q0 is a maximum of this quality curve
		// and t = 0 because of approximate computation
		if(nlambda==0)
		  {
		    tMin = 0.; tMax = 0.;	  
		   
		    // Sanity check: t = 0 should be the maximum of Q(t)
		    qplus = Quality.Compute (My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, EPS);
		    qminus = Quality.Compute (My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, -EPS);
		    if(qplus>Q0 || qminus>Q0)
		      {
			std::cout<<"\nProblem case: Found zero roots.\n";
			std::cout<<"\nQuality at 0: "<<Quality.Compute(My1RingElms[indx])<<" should be close to "<<Q0;
			std::cout<<"\nOrdering: "<<qminus<<", "<<Q0<<", "<<qplus; std::fflush( stdout );
			std::cout<<"\nElement coordinates: ";
			const auto& MD = Quality.GetMesh();
			const auto* conn = MD.connectivity(My1RingElms[indx]);
			for(int a=0; a<4; ++a)
			  {
			    const auto* X = MD.coordinates(conn[a]);
			    std::cout<<"\n"<<X[0]<<", "<<X[1]<<" ,"<<X[2];
			  }
			std::cout<<"\nPerturbed node: "<<LocalNodeNums[elmindx]; 
			std::fflush( stdout ); return;
		      }
		    dvr_assert( (Q0>qplus && Q0>qminus) &&
			    "ReconstructiveMaxMinSolver::ComputeQ0LevelSet()- Expected a maximum.");
		    return;
		  }
		else if(nlambda==1) // If there is only one root, which should be equal to 0.
		  {
		    // Examine the neighborhood of t = 0
		    qplus = Quality.Compute (My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, EPS);
		    qminus = Quality.Compute (My1RingElms[elmindx], LocalNodeNums[elmindx], rdir, -EPS);
		   
		    // Is t = 0 a maximum
		    if(Q0>qplus && Q0>qminus)
		      { tMin = lambda[0]; tMax = lambda[0]; return; }
		    // Is tMin = 0 ?
		    else if(qplus>Q0 && qminus<Q0)
		      { tMin = 0.; flagMin = true; }
		    // Is tMax = 0 ?
		    else if(qplus<Q0 && qminus>Q0)
		      { tMax = 0.; flagMax = true; }
		    else
		      dvr_assert(false && "ReconstructiveMaxMinSolver::ComputeQ0LevelSet()- Unexpected situation with one root.");
		  }
		else // There are two roots
		  {
		    // Order the roots
		    if(lambda[0]>lambda[1])
		      std::swap(lambda[0], lambda[1]);
		   
		    // Update the lower bound
		    if(flagMin==false )
		      { tMin = lambda[0]; flagMin = true; }
		    else if(lambda[0]>tMin )
		      tMin = lambda[0];
		   
		   
		    // Update the upper bound
		    if(flagMax==false )
		      { tMax = lambda[1]; flagMax = true; }
		    else if( lambda[1]<tMax )
		      tMax = lambda[1];
		  }
	       
		// If the bounds are close enough, return the coordinate range as it is
		if( flagMin && flagMax && std::abs(tMax-tMin)<EPS )
		  return;
	      }
	  }
      
     
	// Ensure that bounds have been computed
	dvr_assert( (flagMin && flagMax) &&
		"ReconstructiveMaxMinSolver::ComputeQ0LevelSet()- Could not compute bounds.");
	return;
      }
     	    


    // Compute the first active function
    // \param[in] RD 1-ring data for this vertex
    // \param[in] rdir Relaxation direciton
    // \param[in] EPS Tolerance to be used
    // \param[in] side Reconstruction side
    // \param[out] Kink Computed kink information
    template<typename QType>
      void ComputeFirstKink(const QType& QM, const detail::RingData& RD,
			    const double* rdir, const SIDE side,
			    const double EPS,
			    detail::KinkData& Kink)
      {
	const auto& My1RingElms = QM.GetMeshWrapper().Get1RingElements(RD.vertex);
	const int n1RingElms = static_cast<int>(My1RingElms.size());
	const auto& LocalNodeNums = RD.LocalNodeNums;
  
	// This kink is located at t = 0
	Kink.tcoord = 0.;

	// Initialize the first active function to be element 0
	double qval = QM.Compute(My1RingElms[0]);
	Kink.qval = qval;
	Kink.ActiveFunc = 0;

	// Advancement direction
	const double sign = (side==SIDE::Left) ? -1. : 1.;
	
	// Loop over remaining elements
	for( int i=1; i<n1RingElms; ++i)
	  {
	    // Compute the quality of this element
	    qval = QM.Compute(My1RingElms[i], LocalNodeNums[i], rdir, 0.);

	    // Do we have a new active function
	    if(qval<Kink.qval-EPS)
	      {
		Kink.qval = qval;
		Kink.ActiveFunc = i;
	      }
	    // Resolve a close call by comparing qualities at a slightly perturbed distance
	    else if(std::abs(qval-Kink.qval)<EPS)
	      {
		// Identify the active function be examining their values at a slightly perturbed distance
		if( QM.Compute(My1RingElms[i], LocalNodeNums[i], rdir, sign*EPS) <
		    QM.Compute(My1RingElms[Kink.ActiveFunc], LocalNodeNums[Kink.ActiveFunc], rdir, sign*EPS)) 
		  {
		    // We have a new active function
		    Kink.qval = qval;
		    Kink.ActiveFunc = i;
		  }
	      }
	  }
	return;
      }


    // Computes the next active function
    // \param[in] QM Quality metric
    // \param[in,out] MyReconData reconstruction data for this thread. Intersections get updated.
    // \param[in] rdir Relaxation direction
    // \param[in] tExt Extreme value of coordinate range
    // \param[in] PrevKink Currently known kink
    // \param[out] NextKink Kink to be computed
    // \param[in] EPS Tolerance to be used
    // \param[in] ElmValency Valency of the vertex being optimized
    // \param[in] side Direction along which to look for the next kink
    // \return True if the extreme has been reached, and false otherwise.
    template<typename QType>
      void ComputeNextKink(const QType& QM, detail::ReconData& MyReconData,
			   const double* rdir, const double tExt,
			   const  int& ElmValency,
			   const SIDE side,
			   detail::KinkData& PrevKink,
			   detail::KinkData& NextKink,
			   const double EPS)
      {
	// 1-ring data
	const auto& RD = MyReconData.RD;
	const auto& My1RingElms = QM.GetMeshWrapper().Get1RingElements(RD.vertex);
	const int n1RingElms = static_cast<int>(My1RingElms.size());
	const auto& LocalNodeNums = RD.LocalNodeNums;
  
	// If the existing kink is sufficiently close to the extreme value, return
	if(std::abs(PrevKink.tcoord-tExt)<EPS)
	  {
	    NextKink.tcoord = tExt;
	    NextKink.qval = QM.Compute(My1RingElms[PrevKink.ActiveFunc],
				       LocalNodeNums[PrevKink.ActiveFunc],
				       rdir, tExt);
	    NextKink.ActiveFunc = PrevKink.ActiveFunc;
	    return;
	  }
  
	// Access intersection data
	auto& InitIntersections = MyReconData.InitIntersections;
	auto& Intersections = MyReconData.Intersections;

	// PrevKink details
	const auto PrevActiveFunc = PrevKink.ActiveFunc;
	const auto PrevActiveElm = My1RingElms[PrevActiveFunc];
	const auto PrevActiveNode = RD.LocalNodeNums[PrevActiveFunc];

	// Initialize the next kink presuming that the current function remains active until tExt
	NextKink.tcoord = tExt;
	NextKink.ActiveFunc = PrevActiveFunc;
	NextKink.qval = QM.Compute(My1RingElms[PrevKink.ActiveFunc],
				   LocalNodeNums[PrevKink.ActiveFunc],
				   rdir, tExt);
	  
	// Interval in which to look for intersections
	detail::ParamInterval param;
	auto* tinterval = param.tinterval;

	// Encode side information through a sign
	const double sign = (side==SIDE::Left) ? -1. : 1.;
  
	// Sorted indices when computing intersections
	int indx1, indx2; // Convention: indx1 < indx2

	for( int i=0; i<n1RingElms; ++i)
	  if(i!=PrevActiveFunc) // Don't intersect a function with itself
	    {
	      // Compute the intersection between functions #PrevActiveFunc and #i
	      if(i<PrevActiveFunc)
		{ indx1 = i; indx2 = PrevActiveFunc; }
	      else
		{ indx1 = PrevActiveFunc; indx2 = i; }

	      auto* lambda = &Intersections[ElmValency*indx1+indx2].tcoord[0];
	      auto& nlambda = Intersections[ElmValency*indx1+indx2].nvals;
	      auto* qvals = &Intersections[ElmValency*indx1+indx2].qvals[0];
	
	      // If this intersection has not been computed, compute it.
	      if(InitIntersections[ElmValency*indx1+indx2]==false)
		{
		  if(side==SIDE::Left)
		    { tinterval[0] = NextKink.tcoord-EPS; tinterval[1] = PrevKink.tcoord+EPS; }
		  else
		    { tinterval[0] = PrevKink.tcoord-EPS; tinterval[1] = NextKink.tcoord+EPS; }
	    
		  QM.Intersect(PrevActiveElm, PrevActiveNode, My1RingElms[i], LocalNodeNums[i],
			       rdir, lambda, qvals, nlambda, &param);

		  // Record that this intersection has been computed
		  if(param.flag==true) 
		    InitIntersections[ElmValency*indx1+indx2] = true;
		}

	      // Is an intersection between #i and #PrevActiveFunc the next kink?
	      for( int j=0; j<nlambda; ++j)
		if(IsPointInInterval(PrevKink.tcoord, NextKink.tcoord, lambda[j], side))
		  // lambda[j] is a candidate for the next kink.
		  if( QM.Compute(PrevActiveElm, PrevActiveNode, rdir, lambda[j]+sign*EPS)>
		      QM.Compute(My1RingElms[i], LocalNodeNums[i], rdir, lambda[j]+sign*EPS) )
		    {
		      // Function #i is active. Update the next kink
		      NextKink.tcoord = lambda[j];
		      NextKink.qval = qvals[j];
		      NextKink.ActiveFunc = i;
		    }
	    }
	return;
      }
  }
  

  // Main functionality: implement Solve() for GeomTri2DQuality and GeomTet3DQuality metrics
  // \param[in] MD Mesh object
  // \param[in] Quality Metric
  // \param[in] MyReconData Data structures to use in reconstruction
  // \param[in] vert Vertex to be optimized
  // \param[in] rdir Relaxation direction
  // \param[in] SolSpecs Solver specifications
  // \param[in] usrparams User defined parameters
  // \return Optimal perturbation distance
  template<typename QType>
    double ReconstructiveMaxMinSolver<QType>::Solve(const int vert,  double* rdir,
						    void* usrparams) const
    {
      auto& MyReconData = GetLocalReconstructionData();
	
      // Access and populate 1-ring data structures
      auto& rd = MyReconData.RD;
      rd.vertex = vert;
      auto& LocalNodeNums = rd.LocalNodeNums;
      const auto& My1RingElms = Quality.GetMeshWrapper().Get1RingElements(vert);
      const int n1RingElms    = static_cast<int>(My1RingElms.size());
      
      
      // Number of nodes per element (2D triangles or 3D tets)
      const auto& MD = Quality.GetMesh();
      const auto& mesh_wrapper = Quality.GetMeshWrapper();
      const int NPE = MD.spatial_dimension()+1;
  
      // Local node numbers in 1-ring elements
      for( int i=0; i<n1RingElms; ++i)
	{
	  const auto* elmconn = MD.connectivity(My1RingElms[i]);
	  for(int a=0; a<NPE; ++a)
	    if(elmconn[a]==vert)
	      { LocalNodeNums[i] = a; break; }
	}

      // Reset intersections and extremizers
      std::fill(MyReconData.InitExtremizers.begin(), MyReconData.InitExtremizers.end(), false);
      std::fill(MyReconData.InitIntersections.begin(), MyReconData.InitIntersections.end(), false);
  
      // Level set Q = Q(0).
      double tMin = 0., tMax=0.; // Initialized values are not used.
      ComputeQ0LevelSet(Quality, rdir, rd, EPS, &MyReconData.InverseSet[0], tMin, tMax);
      
      // If the coordinate range is small enough, return 0.
      if( std::abs(tMax-tMin)<2.*EPS)
	return  0.;

      // At least one of tMin and tMax should be zero.
      // If tMin = 0, reconstruct to proceed to the right, else reconstruct to the left
      detail::SIDE side;
      double tExt;
      if(tMin>-EPS)
	{ side = detail::SIDE::Right; tExt = tMax; }
      else
	{ side = detail::SIDE::Left; tExt = tMin; }

      // Initialize the kink at t = 0
      detail::KinkData PrevKink;
      ComputeFirstKink(Quality, rd, rdir, side, EPS, PrevKink);
      
      // Element valency
      const int ElmValency = mesh_wrapper.GetMaxElementValency();
    
      // Advance kink along the direction "side"
      detail::KinkData NextKink;
      ComputeNextKink(Quality, MyReconData, rdir, tExt, ElmValency,
		      side, PrevKink, NextKink, EPS);
      // Access data on extremizers
      auto& InitExtremizers = MyReconData.InitExtremizers;
      auto& Extremizers = MyReconData.Extremizers;
  
      while(true)
	{
	  // Compute the extrema of PrevKink.ActiveFunc 
	  auto& PKExtrema = Extremizers[PrevKink.ActiveFunc];
	  if(InitExtremizers[PrevKink.ActiveFunc]==false)
	    {
	      Quality.Extremize(My1RingElms[PrevKink.ActiveFunc],
				LocalNodeNums[PrevKink.ActiveFunc],
				rdir, &PKExtrema.tcoord[0], &PKExtrema.qvals[0], PKExtrema.nvals);
	      InitExtremizers[PrevKink.ActiveFunc] = true;
	    }

	  // Does the maximizer of PrevKink.ActiveFunc lie
	  // in the interval [PrevKink.tcoord, NextKink.tccord] -> optimum is this maximizer.
	  for( int j=0; j<PKExtrema.nvals; ++j)
	    if(IsPointInInterval(PrevKink.tcoord, NextKink.tcoord, PKExtrema.tcoord[j], side))
	      return PKExtrema.tcoord[j];

	  // Is the function lambda -> Q(lambda) decreasing -> optimum is PrevKink.tcoord
	  if(PrevKink.qval>NextKink.qval)
	    return PrevKink.tcoord;
	  
	  // Have we reached the end of the search interval?
	  if(std::abs(NextKink.tcoord-tExt)<EPS)
	    return NextKink.tcoord;
	  
	  // Update the kinks
	  PrevKink.ActiveFunc = NextKink.ActiveFunc;
	  PrevKink.tcoord = NextKink.tcoord;
	  PrevKink.qval = NextKink.qval;
	  ComputeNextKink(Quality, MyReconData, rdir, tExt, ElmValency,
			  side, PrevKink, NextKink, EPS);
	}
      
      // cannot be here.
      dvr_assert(false && "ReconstructiveMaxMinSolver::Solve()- Algorithm did not converge.");
    }
}

