
/** \file dvr_SamplingMaxMinSolver_Impl.h
 * File implementing the class dvr::SamplingMaxMinSolver
 * \author Ramsharan Rangarajan
 * \date April 02, 2020.
 */

#pragma once

#include <cmath>
#include <dvr_QualityTraits.h>
#include <dvr_MaxMinSolverTraits.h>

#include <cassert>
#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  template<typename QType>
    SamplingMaxMinSolver<QType>::
    SamplingMaxMinSolver(QType& quality,
			 const int ns,
			 const int nthreads)
    :Quality(quality),
    nSamples(ns),
    nMaxThreads(nthreads),
    ThrRingData(nMaxThreads)
    {
      // Check quality traits
      static_assert(CheckQualityTraits<QType, typename std::remove_reference<decltype(std::declval<QType>().GetMesh())>::type>(),
		    "Quality class failed to satisfy trait requirements");

      // Check max-min solver traits
      static_assert(CheckMaxMinSolverTraits<SamplingMaxMinSolver<QType>>(), "Max-min solver failed to satisfy requirements");
      
      // Check that there is at least one sample point
      assert(nSamples>=1);

      // Size local data
      const auto& MD = Quality.GetMesh();
      const auto& mesh_wrapper = Quality.GetMeshWrapper();
      const int ElmValency = mesh_wrapper.GetMaxElementValency();
      for(int thr=0; thr<nMaxThreads; ++thr)
	{ // Resize RingData structures
	  auto& ringdata = ThrRingData[thr];
	  ringdata.LocalNodeNums.resize(ElmValency); }
    }


  // Copy constructor
  template<typename QType>
    SamplingMaxMinSolver<QType>::
    SamplingMaxMinSolver(const SamplingMaxMinSolver<QType>& Obj)
    :Quality(Obj.Quality),
    nSamples(Obj.nSamples),
    nMaxThreads(Obj.nMaxThreads),
    ThrRingData(Obj.ThrRingData) {}


  // Provide thread-safe access to local data structres
  template<typename QType> detail::RingData&
    SamplingMaxMinSolver<QType>::GetLocalRingData() const
    {
      int ThrNum = 0;
#ifdef _OPENMP
      ThrNum = omp_get_thread_num();
#endif
      assert(ThrNum<nMaxThreads && 
	     "dvr::SamplingMaxMinSolver::GetLocalRingData- Exceeded max number of threads.");
      return ThrRingData[ThrNum];
    }


  // Main functionality
  template<typename QType>
    double SamplingMaxMinSolver<QType>::Solve(const int vert,  double* rdir, void* params) const
    {
      // Local reconstruction data
      auto& rd = GetLocalRingData();
      rd.vertex = vert;
      auto& LocalNodeNums = rd.LocalNodeNums;
      
      // Populate 1-ring elements for this vertex
      const auto& MD = Quality.GetMesh();
      const auto& mesh_wrapper = Quality.GetMeshWrapper();
      const auto& My1RingElms = mesh_wrapper.Get1RingElements(vert);
      const int n1RingElms = static_cast<int>(My1RingElms.size());
      
      // Local node numbers in 1-ring elements
      const int SPD = MD.spatial_dimension();
      const int NPE = SPD+1;
      for( int i=0; i<n1RingElms; ++i)
	{
	  const auto* elmconn = MD.connectivity(My1RingElms[i]);
	  for(int a=0; a<NPE; ++a)
	    if(elmconn[a]==vert)
	      { LocalNodeNums[i] = a; break; }
	}
      
      // Compute a representative mesh size at this vertex
      double hval = 0.;
      int nEdges = 0;
      for(int i=0; i<n1RingElms; ++i)
	{
	  const int* elmconn = MD.connectivity(My1RingElms[i]);
	  for(int j=0; j<NPE; ++j)
	    {
	      const double* X = MD.coordinates(elmconn[j]);
	      for(int k=j+1; k<NPE; ++k, ++nEdges)
		{
		  const double* Y = MD.coordinates(elmconn[k]);
		  for(int L=0; L<SPD; ++L)
		    hval += (X[L]-Y[L])*(X[L]-Y[L]);
		}
	    }
	}
      hval /= static_cast<double>(nEdges);
      hval = std::sqrt(hval);

      // User-defined parameters for point perturbations
      PointPerturbationStruct* proj_params = nullptr;
      if(params!=nullptr)
	{ proj_params = static_cast<PointPerturbationStruct*>(params);
	  assert(proj_params->func!=nullptr); }
      
      // Useful variables
      double Qmin, qval, tval, opt_rdir[3];
      
      // Initialize optimal coordinate to lambda = 0
      double tOpt = 0.;
      for(int k=0; k<SPD; ++k)
	opt_rdir[k] = rdir[k];
      Qmin = Quality.Compute(My1RingElms[0]);
      dvr_assert(Qmin>0. && "dvr::SamplingMaxMinSolver- initial quality is inadmissible(negative)");
      for(int i=1; i<n1RingElms; ++i)
	{
	  qval = Quality.Compute(My1RingElms[i]);
	  if(qval<Qmin) Qmin = qval;
	}
      double Qopt = Qmin;
      const double Q0 = Qopt; 
      
      // Sample positive and negative sides
      const double polarity[] = {-1.,1.};
      const double dt = hval/static_cast<double>(nSamples);
      const double* VertCoord = MD.coordinates(vert);
      double PertCoord[3]; // Perturbed point coordinates. Sufficient space for 2D, 3D
      double fict_rdir[3]; // Fictitious perturbation direction in case of projections
      for(int sample=1; sample<nSamples; ++sample)
	for(int pcount=0; pcount<2; ++pcount) // +ve and -ve sides
	  {
	    // This sampling coordinate
	    tval = polarity[pcount]*static_cast<double>(sample)*dt;

	    // Coordinates of the sample point
	    for(int k=0; k<SPD; ++k)
	      PertCoord[k] = VertCoord[k] + tval*rdir[k];

	    // Let user alter this point
	    if(proj_params!=nullptr)
	      proj_params->func(PertCoord, proj_params->params);

	    // Fictitious direction: VertCoord--->PertCoord with t=1
	    for(int k=0; k<SPD; ++k)
	      fict_rdir[k] = PertCoord[k]-VertCoord[k];

	    // Evaluate min element quality for this perturbation
	    Qmin = Quality.Compute(My1RingElms[0], LocalNodeNums[0], fict_rdir, 1.);
	    for(int j=1; j<n1RingElms; ++j)
	      {
		if(Qmin<Qopt) break; // This point is no longer a candidate
		qval = Quality.Compute(My1RingElms[j], LocalNodeNums[j], fict_rdir, 1.);
		if(qval<Qmin) Qmin = qval;
	      }

	    // Do we have a new optimum?
	    if(Qmin>Qopt)
	      { Qopt = Qmin;
		tOpt = 1.;
		for(int k=0; k<SPD; ++k)
		  opt_rdir[k] = fict_rdir[k]; }
	  }
	  
      // Sanity check
      assert(Qopt>Q0-1.e-6 && "dvr::SamplingMaxMinSovler- unexpected optimal quality");

      // -- done --
      for(int k=0; k<SPD; ++k)
	rdir[k] = opt_rdir[k];
      
      return tOpt;
    }
}

