
/** \file testTet_MaxMinSolverCompare.cpp
 * \brief Unit test for the classes dvr::ReconstructiveMaxMinSolver and dvr::PartitionedMaxMinSolver classes.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * The vertifies that vertex perturbations computed by the two classes for a mesh of tetrahedra
 * are identical.
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */
#include <dvr_GeomTet3DQuality.h>
#include <dvr_utils_SimpleMesh.h>
#include <dvr_ReconstructiveMaxMinSolver.h>
#include <dvr_PartitionedMaxMinSolver.h>
#include <cmath>

using namespace dvr::utils;
using namespace dvr;

int main()
{
  const std::vector<double> coordinates{4.8756284028138391e-01,2.0188386209148662e-01,1.9969677390124799e-01,
      3.3967454391503304e-01,1.9628092161665507e-02,2.7881857896195184e-01,
      1.1898052770929178e-01,9.0196512840477072e-01,7.2474775224818844e-01,
      2.4852420741764930e-01,4.0659339213616552e-01,4.9998352755357228e-01,
      9.0212535284426087e-01,4.6836808153958359e-01,8.2177108669933807e-01,
      3.4864076011575335e-01,1.4774438878970231e-01,4.9606722594406516e-01,
      6.5390720227249788e-01,9.2900869019565391e-01,5.9853388676201413e-01,
      1.0937266231537601e-01,3.8575939532245174e-01,7.4125775009374351e-01,
      5.6138761924763014e-01,9.5111604963960361e-01,8.5975077330815275e-01,
      6.1023833174484388e-01,9.4600934106828464e-03,2.2237459390714257e-02};
  const std::vector<int> connectivity{0,5,1,9,
      0,5,3,1,
      2,0,1,9,
      2,0,3,1,
      0,5,9,4,
      5,0,3,4,
      6,0,2,9,
      0,6,2,3,
      6,0,9,4,
      0,6,3,4};
  const SimpleMesh MD(coordinates, connectivity, 3);

  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  // Create quality
  GeomTet3DQuality<decltype(MD)> Quality(mesh_wrapper);

  // Solvers
  ReconstructiveMaxMinSolver<decltype(Quality)> RMMS(Quality);
  PartitionedMaxMinSolver<decltype(Quality)> PMMS(Quality);
  const int nodenum = 0;
  for(int iter=0; iter<360; ++iter)
    {
      // Perturbation direction
      double angle = M_PI*double(iter)/(360.);
      double rdir[] = {cos(angle), sin(angle),0.};
      // Compute optimizers
      double lambda1 = RMMS.Solve(nodenum, rdir);
      double lambda2 = PMMS.Solve(nodenum, rdir);
      if(std::abs(lambda1-lambda2)>1.e-5)
	{
	  std::cout<<"\n"<<lambda1<<" did not equal "<<lambda2
		   <<" at iteration "<<iter; std::fflush( stdout );
	}
      assert(std::abs(lambda1-lambda2)<1.e-5 && "Optimizers did not match.");
    }
}
