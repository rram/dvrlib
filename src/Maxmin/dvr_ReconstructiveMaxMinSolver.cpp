
#include <dvr_ReconstructiveMaxMinSolver.h>

namespace dvr
{
  namespace detail
  {
    bool IsPointInInterval(const double& ThisKinkCoord, const double& NextKinkCoord,
			   const double& tval, const SIDE& side)
    {
      if(side==SIDE::Left)
	return (tval<ThisKinkCoord && tval>NextKinkCoord);
      else
	return (tval>ThisKinkCoord && tval<NextKinkCoord);
    }

  }
}
