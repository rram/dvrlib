
/** \file dvr_MaxMinSolverDetails.h
 * \brief Definition of helper structes used by max-min solvers.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 */

#pragma once

#include <vector>

namespace dvr{  // Private namespace
  namespace detail
  {
    //! Sizing of data structures in the solver
    struct MaxMinSolverSpecs
    {
      int nMaxThreads; //!< Number of threads to be used
      int nExtrema; //!< Number of extrema expected (upper bound)
      int nIntersections; //!< Number of intersections expecte (upped bound);
    };
    
    //! \brief Encapsulate 1-ring data at a vertex
    struct RingData
    {
      int vertex; //! vertex for which this data structure is populated
      std::vector<int> LocalNodeNums; //!< Vector of local node numbers for a vertex in the ring of elements in the 1-ring
    };

    //! \brief Encapsulate pairings of coordinates and
    //! qualities.
    struct CoordValuePairs
    {
      int nvals;         //!< Number of coordinate/qualities computed
      std::vector<double> tcoord; //!< Vector of parametric coordinates
      std::vector<double> qvals;  //!< Corresponding values of qualities
    };

    //! Struct encapsulating data for partitioned solver
    struct PartitionData
    {
      //! 1-ring data structure
      RingData RD;
      
      //! Maxima of a quality curve
      CoordValuePairs Extrema;
      
      //! Intersections of a quality curve
      CoordValuePairs Intersections;
    };

    //! \brief Encapsulating data ay a kink.
    struct KinkData
    {
      double tcoord;           //!< Coordinate of this kink
      int ActiveFunc; //!< Index of function that is active at this kink
      double qval;             //!< Quality at this kink
    };

    struct OptData
    {
      double tcoord;    //!< Current estimate of optimal coordinate
      double qval;      //!< Current estimate of best quality
    };


    //! Struct encapsulating data needed for reconstruction
    //! \todo Consider reordering data
    struct ReconData
    {
      //! 1-ring data structure
      RingData RD;
    
      //! Track extremal calculations
      //! Extremizers[i] = struct of data for extrema of the i-th function
      std::vector<CoordValuePairs> Extremizers; 
      std::vector<bool> InitExtremizers; //!< Flag to track if extremizers have been computed
    
      //! Track intersection calculations 
      //! Intersections[N*i+j] = data for intersections
      //! of the i-th and j-th function, where N is the max-number of 1-ring elements
      std::vector<CoordValuePairs> Intersections;
      std::vector<bool> InitIntersections; //!< Flag to track if intersections have been computed

      //! For inverse computations
      std::vector<double> InverseSet; 
    };

  }}

