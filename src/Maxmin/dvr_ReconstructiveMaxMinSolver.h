
/** \file dvr_ReconstructiveMaxMinSolver.h
 * \brief Definition of the class dvr::ReconstructiveMaxMinSolver.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <dvr_MaxMinSolverDetails.h>
#include <algorithm>
#include <cfloat>


namespace dvr{

  /** \brief Class implementing a univariate max-min solver to compute optimal vertex perturbations.
   *
   * The main functionality of this class is to resolve problems of the form
   * \f[ \text{Find}~\lambda^\text{opt}\in \arg\max_{\lambda\in 
   * {\mathbb R}}\min\{{\rm Q}(K^\lambda)\,:\,K^\lambda\in \stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\},\f]
   * where:
   * <ul>
   * <li> \f$i\in {\rm I}_{\rm R}\f$ is a vertex in the triangulation \f${\cal T}\f$ to be relaxed</li>
   * <li> \f${\cal T}^{i,\lambda}\f$ is the mesh realized by perturbing vertex \f$i\f$ along a prescribed
   * direction by the coordinate \f$\lambda\f$ </li>
   * <li> \f$K^\lambda\in {\cal T}^{i,\lambda}\f$ is the element corresponding to \f$K\in {\cal T}\f$,
   * i.e., \f$K^\lambda\f$ is the image of \f$K\f$ when the vertex \f$i\f$ is perturbed by the 
   * coordinate \f$\lambda\f$
   * <li> \f$\stackrel{\star}{e}(i,{\cal T}^{i,\lambda})\f$ denotes the list of elements 
   * incident at the vertex \f$i\f$</li>
   * <li> \f${\rm Q}\f$ is the element quality metric. </li>
   * </ul>
   * 
   * In the following, it is convenient to use the following notation:
   * \f[ f_\alpha(\lambda) = {\rm Q}(K_\alpha^\lambda)~\text{for}~\{K_1,K_2,\ldots,K_n\}\in \stackrel{\star}{e}(i,{\cal T}).\f]
   * \f[ F(\lambda) = \min\{f_1(\lambda),f_2(\lambda),\ldots,f_n(\lambda)\}\f]
   *
   * The class hence implements a solver for the problem 
   * \f[\text{Find}~\lambda_{\rm opt} = \arg\max_{\lambda\in{\mathbb R}} {\rm F}(\lambda).\f]
   * 
   * The main challenge in resolving the maximization problem above, despite its one-dimensional nature, 
   * stems from the non-smoothness of \f$F\f$. 
   * Specifically, the quality metric \f${\rm Q}\f$ is expected to be a smooth function of  
   * vertex coordinates. Consequently, each function \f$f_1,\ldots, f_n\f$ is expected to be a smooth function,.
   * However, \f$\lambda\mapsto F(\lambda)\f$, as a minimum among \f$n\f$ smooth functions, 
   * is in general only continuous, and fails to be differentiable everywhere. Hence, identifying
   * the maximizer \f$\lambda^{\rm opt}\f$ is a non trivial problem. Commonly used  Newton-type methods do not suffice.
   * 
   * This class implements Algorithm 2 from the article:
   * <br>
   * R Rangarajan, <em>On the resolution of certain discrete univariate max–min problems,</em>
   * Computational Optimization and Applications (2017), 68 (1), 163-192 <a href="https://doi.org/10.1007/s10589-017-9903-z">(doi)</a>.
   *
   * Theorem 3 in the article identifies sufficient conditions on the functions
   * \f$\{f_\alpha\}_{1\leq \alpha\leq n}\f$ for the algorithm
   * to correctly determine \f$\lambda^{\rm opt}\f$ and for the algorithm to terminate:
   * <ul>
   * <li> (Feasibility) \f$f_\alpha(0)>0\f$ </li>
   * <li> (Continuity) \f$f_\alpha\f$ is continuous </li>
   * <li> (Decay) \f$f_\alpha(\lambda)\rightarrow 0\f$ as \f$|\lambda|\rightarrow \infty\f$ </li>
   * <li> (Strict quasi-concavity) \f$f_\alpha\f$ is strictly quasi-concave on the interval
   * \f$\{\lambda\,:\,f_\alpha(\lambda)\geq y\}\f$ for each \f$y\geq f_\alpha(0)\f$. </li>
   * <li> (Non-duplicity) \f$f_\alpha = f_\beta\f$ if and only if \f$\alpha=\beta\f$.</li>
   * </ul>
   * These conditions are guaranteed to be satisfied if:
   * <ul>
   * <li> \f${\rm Q}\f$ is chosen to be the mean ratio metric (for triangles and tets). This ensures
   * continuity, strict quasiconcavity and decay properties. </li>
   * <li> All perturbable elements in the mesh, i.e., in the set 
   * \f$\cup_{i\in {\rm I}_{\rm R}}\stackrel{\star}{e}(i,{\cal T})\f$ have strictly positive
   * qualities. This ensures feasibility. </li>
   * <li> Functions in the list
   * \f$\{\lambda\mapsto f_\alpha(\lambda)\}_{\alpha}\f$ are uniquated. 
   * This is required to satisfy the non-duplicity condition.
   * <br>
   * Specifically, it is possible that \f$K_\alpha,K_\beta\f$ are distinct elements in the 
   * 1-ring of a vertex \f$i\in {\rm I}_{\rm R}\f$, but their quality curves coincide, i.e., 
   * \f$f_\alpha=f_\beta\f$  for \f$\lambda\in {\mathbb R}\f$.
   * This can happen, for instance, if the elements \f$K_\alpha\f$ and \f$K_\beta\f$
   * are congruent elements and the relaxation direction is chosen such that 
   * \f$K_\alpha^\lambda\f$ and \f$K_\beta^\lambda\f$ remain congruent as a common
   * vertex is perturbed. 
   * <br>
   * A "duplication" in the list of functions \f$\{f_\alpha\}_\alpha\f$
   * is inconsequential in the definition of the optimizer \f$\lambda^{\rm opt}\f$.
   * The algorithm, however, requires that only a unique copy of each function in the list
   * be retained.
   * </li>
   * </ul>
   *
   *  The main idea behind the algorithm consists in 
   * <ul>
   * <li> Bracketing the maximizer \f$\lambda^{\rm opt}\f$ to the super level set
   * \f[{\rm I} = 
   * \{\lambda\in {\mathbb R}\,:\, F(\lambda)\geq F(0)\}.\f] 
   * Identifying  \f${\rm I}\f$ limits the search for \f$\lambda^{\rm opt}\f$ to a small interval.
   * For instance, owing to strict quasiconcavity of \f$F\f$ (which follows from those of \f$f_\alpha\f$), 
   * \f${\rm I}\f$ is necessarily one-sided, i.e., either of the form 
   * \f$[0,\lambda^{\rm hi}]\f$ or of the form \f$[-\lambda^{\rm lo},0]\f$, where 
   * \f$\lambda^{\rm hi},\lambda^{\rm lo}\f$ are non negative reals. </li>
   *
   * <li> Explicity reconstructing the function 
   * \f$\lambda\mapsto F(\lambda)\f$ 
   * within the interval \f${\rm I}\f$. This entails computing a limited set of 
   * pairwise intersections of element quality curves, i.e., 
   * \f$\{\lambda\in {\rm I}\,:\,f_\alpha(\lambda) = f_\beta(\lambda)\}\f$ for a
   * small subset of \f$\alpha,\beta\in \{1,\ldots,n\}\f$.
   * When adopting the mean ratio metric, these intersection calculations reduce to 
   * computing roots of polynomials.
   * </li>
   * </ul>
   * 
   * The complexity of the algorithm is dominated by the determination
   * of active functions defining \f$F\f$, i.e., the index
   * \f$\alpha\f$ and the corresponding
   * interval \f$\Lambda_\alpha\f$ where \f$F(\lambda)=f_\alpha(\lambda)\f$. 
   * This is expected to scale with \f$n\f$, the number of elements in the 1-ring
   * of the vertex being relaxed. 
   *
   * The main functionality of the class is the dvr::ReconstructiveMaxMinSolver::Solve method,
   * which, for a given vertex and relaxation direction, 
   * uses the detailed information from the quality metric object provided
   * to evaluate the functions \f$f_\alpha\f$, their derivatives, and their pairwise intersections.
   *   
   * The class is thread safe. It creates as many copies of local data structures as
   * the number of threads specified during construction.  The number of threads used
   * during mesh optimization cannot exceed this number.
   *
   * The class is templated by the type of element quality metric.
   * \tparam QType typename of the element quality metric.
   * 
   * \see dvr::PartitionedMaxMinSovler for an alternative max-min solver.
   * \note testVertexOptimize.cpp: Unit test for this class.
   *
   * \warning A hard-coded tolerance dvr::ReconstructiveMaxMinSolver::EPS = 1.e-6
   * is used to order qualities and interval bounds in the method dvr::ReconstructiveMaxMinSolver::Solve.
   * 
   * \ingroup dvr_maxmin_solvers
   */ 

  /* \todo Using sets as key for intersection computations. Can we avoid this?
     \todo Copying 1-ring sets to 1-ring vectors during each solve. Can we avoid this?
     \todo How can we check that the mesh used by the quality metric is consistent?
     \todo Use memory for intersection data structs more efficiently.
     Currently, all possible intersections are assumed possible. Symmetry of the data structure
     is not accounted for. Memory is also allocated for self-intersections. These help
     easier indexing into the array, but the memory overhead may become significant, especially in 3D.
     \todo At intersections, account for the case in which values and slopes are idential.
     \todo Account for the cases in which kinks are very close by. Numerical accuracy of
     kink computations has not been accounted for.
     \todo It is possible that the CoordValuePairs struct is making memory access inefficient.
     Need to revisit this issue to decide how coordinate-quality pairs are stored.
     \todo Have boolean types for recording is extremizers/intersections have been computed or not. Resetting
     these is taking too much time.
     \todo A tolerance is being used here. Need to examine how to choose its value.
     \todo Permit suboptimality through usrparams.
  */
  template<typename QType>
    class ReconstructiveMaxMinSolver
    {
    public:
      //! Constructor
      //! \param[in] quality Instance of a quality metric
      //! \param[in] nthreads Max number of omp threads. Used to size local data structures for thread-safe access. Default: 1
      ReconstructiveMaxMinSolver(QType& quality, const int nthreads=1);
      
      //! Destructor
      inline virtual ~ReconstructiveMaxMinSolver() {}

      //! Copy constructor
      //! \param[in] Obj Object to be copied from
      //! \todo May have to perform a deep copy here
      ReconstructiveMaxMinSolver(const ReconstructiveMaxMinSolver<QType>& Obj);

      //! Cloning
      inline virtual ReconstructiveMaxMinSolver<QType>* Clone() const
      { return new ReconstructiveMaxMinSolver<QType>(*this); }

      //! Returns the max number of threads
      inline int GetMaxNumThreads() const
      { return SolSpecs.nMaxThreads; }

      //! Returns the mesh quality
      inline QType& GetMeshQuality()
      { return Quality; }

      //! Main functionality
      //! Can be specialized for specific quality metrics
      //! \param[in] vert Vertex to be optimized
      //! \param[in,out] rdir Relaxation direction for vertex. Can be changed on return
      //! \param[in] usrparams User defined parameters specific to the implementation
      //! \return Computed optimal perturbation coordinate
      //! \todo Permit suboptimality through usrparams.
      //! \bug There may be an issue when more than two curves intersect at the same kink
      //! \bug There is an issue when kinks are very close by- we should use a tolerance to check
      //! when successive kinks can be merged and update the active function correctly.
      double Solve(const int vert,  double* rdir, void* usrparams=nullptr) const;

    private:
      // Members
      QType& Quality; //!< Mesh object
      const detail::MaxMinSolverSpecs SolSpecs; //!< Solver specifications
      const double EPS = 1.e-6; /*! < Tolerance.
				 * Used by the Solve method to order qualities and interval bounds. */
      //! Thread safe access to local reconstruction data
      mutable std::vector<detail::ReconData> ThrReconData;

      //! Provides thread-safe access to reconstruction data
      detail::ReconData& GetLocalReconstructionData() const;
     
    };

  // Utility functions
  namespace detail
  {
    // Sepcify reconstruction side
    enum class SIDE {Left, Right};
    
    // Examines if a point lies in the specified interval
    // \param[in] ThisKinkCoord Currently known kink
    // \param[in,out] NextKinkCoord Next kink
    // \param[in] tval New coordinate to inspect
    // \param[in] side Left side interval or right side interval
    // \todo Need to examine tolerances here
    bool IsPointInInterval(const double& ThisKinkCoord, const double& NextKinkCoord,
			   const double& tval, const SIDE& side);
  }
}

// Implementation of the solve() method
#include <dvr_ReconstructiveMaxMinSolver_Impl.h>
