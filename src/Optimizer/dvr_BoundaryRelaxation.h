
/** \file dvr_BoundaryRelaxation.h
 * \brief Definition of structs to help constrain nodes lying on a curvilinear boundary
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#pragma once

#include <dvr_RelaxationDirGenerators.h>
#include <boost/function.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  using SignedDistanceFunction = boost::function<void(const double* X, double& sd, double* dsd)>; /*!< Function type that evaluates the signed 
												   * distance function and its derivatives.
												   * \param[in] X Coordinates of the point where to evaluate the signed distance
												   * \param[out] sd Computed signed distance function \f$\phi(X)\f$. 
												   * \param[out] dsd Gradient of the signed distance function, \f$\nabla\phi(X)\f$. Should have unit norm.
												   */


  
  /** \brief Helper struct to encapsulate relaxation orthogonal to a level set function
   * The struct is templated by the spatial dimension and is thread-safe.
   * The tangential direction is computed as a (random) vector orthogonal to the gradient of the level set function.
   * \tparam SPD Spatial dimension. Should equal 2 or 3.
   * \ingroup dvr_relax_dirs
   * \see dvr::CartesianDirGenerator, dvr::RandomDirGenerator
   */
  template<int SPD>
    struct TangentialDirGenerator: public RelaxationDirGenerator
    {
    private:
      struct ParamStruct
      {
	SignedDistanceFunction SDfunc;
	std::random_device rd;
	std::vector<std::mt19937> genVec;
	std::vector<std::uniform_real_distribution<>> disVec;
	inline ParamStruct(SignedDistanceFunction sdfunc, int nthreads)
	  :SDfunc(sdfunc),
	  genVec(nthreads, std::mt19937(rd())),
	  disVec(nthreads, std::uniform_real_distribution<>(-1.,1.)) {}
      };

    public:
      //! Constructor
      //! \param[in] sdfunc Signed distance function
      //! \param[in] nthreads Max number of threads. Defaulted to 1
      inline TangentialDirGenerator(SignedDistanceFunction sdfunc, const int nthreads=1)
	:dvr::RelaxationDirGenerator(),
	funcParams(sdfunc, nthreads)
	  {
	    assert(SPD==2 || SPD==3);
	    func = &TangentialDirection;
	    params = &funcParams;
	  }
      
      // Require template specializations
      static void TangentialDirection(const int vertnum, const double* X,
				      double* rdir, void* ctx);
      
    private:
      ParamStruct funcParams; //!< Parameters to pass to the static method TangentialDirection
    };
  
  /** \brief Struct enabling perturbations of sampling points
   * \ingroup dvr_helpers
   * \see ClosestPointStruct
   */
  struct PointPerturbationStruct
  {
    std::function<void(double* X, void* usr)> func; /*!< Point to function that perturbs a point X.
						     * \param[in,out] X Point to perturb. On return, perturbed point
						     * \param[in,out] usr user-defined parameters.
						     */
    void* params; //!< User-defined parameters accepted by the member 'func'
  };
  
   
  /** \brief Helper struct that defines PointPerturbation as a closest point projection onto a domain boundary.
   * The struct is thread safe and is templated by the spatial dimension.
   * \tparam SPD Spatial dimension
   * \ingroup dvr_helpers
   */
  template<int SPD>
    struct ClosestPointStruct: public PointPerturbationStruct
    {
      //! Constructor
      //! \param[in] sdfunc Signed distance function
      inline ClosestPointStruct(SignedDistanceFunction sdfunc)
	:PointPerturbationStruct(),
	SDfunc(sdfunc)
	  { func = ClosestPointProjection;
	    params = &SDfunc;
	  }
      
      inline static void ClosestPointProjection(double* X, void* ctx)
      {
	// Access the signed distance function
	assert(ctx!=nullptr);
	const auto& SDfunc = *static_cast<SignedDistanceFunction*>(ctx);
      
	// Compute the signed distance and its gradient at X
	double sd, dsd[SPD];
	SDfunc(X, sd, dsd);
     
	// Closest point projection
	for(int k=0; k<SPD; ++k)
	  X[k] -= sd*dsd[k];
	
	// -- done --
	return;
      }

      SignedDistanceFunction SDfunc; //!< Function to compute signed distances and derivatives, and the parameters to pass to it.
    };
}


