
/** \file dvr_MeshOptimizer_Impl.h
 * \brief Implementation of vertex and mesh optimization routines.
 * \author Ramsharan Rangarajan
 * \date March 22, 2020.
 */

#pragma once

#include <cfloat>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  // Computes the quality of the requested element
  template<typename QType>
    double ComputeElementQuality(const int elm, const QType& Quality)
    { return Quality.Compute(elm); }


  // Computes the quality at a vertex
  template<typename QType>
    double ComputeVertexQuality(const int vert, const QType& Quality)
    {
      // Get the 1-ring data for this vertex
      const auto& My1RingElms = Quality.GetMeshWrapper().Get1RingElements(vert);
      const int n1RingElms    = static_cast<int>(My1RingElms.size());
      
      double Qmin = DBL_MAX;
      double qval;
      for(int i=0; i<n1RingElms; ++i)
	{
	  qval = ComputeElementQuality(My1RingElms[i], Quality);
	  if(qval<Qmin) Qmin = qval;
	}
      return Qmin;
    }


  // Computes the optimal coordinate for a vertex along a prescribed direction
  template<typename MMSType>
    double Optimize(const int vert, double* rdir,
		    MMSType& MMSolver,
		    void* solparams)
    { return MMSolver.Solve(vert, rdir, solparams); }


  // Optimizes the coordinates of all nodes being relaxed
  template<typename MMSType>
    void Optimize(const std::vector<int>& Ir,
		  MMSType& MMSolver,
		  RelaxationDirGenerator& rdir_gen,
		  void* solparams)
  {
    // Access the mesh
    auto& MD = MMSolver.GetMeshQuality().GetMeshWrapper().GetMesh();
    
    // Spatial dimension
    const int SPD = MD.spatial_dimension();

    // Loop over vertices to be relaxed, relax 1-by-1.
    double lambda;
    std::vector<double> rdir(SPD);
    std::vector<double> Y(SPD);
    int k;
      
    //! [doc_seq_optimize_mesh]
    for(const auto& vert:Ir)
      {
	// Original coordinates of this vertex
	const double* X = MD.coordinates(vert);
	  
	// Relaxation direction
	rdir_gen.func(vert, X, &rdir[0], rdir_gen.params);
	  
	// Optimal perturbation coordinates
	lambda = MMSolver.Solve(vert, &rdir[0], solparams);
	  
	// Perturb the vertex to its optimal location
	for(k=0; k<SPD; ++k)
	  Y[k] = X[k] + lambda*rdir[k];
	  
	// Set these coordinates in the mesh
	MD.update(vert, &Y[0]);
      }
    //! [doc_seq_optimize_mesh]
      
    // -- done --
    return;
  }

  // Thread-parallel vertex relaxation
  template<typename MeshType, typename MMSType>
    void Optimize(const std::vector<int>& Ir,
		  MMSType& MMSolver,
		  RelaxationDirGenerator& rdir_gen,
		  const VertexColoring<MeshType>& vshades,
		  void* solparams)
  {
    // Check that OpenMP is available
#ifndef _OPENMP
    dvr_assert(false && "dvr::Optimize()- openMP not available.");
#endif
    
    // access the mesh
    auto& MD = MMSolver.GetMeshQuality().GetMeshWrapper().GetMesh();
    
    // Spatial dimension
    const int SPD = MD.spatial_dimension();
    
    //! [doc_omp_vert_groups]
    // Access vertex groups
    const std::vector<std::vector<int>>& VertGroups = vshades.GetPartitions();
    //! [doc_omp_vert_groups]

    //! [doc_omp_algorithm]
    // Loop over partitions/colorings of vertices in Ir
    for(auto& VertList:VertGroups)
      {
	// Concurrently relax vertices of the same color
	const int nVerts = static_cast<int>(VertList.size());

	// Spawn parallel threads
#pragma omp parallel default(shared)
	{
	  // Check that the number of threads is consistent with that in the max-min solver
#pragma omp single
	  {
	    int nthr = 1;
#ifdef _OPENMP
	    nthr = omp_get_num_threads();
#endif
	    dvr_assert( (MMSolver.GetMaxNumThreads()>=nthr) &&
			"Max-Min solver and optimizer have inconsistent number of threads.");
	  }
	  
	  // Thread-local variables
	  int vert;                      // Vertex number
	  std::vector<double> rdir(SPD); // Relaxation direction
	  std::vector<double> Y(SPD);    // Updated vertex coordinates
	  double lambda;                 // Optimal distance
	  int k;                // Iteration index
	    
#pragma omp for schedule(dynamic)
	  for( int vertnum=0; vertnum<nVerts; ++vertnum)
	    {
	      // This vertex
	      vert = VertList[vertnum];
	      const double* X = MD.coordinates(vert);
		
	      // Relaxation direction
	      rdir_gen.func(vert, X, &rdir[0], rdir_gen.params);
	      
	      // Compute optimal coordinate
	      lambda = Optimize(vert, &rdir[0], MMSolver, solparams);
		
	      // Update coordinates
	      for(k=0; k<SPD; ++k)
		Y[k] = X[k] + lambda*rdir[k];
		
	      // Set the new coordinates in the mesh
	      MD.update(vert, &Y[0]);
		
	    } // End parallel for
	} // End parallel construct
      } // End loop over partitions of Ir
    //! [doc_omp_algorithm]
    
    // -- done --
    return;
  }
 
}
