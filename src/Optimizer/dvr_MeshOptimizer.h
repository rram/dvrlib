
/** \file dvr_MeshOptimizer.h
 * \brief Definition of vertex and mesh optimization routines.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#pragma once

#include <dvr_RelaxationDirGenerators.h>
#include <dvr_VertexColoring.h>

namespace dvr{

 /** @defgroup dvr_funcs Optimization routines
   * @{
   */
  
  /** \brief Utility function to compute the quality of an element in the mesh.
   *
   * \param[in] elm Element index. 
   * \param[in] Quality Metric to use
   * \tparam QType Quality typename
   * \return Quality of specified element
   * \ingroup dvr_funcs
   */
  template<typename QType>
    double ComputeElementQuality(const int elm, const QType& Quality);

  /** \brief Utility function to compute the minimum among qualities of elements in the 1-ring of a vertex
   * 
   * Specifically, the function computes 
   *  This is directly useful when determining components of the mesh quality vector.
   *  \param[in] vert Index of vertex at which to compute quality
   *  \param[in] Quality Metric to use for quality
   *  \return Minimum among the qualities of elements in the  1-ring of a specified vertex, 
   * i.e., \f$\min\{{\rm Q}(K)\,:\,K\in \stackrel{\star}{e}({\tt vert},{\cal T})\f$.
   * \ingroup dvr_funcs
   */
  template<typename QType>
    double ComputeVertexQuality(const int vert, const QType& Quality);
    
    
  /** \brief Computes the optimal coordinate for relaxing a vertex along a prescribed direction
   *
   * \param[in] vert Vertex number
   * \param[in,out] rdir Relaxation direction
   * \param[in] MMSolver Solver to use
   * \param[in] solparams User-defined parameters used with solver calls. Assumed to be null by default
   * \tparam MMSType Typename of the max-min solver.
   * \return The optimal perturbation coordinates
   * \f$\lambda^{\rm opt} = \arg\max_{\lambda\in {\mathbb R}} \min\{{\rm Q}(K)\,:\,K\in {\cal T}^{i,\lambda}\}\f$,
   * where \f${\cal T}^{i,\lambda}\f$ is the mesh realized by perturbing the vertex \f$i\f$ along the 
   * prescribed direction <tt>rdir</tt> by the coordinate \f$\lambda\f$.
   * \ingroup dvr_funcs
   * 
   */
  template<typename MMSType>
    double Optimize(const int vert, double* rdir,
		    MMSType& MMSolver,
		    void* solparams=nullptr);


  /** \brief Sequentially optimize locations of a prescribed list of vertices.
   * 
   * The routine is functionally equivalent to invoking the 
   * overloaded dvr::Optimize(const int vert, double* rdir, const MMSType& MMSolver, void* solparams=nullptr)
   * routine for each vertex in a given list, with the relaxation direction generated using a function pointer.
   *
   * \param[in] Ir List of vertices to be relaxed
   * \param[in] rdir_gen relaxation direction generator.
   * \param[in] solparams User-defined parameters to be passed during solver calls
   * \param[in] MMSolver Max-Min solver to use
   * \tparam MeshType typename for the mesh data structure
   * \tparam MMSType typename of the max-min solver
   * \ingroup dvr_funcs
   */
  template<typename MMSType>
    void Optimize(const std::vector<int>& Ir,
		  MMSType& MMSolver,
		  RelaxationDirGenerator& rdir_gen,
		  void* solparams=nullptr);
  
  /** \brief Thread-parallel optimization of locations of a prescribed list of vertices
   * 
   * The routine uses a given list of vertices to be relaxed, and their
   * partitioning (i.e., a coloring) to concurrently relax vertices
   * belonging to the same partition.
   * 
   * The routine uses a simple data-parallelism model in which
   * vertices assigned the same color are distributed among threads
   * using openMP directives. The routine avoid the usage of locks by
   * exploiting the fact the vertices assigned the same color by the
   * dvr::VertexColoring class do not belong to 1-rings of each
   * other. Hence, computing perturbations for vertices in a given
   * partition only access coordinates of vertices in a different
   * partition (or color group).
   *   
   * The routine uses a dynamic schedule, since the time taken to
   * compute perturbations for vertices can have large variations.
   *
   * Assumes that the number of threads to use has been set using omp_set_num_threads().
   * This number cannot exceed the number of threads specified to the quality metric
   * or the max-min solver during construction. 
   *
   * \param[in] Ir List of vertices to be relaxed
   * \param[in] vshades Object for accessing vertex colorings
   * \param[in] MMSolver Max-Min solver to use
   * \param[in] rdir_gen Generator for relaxation directions
   * \param[in] solparams User-defined parameters used during solver calls
   * \tparam MeshType typename of the mesh data structure
   * \tparam MMSType typename of the max-min solver. Assumed to be a thread-safe class.
   *
   * \ingroup dvr_funcs
   */
  template<typename MeshType, typename MMSType>
    void Optimize(const std::vector<int>& Ir,
		  MMSType& MMSolver,
		  RelaxationDirGenerator& rdir_gen,
		  const VertexColoring<MeshType>& vshades,
		  void* solparams=nullptr);
}


// Implementation
#include <dvr_MeshOptimizer_Impl.h>
