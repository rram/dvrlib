
/** \file dvr_BoundaryRelaxation.cpp
 * \brief Implementation of template specializations
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#include <dvr_BoundaryRelaxation.h>

namespace dvr
{
  // Template specialization for tangential direction generator in 2D
  template<> void TangentialDirGenerator<2>::
  TangentialDirection(const int vertnum, const double* X,
		      double* rdir, void* ctx)
  {
    // Access the signed distance function
    assert(ctx!=nullptr);
    const auto& pstruct = *static_cast<const ParamStruct*>(ctx);
    const auto& SDFunc = pstruct.SDfunc;
          
    // Compute the signed distance and its gradient
    double sd, dsd[2];
    SDFunc(X, sd, dsd);

    // Return the direction orthogonal to 'dsd'
    rdir[0] = dsd[1];
    rdir[1] = -dsd[0];

    // -- done --
    return;
  }

  // Tangential direction generator in 3D
  template<> void TangentialDirGenerator<3>::
  TangentialDirection(const int vertnum, const double* X,
		      double* rdir, void* ctx)
  {
    const int SPD = 3;
      
    // Access the signed distance function, random number generators
    assert(ctx!=nullptr);
    auto& pstruct = *static_cast<ParamStruct*>(ctx);
    const auto& SDFunc = pstruct.SDfunc;
          
    int thrnum = 0;
#ifdef _OPENMP
    thrnum = omp_get_thread_num();
    assert(thrnum<static_cast<int>(pstruct.genVec.size()));
#endif
    auto& gen = pstruct.genVec[thrnum];
    auto& dis = pstruct.disVec[thrnum];

    // Signed distance
    double sd, dsd[SPD];
    SDFunc(X, sd, dsd);
      
    // Generate a random vector with a non-trivial component orthogonal to the normal
    while(true)
      {
	double dot = 0.;
	double norm = 0.;
	for(int k=0; k<SPD; ++k)
	  {
	    rdir[k] = dis(gen);
	    norm += rdir[k]*rdir[k];
	    dot += rdir[k]*dsd[k];
	  }
	norm = std::sqrt(norm);

	// If norm is too small, regenerate the vector
	if(norm<1.e-2) continue;

	// Check projection along the normal
	dot /= norm; // dot = 0 is ideal. dot = 1 is not.
	if(std::abs(dot)>0.95) continue;
	  
	// project rdir onto the tangent plane
	for(int k=0; k<SPD; ++k)
	  { rdir[k] -= dot*dsd[k];
	    norm += rdir[k]*rdir[k]; }
	norm = std::sqrt(norm);
	for(int k=0; k<SPD; ++k)
	  rdir[k] /= norm;

	break;
      }
      
    // -- done --
    return;
  }


}
