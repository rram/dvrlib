
/** \file testTri_VertexOptimize.cpp
 * \brief Unit test checking correctness of the function dvr::Optimize.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * Uses a 1, 2 and 3 element test to check correctness of the 
 * perturbation coordinate computed by the routine dvr::Optimize.
 * This in turn verifies correctness of the methods 
 * dvr::ReconstructiveMaxMinSolver::Solve and
 * dvr::PartitionedMaxMinSolver::Solve and
 *
 * Inputs: none
 *
 * Outputs: none if successful, assertions in case of an error. 
 */

#include <dvr_utils_SimpleMesh.h>
#include <dvr_MeshOptimizer.h>
#include <dvr_GeomTri2DQuality.h>
#include <dvr_PartitionedMaxMinSolver.h>
#include <dvr_ReconstructiveMaxMinSolver.h>
#include <cmath>

using namespace dvr::utils;
using namespace dvr;

template<typename MMSType> void OneElementTest();
template<typename MMSType> void TwoElementTest();
template<typename MMSType> void ThreeElementTest();

int main()
{
  OneElementTest<PartitionedMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();
  OneElementTest<ReconstructiveMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();

  TwoElementTest<PartitionedMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();
  TwoElementTest<ReconstructiveMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();

  ThreeElementTest<PartitionedMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();
  ThreeElementTest<ReconstructiveMaxMinSolver<GeomTri2DQuality<SimpleMesh>>>();
}



// One element test
template<typename MMSType> void OneElementTest()
{
  // One element mesh
  SimpleMesh MD({0.,0., 1.,-2., 1.25, 1.75}, {0,1,2}, 2);
  
  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Create quality metric
  GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);
  
  // Max-min solver
  MMSType solver(Quality);
  
  // Optimizer node number #0
  const int nodenum = 0;
  // Relaxation direction
  double rdir[] = {cos(M_PI/7), sin(M_PI/7)};
  double lambda = Optimize(nodenum, rdir, solver);
  assert(std::abs(lambda+2.0275952820016)<1.e-6);
}

template<typename MMSType> void TwoElementTest()
{
 // Create a mesh with two elements
  SimpleMesh MD({0.,0., 1.,-2., 1.25,1.75, -2.5,3., -1.75, -1.}, {0,1,2,0,3,4}, 2);
  
  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);

  // Create qualtiy
  GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);

  // Compute the quality of elements (compare with values from mathematica)
  const double EPS = 1.e-6;
  assert(std::abs(Quality.Compute(0)-0.6198918679720192)<EPS);
  assert(std::abs(Quality.Compute(1)-0.7483425091935219)<EPS);

  // Perturbation
   double rdir[] = {cos(M_PI/7), sin(M_PI/7)};
   
  // Create 1-ring data  
  const int nodenum = 0;

  // Minimax solver
  MMSType solver(Quality);
  
  // Optimize
  double lambda = Optimize(nodenum, rdir, solver);
  assert(std::abs(lambda+0.20513266942439135)<1.e-6);
}


template<typename MMSType> void ThreeElementTest()
{
  // Create a mesh with three elements
  SimpleMesh MD({0.,0., 2.,-2., 1.25,1.75, -3.5,3., -1.75, -1., 1.5,4., -1.,3.}, {0,1,2,0,3,4,0,5,6}, 2);
  
  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Create qualtiy
  GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);

  // Compute the quality of elements (compare with values from mathematica)
  const double EPS = 1.e-6;
  assert(std::abs(Quality.Compute(0)-0.7627379703055607)<EPS);
  assert(std::abs(Quality.Compute(1)-0.683062290308853)<EPS);
  assert(std::abs(Quality.Compute(2)-0.8294327810893215)<EPS);
  
  // Perturbation
  double rdir[] = {cos(M_PI/5), sin(M_PI/5)};
  const int nodenum = 0;

  // Minimax solver
  MMSType solver(Quality);
  
  // optimize
  double lambda = Optimize(nodenum, rdir, solver);
  assert(std::abs(lambda-0.145424452371552)<1.e-6);
}
