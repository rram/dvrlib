
/** \file testTri_SeqMeshOptimize.cpp
 * \brief Example using dvr to sequentially optimize vertices in a triangle mesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * Uses the mesh "superior.mesh" in tecplot format.
 * Optimizes vertices in the interior along Cartesian directions.
 * Prints the final mesh, and qualities of the initial and final meshes.
 */

#include <dvr_utils_SimpleMesh.h>
#include <dvr_MeshOptimizer.h>
#include <dvr_GeomTri2DQuality.h>
#include <dvr_PartitionedMaxMinSolver.h>
#include <dvr_RelaxationDirGenerators.h>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>

using namespace dvr::utils;
using namespace dvr;

// Print the quality vector
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);


int main()
{
  SimpleMesh MD("superior.tec");
  
  // Relax all nodes except the ones on the boundary
  const std::vector<int> Ir = MD.interior_nodes();
  
  // mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Create quality
  GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);
  
  // Max-min Solver
  PartitionedMaxMinSolver<decltype(Quality)> solver(Quality);

  // Relaxation direction generator
  CartesianDirGenerator<2> dirGen;

  // Print the mesh quality before relaxation
  PrintMeshQuality("q-pre.dat", Ir, Quality);
  
  // Iteratively relax
  const int nRelaxIters = 10;
  for(int iter=0; iter<nRelaxIters; ++iter)
    {
      // Alternate between Cartesian directions with iteration#
      dirGen.iteration = iter;
      Optimize(Ir, solver, dirGen);
    }

  // Plot the mesh
  MD.save("relaxed_mesh.tec");
  
  // Print quality of the relaxed mesh
  PrintMeshQuality("q-post.dat", Ir, Quality);
}


// Print the quality vector
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
