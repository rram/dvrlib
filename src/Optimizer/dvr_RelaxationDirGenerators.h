
/** \file dvr_RelaxationDirGenerators.h
 * \brief Definition of the structs to help pass
 * relaxation directions for vertices.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021
 */

#pragma once

#include <functional>
#include <cassert>
#include <random>
#include <dvr_assert.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace dvr
{
  /** \brief Helper struct to encapsulate relaxation directions and parameters
   * \see dvr::CartesianDirGenerator, dvr::RandomDirGenerator, dvr::TangentialDirGenerator
   * \ingroup dvr_relax_dirs
   */
  struct RelaxationDirGenerator
  {
    std::function<void(const int vert_indx, const double* X, 
		       double* rdir, void* ctx)> func; /*!< Pointer to function that computes the  the relaxation direction. 
							*\param[in] vert_indx Vertex index 
							*\param[in] X Cartesian coordinate of the vertex
							*\param[out] rdir computed relaxation direction. 
							* Should be of unit norm 
							*\param[in,out] ctx User defined parameters.
							* dvr::Optimize sets it to equal dvr::RelaxationDirGenerator::params.*/
    void* params; //!< User defined parameters, possibly iteration dependent that are passed to the function generator
  };



  
  /** \brief Helper struct to generate Cartesian directions for vertex relaxation.
   *
   * The direction generator alternates between Cartesian directions 
   * based on the iteration count.
   * 
   * For this, the public member <tt>iteration</tt> is required to point to the iteration counter.
   *
   * The struct is templated by the spatial dimesion.
   * \tparam SPD Spatial dimension. Should equal 2 or 3.
   * \ingroup dvr_relax_dirs
   */
  template<int SPD>
    struct CartesianDirGenerator: public RelaxationDirGenerator
    {
      //! Constructor. 
      inline CartesianDirGenerator()
	:RelaxationDirGenerator()
      {	assert(SPD==2 || SPD==3);
	func = CartesianDirections;
	params = &iteration;  }

      int iteration; //!< Iteration counter.
      
    private:
      /** \brief Method to generate cartesian directions.
       * \param[in] vertnum Vertex index, unused 
       * \param[in] X Cartesian coordinate of the vertex, unused
       * \param[out] rdir computed relaxation direction. Has unit norm 
       * \param[in] params User defined parameters. castable to integer N, such that N%SPD indicates the coordinate direction to choose
       */
      inline static void CartesianDirections(const int vertnum, const double* X,
					     double* rdir, void* ctx)
      { assert(ctx!=nullptr && "dvr::CartesianDirections()- iteration number not set");
	const int& iter = *static_cast<const int*>(ctx);
	for(int k=0; k<SPD; ++k)
	  rdir[k] = 0.;
	rdir[iter%SPD] = 1.;
	return; }
    };



  
  /** \brief Helper struct to generate random directions for vertex relaxations.
   * A new relaxation direction is generated for each vertex at each iteration.
   * The struct is templated by the spatial dimension.
   * \tparam SPD Spatial dimension. Should equal 2 or 3.
   * \ingroup dvr_relax_dirs
   */
  template<int SPD>
    struct RandomDirGenerator: public RelaxationDirGenerator
    {
      using RandPair = std::pair<std::mt19937, std::uniform_real_distribution<>>;
      
      //! Constructor. Creates a thread-safe random number generator
      //! \param[in] nthreads Upper bound on number of threads that can beused. Defaulted to 1.
    RandomDirGenerator(const int nthreads=1)
      :RelaxationDirGenerator(),
	nMaxThreads(nthreads)
	{
	  assert(SPD==2 || SPD==3);
	    
	  // Create as many random number generators as the number of threads
	  std::random_device rd;
	  std::mt19937 gen(rd());
	  std::uniform_real_distribution<> dis(-1.,1.);
	  for(int n=0; n<nMaxThreads; ++n)
	    randvec.push_back(RandPair{gen,dis});

	  // Set base class members
	  func = RandomDirections;
	  params = &randvec;
	}
      
    private:
      const int nMaxThreads; //!< Max. number of threads specified at construction
      std::vector<RandPair> randvec; //!< Random number generators, one per thread.
      
      /** Method to generate random directions
       * \param[in] vertnum Vertex index, unused
       * \param[in] X Cartesian coordinate of the vertex, unused
       * \param[out] rdir computed relaxation direction. Has unit norm 
       * \param[in] ctx User defined parameters. Should point to a vector of random number generators
       */
      inline static void RandomDirections(const int vertnum, const double* X, double* rdir, void* ctx)
      {
	assert(ctx!=nullptr);
	auto* vec = static_cast<std::vector<RandPair>*>(ctx);

	// Access the random number generator for this thread
	int thrnum = 0;
#ifdef _OPENMP
	thrnum = omp_get_thread_num();
	assert(thrnum<static_cast<int>(vec->size()));
#endif
	auto& rp = (*vec)[thrnum];
	auto& gen = rp.first;
	auto& dis = rp.second;
	double norm = 0.;
	while(true) // Generate a vector with non zero norm
	  {
	    for(int k=0; k<SPD; ++k)
	      { rdir[k] = dis(gen);
		norm += rdir[k]*rdir[k]; }
	    if(norm>1.e-4) break;
	  }
	norm = std::sqrt(norm);
	for(int k=0; k<SPD; ++k)
	  rdir[k] /= norm;

	// -- done --
	return;	  
      }
    };
  
}

