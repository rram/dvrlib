
/** \file dvr_MeshTraits.h
 * \brief File defining the set of traits assumed for the mesh data type
 * \author Ramsharan Rangarajan
 * \date March 8, 2022
 */

#pragma once

#include <boost/tti/tti.hpp>

namespace dvr
{
  // Method to provide access to nodal coordinates
  // const double* coordinates(const int& node_index) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(coordinates);
  template<class T>
    using has_signed_member_function_coordinates = has_member_function_coordinates<const double* (T::*)(const int) const>;

  // Method to update nodal coordinates
  // void update(const int node_index, const double* X)
  BOOST_TTI_HAS_MEMBER_FUNCTION(update);
  template<class T>
    using has_signed_member_function_update = has_member_function_update<void (T::*)(const int, const double*)>;
  
  // Method providing access to element connectivities
  // const int* connectivity(const int elm_index) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(connectivity);
  template<class T>
    using has_signed_member_function_connectivity = has_member_function_connectivity<const int* (T::*)(const int) const>;

  // Returns the spatial dimension
  // int spatial_dimension() const
  BOOST_TTI_HAS_MEMBER_FUNCTION(spatial_dimension);
  template<class T>
    using has_signed_member_function_spatial_dimension = 
    has_member_function_spatial_dimension<int(T::*)() const>;

  // Returns the number of elements
  // int n_elements() const
  BOOST_TTI_HAS_MEMBER_FUNCTION(n_elements);
  template<class T>
    using has_signed_member_function_n_elements =
    has_member_function_n_elements<int(T::*)() const>;

  // Returns the number of nodes
  // int n_nodes() const
  BOOST_TTI_HAS_MEMBER_FUNCTION(n_nodes);
  template<class T>
    using has_signed_member_function_n_nodes =
    has_member_function_n_nodes<int(T::*)() const>;
  
  //! Checks that a class satisfies all mesh traits
  template<class T>
    inline constexpr bool CheckMeshTraits()
    {
      static_assert(has_signed_member_function_coordinates<T>::value, "Mesh class missing method const double* coordinates(const int node_index) const");
      
      static_assert(has_signed_member_function_update<T>::value, "Mesh class missing method void update(const int node_index, const double* X)");
      
      static_assert(has_signed_member_function_connectivity<T>::value, "Mesh class missing method const int* connectivity(const int elm_index) const");

      static_assert(has_signed_member_function_spatial_dimension<T>::value, "Mesh class is missing the method int spatial_dimension() const");
	  
      static_assert(has_signed_member_function_n_elements<T>::value, "Mesh class is missing the method int n_elements() const");

      static_assert(has_signed_member_function_n_nodes<T>::value, "Mesh class is missing the method int n_nodes() const");
      
      return true;
    }
}
