
/** \file dvr_VertexColoringTraits.h
 * \brief File defining the set of traits assumed for the Vertex coloring class
 * \author Ramsharan Rangarajan
 * \date March 17, 2022
 */

#pragma once

#include <boost/tti/tti.hpp>

namespace dvr
{
  // Method to access the vertex coloring
  // const std::vector<std::vector<int>>& GetPartitions() const
  BOOST_TTI_HAS_MEMBER_FUNCTION(GetPartitions);
  template<class T>
    using has_signed_member_function_GetPartitions = has_member_function_GetPartitions<const std::vector<std::vector<int>>& (T::*)() const>;

  //! Checks that a class satisfies all the max-min solver traits
  template<class T>
    inline constexpr bool CheckVertexColoringTraits()
    {
      static_assert(has_signed_member_function_GetPartitions<T>::value, "Vertex coloring class missing method const std::vector<std::vector<int>>& GetPartitions() const");
      return true;
    }
  
}


