
/** \file dvr_MaxMinSolverTraits.h
 * \brief File defining the set of traits assumed for the max-min solver
 * \author Ramsharan Rangarajan
 * \date March 17, 2022
 */

#pragma once

#include <boost/tti/tti.hpp>

namespace dvr
{
  // Method to compute the optimal vertex perturbation
  // double Solve(const int vert,  double* rdir, void* usrparams=nullptr) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(Solve);
  template<class T>
    using has_signed_member_function_Solve = has_member_function_Solve<double (T::*)(const int, double*, void*) const>;

  //! Checks that a class satisfies all the max-min solver traits
  template<class T>
    inline constexpr bool CheckMaxMinSolverTraits()
    {
      static_assert(has_signed_member_function_Solve<T>::value, "Max-min solver class missing method double Solve(const int vert,  double* rdir, void* usrparams=nullptr) const");

      return true;
    }
  
}
