
/** \file dvr_QualityTraits.h
 * \brief File defining the set of traits assumed for the element quality type
 * \author Ramsharan Rangarajan
 * \date March 9, 2022
 */

#pragma once

#include <boost/tti/tti.hpp>

namespace dvr
{
  // Returns reference to the mesh object
  // const MeshType&  GetMesh() const
  BOOST_TTI_HAS_MEMBER_FUNCTION(GetMesh);
  template<class Q, class M>
    using has_signed_member_function_GetMesh =
    has_member_function_GetMesh<const M& (Q::*)() const>;
     
  // Computes the quality of a given element in a mesh
  // double Compute(const int elm) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(Compute);
  template<class Q>
    using has_signed_member_function_ComputeE =
    has_member_function_Compute<double (Q::*)(const int) const>;
  
  // Compute the perturbed element quality
  // double Compute(const int elm, const int locnode, const double* rdir, const double lambda) const
  template<class Q>
    using has_signed_member_function_ComputeLambda =
    has_member_function_Compute<double (Q::*)(const int, const int, const double*, const double) const>;

  // Compute the level set of the perturbed element quality
  // void Invert(const int elm, const int locnode, const double* rdir, const double qval, double* lambda,  int& nlambda, void* usrparams) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(Invert);
  template<class Q>
    using has_signed_member_function_Invert =
    has_member_function_Invert<void (Q::*)(const int, const int, const double*, const double, double*,  int&, void*) const>;

  // Computes extrema of the perturbed element quality
  // void Extremize(const int elm, const int locnode, const double* rdir, double* lambda, double* qvals, int& nlambda) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(Extremize);
  template<class Q>
    using has_signed_member_function_Extremize =
    has_member_function_Extremize<void (Q::*)(const int, const int, const double*, double*, double*, int&) const>;

  // Computes quality curve intersections:
  // void Intersect(const int elm1, const int locnode1, const int elm2, const int locnode2, const double* rdir, double* lambda, double* qvals, int& nlambda, void* usrparams=nullptr) const
  BOOST_TTI_HAS_MEMBER_FUNCTION(Intersect);
  template<class Q>
    using has_signed_member_function_Intersect =
    has_member_function_Intersect<void (Q::*)(const int, const int, const int, const int, const double*, double*, double*, int&, void*) const>;

  // Helper to check quality traits
  template<class Q, class M>
    inline constexpr bool CheckQualityTraits()
  {
    static_assert(has_signed_member_function_GetMesh<Q,M>::value,
		  "Quality class is missing the method const MeshType&  GetMesh() const ");
    
    static_assert(has_signed_member_function_ComputeE<Q>::value,
		  "Quality class is missing the method double Compute(const int elm) const");

    static_assert(has_signed_member_function_ComputeLambda<Q>::value,
		  "Quality class is missing the method double Compute(const int elm, const int locnode, const double* rdir, const double lambda) const");

    static_assert(has_signed_member_function_Invert<Q>::value,
		  "Quality class is missing the method Invert(const int elm, const int locnode, const double* rdir, const double qval, double* lambda,  int& nlambda, void* usrparams) const");
    
    static_assert(has_signed_member_function_Extremize<Q>::value,
		  "Quality class is missing the method void Extremize(const int elm, const int locnode, const double* rdir, double* lambda, double* qvals, int& nlambda) const");
    
    static_assert(has_signed_member_function_Intersect<Q>::value,
		  "Quality class is missing the method void Intersect(const int elm1, const int locnode1, const int elm2, const int locnode2, const double* rdir, double* lambda, double* qvals, int& nlambda, void* usrparams=nullptr) const");

    return true;
  }
}

