
/** \file dvr_VertexColoring_Impl.h
 * \brief Implementation of the class dvr::VertexColoring.
 * \author Ramsharan Rangarajan
 * \date March 22, 2020.
 */

#pragma once

#include <unordered_map>
#include <algorithm>
#include <cassert>
#include <dvr_MeshTraits.h>
#include <dvr_VertexColoringTraits.h>

namespace dvr
{
  // Constructor
  template<typename MeshType>
    VertexColoring<MeshType>::
    VertexColoring(const MeshWrapper<MeshType>& mw,
		   const std::vector<int>& ir)
    :mesh_wrapper(mw),
    MD(mesh_wrapper.GetConstMesh()),
    Ir(ir)
  {
    // Check vertex shader requirements
    static_assert(CheckVertexColoringTraits<VertexColoring<MeshType>>(), "Vertex coloring class failed to satisfy requirements");
    
    // Initialize all vertices in Ir to an invalid color
    std::unordered_map<int, int> VertColors{};
    for(auto& n:Ir) VertColors[n] = -1;

    // Track number of colors assigned
    int nColors = 0;

    // Keep track of sizes of color groups
    std::vector<int> ColorGroupSize{};
	
    // Track colors of vertices in the 1-ring
    // ringColors[colornum] = +1 if 'colornum' has been taken, equals -1 otherwise
    // Size is increased as needed.
    std::vector<int> ringColors{};
    std::vector<int>::iterator itMissing; // Iterator to missing color
    int mycolor;
	
    // Start coloring
    for(const auto& vert:Ir)
      {
	// Vertices in the 1-ring
	const auto& nbVerts = mesh_wrapper.Get1RingVertices(vert);
	const int n1RingVerts = static_cast<int>(nbVerts.size());
	    
	// Inspect neighbors of this vertex
	std::fill(ringColors.begin(), ringColors.end(), -1);
	for(int j=0; j<n1RingVerts; ++j)
	  {
	    // Neighboring vertex
	    const int& nbvert = nbVerts[j];

	    // Color of the neighboring vertex
	    auto it = VertColors.find(nbvert);
	    if(it==VertColors.end()) continue; // This vertex is not in Ir.
	    const int& nbcolor = it->second;
		
	    // If neighbor has been assigned a color, note that it is not available
	    if(nbcolor!=-1) ringColors[nbcolor] = 1;
	  }
	    
	// Find the smallest missing color in the 1-ring
	itMissing = std::find(ringColors.begin(), ringColors.end(), -1);
	if(itMissing==ringColors.end())
	  {
	    // A new color is required
	    VertColors[vert] = nColors++;
	    ringColors.resize(nColors);
	    ColorGroupSize.resize(nColors);
	    ColorGroupSize[nColors-1] = 1;
	  }
	else
	  {
	    // Use an existing color
	    mycolor =  (itMissing-ringColors.begin());
	    VertColors[vert] = mycolor;
	    ++ColorGroupSize[mycolor];
	  }
      }

    // Group vertices by color
    VertGroups.resize(nColors);
    for(int i=0; i<nColors; ++i)
      VertGroups[i].resize(ColorGroupSize[i]);
    
    // Track the number of vertices added to each group
    std::vector<int> nVertsInGroup(nColors);
    std::fill(nVertsInGroup.begin(), nVertsInGroup.end(), 0);
	
    // Assign vertices to color groups
    for(auto& it:VertColors)
      { const int& vert = it.first;
	const int& color = it.second;
	assert(color!=-1);
	VertGroups[color][nVertsInGroup[color]++] = vert; }

    // done
  }


    // Consistency test
  template<typename MeshType>
    void VertexColoring<MeshType>::ConsistencyTest() const
    {
      // Create a map vertex# --> color
      std::unordered_map<int, int> VertColors{};
      
      // Check number of vertices assigned each color
      const int nColors = static_cast<int>(VertGroups.size());
      std::vector<int> groupSizes(nColors);
      std::fill(groupSizes.begin(), groupSizes.end(), 0);
      for(int c=0; c<nColors; ++c)
	for(auto& vert:VertGroups[c])
	  {
	    VertColors[vert] = c;
	    ++groupSizes[c];
	  }

      // Checks #vertices assigned each color
      for(int c=0; c<nColors; ++c)
	{ assert(groupSizes[c]>0);  // A color should have been created only if needed
	  assert(groupSizes[c]==static_cast<int>(VertGroups[c].size())); }

      // Check correctness of coloring
      for(auto& vert:Ir)
	{
	  auto it = VertColors.find(vert);
	  assert(it!=VertColors.end());    // All vertices in Ir should be assigned a color
	  const int& mycolor = it->second;
	  assert(mycolor>=0 && mycolor<nColors);
	  
	  // Check colors of neighbors
	  const auto& nbVerts = mesh_wrapper.Get1RingVertices(vert);
	  const int n1RingVerts = static_cast<int>(nbVerts.size());
	  for(int j=0; j<n1RingVerts; ++j)
	    {
	      const int& nbvert = nbVerts[j];
	      auto jt = VertColors.find(nbvert);
	      if(jt!=VertColors.end())           // Only neighbors in Ir are colored
		assert(mycolor!=jt->second);
	    }
	}

      // -- done --
      return;
    }
}

