
/** \file testTri_VertexColoring.cpp
 * \brief Unit test for the class dvr::VertexColoring.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020.
 *
 * Checks consistency of the coloring using an example of a triangle mesh.
 *
 * Inputs: none
 *
 * Outputs: none, if successful and an assertion otherwise.
 */

#include <dvr_VertexColoring.h>
#include <dvr_utils_SimpleMesh.h>
#include <random>
#include <algorithm>
#include <cstdlib>

using namespace dvr::utils;
using namespace dvr;

int main()
{
  SimpleMesh MD("superior.tec");

  // Mesh wrapper
  MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Color a random subset of vertices
  std::vector<int> Ir{};
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution dis(0.8); // true 8/10 times
  int nnodes = MD.n_nodes();
  for(int n=0; n<nnodes; ++n)
    if(dis(gen))
      Ir.push_back(n);
  std::shuffle(Ir.begin(), Ir.end(), gen);

  VertexColoring<decltype(MD)> vshades(mesh_wrapper, Ir);
  vshades.ConsistencyTest();
}
