
/** \file dvr_VertexColoring.h
 * \brief Definition of the class dvr::VertexColoring.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <vector>
#include <dvr_MeshWrapper.h>

namespace dvr{  
  /** \brief Class implementing a sequential greedy vertex coloring algorithm.
   *
   * This class partitions a given list \f${\rm I}_{\rm R}\f$
   * of vertices in a triangle/tetrahedral mesh \f${\cal T}\f$
   * to compute a coloring \f${\rm color}:{\rm I}_{\rm R}\rightarrow {\mathbb Z}\f$.
   *
   * The computed coloring satisfies the following property:
   * \f[ ~i,j\in {\rm I}_{\rm R}~{\rm and }~j\in\,\text{ 1-ring of } i ~\Rightarrow
   * {\rm color}(i)\neq {\rm color}(j).\f]
   *
   * The class explicitly computes a coloring function and stores
   * the resulting partitioning, which is accessed using the method
   *  dvr::VertexColoring::GetPartitions
   *
   * The method dvr::VertexColoring::ConsistencyTest verifies that the
   * computed coloring satisfies the property mentioned above.
   *
   * The partitioning computed by the class is used by the
   * parallelized dvr::Optimize routine to concurrently optimize the
   * locations of vertices that are assigned the same color.
   *
   * The class is templated by the mesh type.
   * \tparam MeshType typename of the mesh data structure.
   *
   * \see Unit test for the class using a triangle mesh: testVertexColoring.cpp
   * \see Parallel dvr::Optimize routine in dvr_funcs
   * \ingroup dvr_helpers
   */
  template<typename MeshType>
    class VertexColoring
    {
    public:
      //! Constructor, computes the vertex partitioning.
      //! \param[in] mw mesh wrapper, referenced
      //! \param[in] ir List of vertices to partition. Referenced.
      VertexColoring(const MeshWrapper<MeshType>& mw,
		     const std::vector<int>& ir);
      
      //! Returns a reference to the mesh used
      inline const MeshType& GetMesh() const
      { return MD; }

      //! Returns a reference to the list of relaxed vertices
      inline const std::vector<int>& GetRelaxedVertices() const
      { return Ir; }
      
      /** Access to the vertex coloring.
       * \return Partitioning <tt>VertGroups</tt> such that
       * <tt>VertGroups[i]</tt> contains the list of all vertices
       * in <tt>Ir</tt> assigned the color <tt>i</tt>.
       */
      inline const std::vector<std::vector<int>>& GetPartitions() const
      { return VertGroups; }

      //! Checks correctness of the computed vertex coloring.
      void ConsistencyTest() const;

    private:
      const MeshWrapper<MeshType>& mesh_wrapper; //!< Reference to the mesh wrapper object
      const MeshType& MD; //!< Reference to the mesh object
      const std::vector<int>& Ir; //!< Reference to the list of colored vertices
      std::vector<std::vector<int>> VertGroups; //!< Partitioning of vertices in Ir.
    };
}


// Implementation
#include <dvr_VertexColoring_Impl.h>
