
cmake_minimum_required(VERSION 3.19)

project("DVRlib" VERSION 1
  	    LANGUAGES CXX
  	    DESCRIPTION "A C++ library of directional vertex relaxation")

# Set runtime path (for Linux)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${PROJECT_NAME}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# build options
option(BUILD_TESTS "Run unit tests" ON)
option(BUILD_DOCS "Build documentation" OFF)

# Required packages
find_package(GSL REQUIRED)
find_package(Boost REQUIRED)
find_package(OpenMP)
if(BUILD_DOCS)
	find_package(Doxygen REQUIRED)
endif()

# check GSL
try_compile(GSL_SUCCESS
	"${CMAKE_BINARY_DIR}/dependency_tests/"
	"${CMAKE_SOURCE_DIR}/dependency_tests/test_gsl.cpp"
	CMAKE_FLAGS "-DINCLUDE_DIRECTORIES=${GSL_INCLUDE_DIRS}" "-DLINK_LIBRARIES=${GSL_LIBRARIES}"
	OUTPUT_VARIABLE TRY_GSL_SUCCESS)
if(NOT ${GSL_SUCCESS})
	       message(FATAL_ERROR "Problem using GSL. Could not compile dependency test ")
else()
		message(STATUS "GSL works")
endif()

# check OpenMP
if(OpenMP_FOUND)

	try_compile(OMP_SUCCESS
			"${CMAKE_BINARY_DIR}/dependency_tests/"
			"${CMAKE_SOURCE_DIR}/dependency_tests/test_openmp.cpp"
			CMAKE_FLAGS
						"-DINCLUDE_DIRECTORIES=${OpenMP_CXX_INCLUDE_DIRS}"
						"-DLINK_LIBRARIES=${OpenMP_CXX_LIBRARIES}"
			OUTPUT_VARIABLE TRY_OMP_SUCCESS)

	if(NOT ${OMP_SUCCESS})
	       message(FATAL_ERROR "Problem using OpenMP_CXX. Could not compile dependency test")
	else()
		message(STATUS "OpenMP_CXX works")
        endif()

endif()

# Compile features
list(APPEND DVR_COMPILE_FEATURES cxx_std_17)

# Compile code
add_subdirectory(src)
add_subdirectory(utils)

# Testing
if(${BUILD_TESTS})  
	include(CTest)
	add_subdirectory(utils/test)
	add_subdirectory(src/Poly/test)
	add_subdirectory(src/Quality/test)
	add_subdirectory(src/Maxmin/test)
	add_subdirectory(src/VertexColoring/test)
	add_subdirectory(src/Optimizer/test)	
endif()

# install 
include (GNUInstallDirs)
include(CMakePackageConfigHelpers)

install(TARGETS
	dvr_assert
	dvr_traits
	dvr_meshwrapper
	dvr_vertcolor
  	dvr_poly
  	dvr_quality
  	dvr_maxmin
  	dvr_optimizer
  	dvr_utils
  	EXPORT ${PROJECT_NAME}Targets
  	DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}
  	INCLUDES DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})

install(EXPORT ${PROJECT_NAME}Targets
  	       DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  	       NAMESPACE DVRlib::
  	       FILE ${PROJECT_NAME}Targets.cmake)

set(DVRlib_INCLUDE_DIRS_VAR ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})
set(DVRlib_LIBRARY_DIRS_VAR ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR})

configure_package_config_file(
	"cmake/${PROJECT_NAME}Config.cmake.in" "${PROJECT_NAME}Config.cmake"
  	INSTALL_DESTINATION
  	${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  	PATH_VARS DVRlib_INCLUDE_DIRS_VAR DVRlib_LIBRARY_DIRS_VAR)

write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  	VERSION ${PACKAGE_VERSION}
  	COMPATIBILITY AnyNewerVersion)

install(FILES
	"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  	"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
  	DESTINATION "${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")

set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/LICENSE.txt")

if(BUILD_DOCS)
	add_subdirectory(docs)
endif()
