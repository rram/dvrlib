
/** \file dvr-constraint.cpp
 * \brief Tutorial-style example explaining the use of dvr to sequentially improve a triangle mesh of a circle.
 * The example explains using a sampling-based max-min solver to perturb vertices 
 * lying on the boundary of the mesh.
 * It also demonstrates using a custom relaxation direction generator.
 *
 * \author Ramsharan Rangarajan
 * \date November 30, 2020
 *
 * This is an example demonstrating the use of the \b dvr library to 
 * improve a mesh of planar triangles while constraining boundary vertices to lie on a circle
 * and relaxing the interior nodes alternately along radial and circumferential directions.
 * 
 * The example proceeds as follows:
 * 
 * - Read a file called "circle-rad-0p5.mesh" in tecplot format
 * to a mesh object of type SimpleMesh
 * 
 * - Identify the set of interior nodes. 
 *   These will be relaxed along radial and circumferential directions during alternate iterations.
 *
 * - Identify the set of boundary vertices, which will be relaxed along the local tangent to the circular boundary
 * 
 * - Create a triangle mesh quality evaluator of type dvr::GeomTri2DQuality
 * 
 * - Create a max-min solver of type dvr::ReconstructiveMaxMinSolver to relax interior nodes.
 *
 * - Create a max-min solver of type dvr::SamplingMaxMinSovler to relax boundary nodes. 
 *   The constraint that boundary vertices lie on a circle is passed to the solver.
 *
 * - Create a relaxation direction generator furnishing radial and circumferential directions for each interior vertex
 *   at each iteration
 *
 * - Create a relaxation direction generator furnishing local tangential directions for the boundary vertices
 * 
 * - Create a closest point projector that constrains boundary nodes to remain constrained to the circular boundary.
 *
 * - Iteratively call the function dvr::Optimize to sequentially relax interior and boundary vertices
 * 
 * - The example prints the mesh in Tecplot format, and the mesh quality vector at the end of each iteration.
 * 
 * Running this example: 
 * - Build
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   cmake ../
 *   make dvr-constraint
 *   \endcode
 *
 * - Execute: <tt>./dvr-constraint </tt>
 * 
 * ----
 */

// Include all header files relevant to improving a triangle mesh
//! [doc_include_header]
#include <DVRlib>
//! [doc_include_header]

// Mesh data structure. Not part of the library
#include <dvr_utils_SimpleMesh.h>
#include <cstdlib>
#include <random>
#include <fstream>

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);

//! [doc_polar]
// Generate relaxation directions along radial and circumferential directions
// ctx is assumed to point to the iteration counter
void PolarDirGenerator(const int vert_indx, const double *X, double *rdir, void *ctx)
{ assert(ctx!=nullptr);
  const int& iter = *static_cast<const int*>(ctx);
  rdir[0] = 0.; rdir[1] = 0.;
  const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);

  // At the origin, relax along Ex and Ey
  if(r<1.e-3) 
    { rdir[iter%2] = 1.; return; }

  // Elsewhere, return Er and Etheta
  if(iter%2==0)
    { rdir[0] = X[0]/r; rdir[1] = X[1]/r; return;}
  else
    { rdir[0] = -X[1]/r; rdir[1] = X[0]/r; return; }
}
//! [doc_polar]


//! [doc_sdfunc]
// Signed distance function calculation for a circle of radius 0.5
void CircleSDFunc(const double* X, double& sd, double* dsd)
{
  // sd(x) = sqrt(x^2+y^2)-0.5, grad(sd) = Er.
  const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
  sd = r-0.5; 
  if(dsd!=nullptr) 
    { assert(r>1.e-3);
      dsd[0] = X[0]/r; 
      dsd[1] = X[1]/r; }
  return;
}
//! [doc_sdfunc]


// Only class and functions with the dvr:: namespace are part
// of the library; rest are provided for demonstration and testing purposes

int main()
{
  // Read a mesh in tecplot format
  // The "SimpleMesh" class and related utilities for reading and writing
  // meshes is provided along with this tutorial folder for convenience
  // and for the purpose of testing.
  // See the documentation for assumptions on the mesh data structure
  //! [doc_read_mesh]
  dvr::utils::SimpleMesh MD("sample_meshes/circle-rad-0p5.tec");
  //! [doc_read_mesh]

  // Partition the set of nodes into interior and boundary nodes.
  // Interior nodes are relaxed along randomly generated directions
  // Boundary nodes are relaxed along local tangential directions
  //! [doc_IR]
  const std::vector<int> intNodes = MD.interior_nodes();
  const std::vector<int> bdNodes  = MD.boundary_nodes();
  //! [doc_IR]

  // All mesh related data has been setup. DVR functionalities from here on.

  // Mesh wrapper
  dvr::MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Object to evaluate element qualities
  // Notice templating by mesh type
  //! [doc_quality]
  dvr::GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);
  //! [doc_quality]

  //! [doc_int_solver]
  // Create max-min solver for relaxing interior nodes (unconstrained)
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> intSolver(Quality);
  //! [doc_int_solver]
  
  // Choose random directions for relaxation
  //! [doc_polar_dir]
  dvr::RelaxationDirGenerator polarDirGen{.func=PolarDirGenerator, .params=nullptr};
  //! [doc_polar_dir]

  //! [doc_bd]
  // Create sampling-based max-min solver for relaxing boundary nodes, which are constraint to lie on a circle
  const int nSamples = 20; // Number of sampling points used by the solver
  dvr::SamplingMaxMinSolver<decltype(Quality)> bdSolver(Quality, nSamples);

  // Any sample point generated by the solver for a node on the boundary
  // is projected to the closest point on the constraint boundary.
  dvr::SignedDistanceFunction sdfunc(&CircleSDFunc);
  dvr::ClosestPointStruct<2> cpt(CircleSDFunc);

  // Relax boundary nodes along tangential directions.
  dvr::TangentialDirGenerator<2> tgtDirs(CircleSDFunc);
  //! [doc_bd]

  // Print the mesh quality before relaxation
  //! [doc_mesh_quality]
  const int nNodes = MD.n_nodes();
  std::vector<int> allnodes(nNodes);
  std::iota(allnodes.begin(), allnodes.end(), 0);
  PrintMeshQuality("output-dvr-constraint/q-pre.dat", allnodes, Quality);
  //! [doc_mesh_quality]

  // Iteratively relax
  const int Nr = 20; // Number of iterations
  for(int iter=0; iter<Nr; ++iter)
    {
      std::cout<<"\nIteration #"<<iter<<std::flush;

      //! [doc_iteration]
      // Relax interior vertices along randomly generated directions
      polarDirGen.params = &iter;
      dvr::Optimize(intNodes, intSolver, polarDirGen);
      
      // Relax boundary nodes
      // The closest point projection for the circular boundary is passed as a parameter (i.e., cpt)
      dvr::Optimize(bdNodes, bdSolver, tgtDirs, &cpt);
      //! [doc_iteration]
      
      // Optional: print the mesh and its quality after each iteration
      MD.save("output-dvr-constraint/MD-"+std::to_string(iter)+".tec");
      PrintMeshQuality("output-dvr-constraint/q-"+std::to_string(iter)+".dat", allnodes, Quality);
    }

    // Print the final relaxed mesh and its quality
  MD.save("output-dvr-constraint/relaxed_mesh.tec");
  PrintMeshQuality("output-dvr-constraint/q-post.dat", allnodes, Quality);

  std::cout<<"\n---done---\n"<<std::flush;
  // done
}

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
