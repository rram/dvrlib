
/** \file dvr-triangle.cpp
 * \brief Tutorial-style example explaining the use of dvr to sequentially optimize vertices in a triangle mesh.
 * \author Ramsharan Rangarajan
 * \date March 23, 2020
 *
 * This is an example demonstrating the use of DVRlib to relax a mesh of planar triangles.
 * 
 * The example proceeds as follows:
 * 
 * - Read a file called "superior.mesh" in tecplot format
 * to a mesh object of type SimpleMesh
 * 
 * - Set the list of nodes to be relaxed, labeled \f${\rm I}_{\rm R}\f$
 * to be the set of all interior nodes, i.e., all nodes not lying on the 
 * boundary of the mesh
 * 
 * - Create a triangle mesh quality evaluator of type dvr::GeomTri2DQuality
 * 
 * - Create a max-min solver of type dvr::ReconstructiveMaxMinSolver
 * The dvr::PartitionedMaxMinSolver can be used as well. The performance of the two solvers
 * is not drastically different when relaxing 2D triangle meshes, although the latter is expected
 * to be more efficient in general.
 *
 * - Create a relaxation direction generator of type dvr::RelaxationDirGenerator
 * A pre-implemented function called dvr::CartesianDirections templated by the 
 * spatial dimension is used to alternate between Cartesian directions over 
 * successive iterations. 
 * 
 * - Iteratively class the function dvr::Optimize to sequentially relaxed
 * vertex positions in \f${\rm I}_{\rm R}\f$.
 * 
 * - The example prints a "mesh quality vector" at the end of each iteration
 * to a file. These files can be used to inspect the magnitude of improvement in 
 * element qualities, to monitor convergence of mesh qualities and hence to determine
 * a suitable number of relaxation iterations
 *
 * - The example prints the mesh realized at the end of each iteration 
 * in Tecplot format for the sake of visualization.
 * Notice that meshes differ from each other only in 
 * the locations of vertices in the list \f${\rm I}_{\rm R}\f$.
 * 
 * Running this example: 
 * - Build 
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   cmake $DVRDIR/tutorial/
 *   make dvr-triangle
 *   \endcode
 * 
 * - Execute: <tt>./dvr-triangle </tt>
 * 
 * ----
 */

// Include all header files relevant to improving a triangle mesh
//! [doc_include_header]
#include <DVRlib>
//! [doc_include_header]

// Mesh data structure. Not part of the library
#include <dvr_utils_SimpleMesh.h>
#include <cstdlib>
#include <fstream>

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);

// Only class and functions with the dvr:: namespace are part
// of the library; rest are provided for demonstration and testing purposes

int main()
{
  // Read a mesh in tecplot format
  // The "SimpleMesh" class and related utilities for reading and writing
  // meshes is provided along with this tutorial folder for convenience
  // and for the purpose of testing.
  // See the documentation for assumptions on the mesh data structure
  //! [doc_read_mesh]
  dvr::utils::SimpleMesh MD("sample_meshes/superior.tec");
  //! [doc_read_mesh]

  // Choose nodes to relax: Ir
  // In this example, we relax all nodes except the ones lying
  // on the boundary of the mesh.
  // These nodes are identified as the complement of the set of boundary nodes
  // Then set Ir in MD to compute 1-rings of vertices/elements around nodes in Ir.
  //! [doc_IR]
  const std::vector<int> Ir = MD.interior_nodes();
  //! [doc_IR]

  // All mesh related data has been setup. DVR functionalities from here on.

  // Mesh wrapper
  dvr::MeshWrapper<decltype(MD)> mesh_wrapper(MD);
  
  // Object to evaluate element qualities
  // Notice templating by mesh type
  //! [doc_quality]
  dvr::GeomTri2DQuality<decltype(MD)> Quality(mesh_wrapper);
  //! [doc_quality]

  // Create max-min solver
  //! [doc_solver]
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality);
  //! [doc_solver]

  // Relaxation direction generator
  //! [doc_rdir]
  dvr::CartesianDirGenerator<2> dirGen;
  //! [doc_rdir]

  // Print the mesh quality before relaxation
  //! [doc_mesh_quality]
  PrintMeshQuality("output-dvr-triangle/q-pre.dat", Ir, Quality);
  //! [doc_mesh_quality]

  // Iteratively relax
  //! [doc_iteration]
  const int Nr = 10; // Number of iterations
  for(int iter=0; iter<Nr; ++iter)
    {
      std::cout<<"\nIteration #"<<iter<<std::flush;
      
      // Alternate between Cartesian directions with each iteration
      dirGen.iteration = iter;
      Optimize(Ir, solver, dirGen);

      // Optional: print the mesh and its quality after each iteration
      MD.save("output-dvr-triangle/MD-"+std::to_string(iter)+".tec");
      PrintMeshQuality("output-dvr-triangle/q-"+std::to_string(iter)+".dat", Ir, Quality);
    }
  //! [doc_iteration]

  // Print the final relaxed mesh and its quality
  MD.save("output-dvr-triangle/relaxed_mesh.tec");
  PrintMeshQuality("output-dvr-triangle/q-post.dat", Ir, Quality);

  std::cout<<"\n---done---\n"<<std::flush;
  // done
}

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Print to file
  std::fstream qfile;
  qfile.open(filename, std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}
