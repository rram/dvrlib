
/** \file dvr-cmdline.cpp
 * \brief Tutorial-style example demonstrating using dvr with command line options.
 * \author Ramsharan Rangarajan
 *
 * An example to run \b dvr from the command line using a sequence
 * of arguments to relax either triangle or tetrahedral meshes.
 * The input and output file names, and the number of 
 * relaxation iterations are provided through commandline arguments.
 *
 * Assumes the following:
 * - Input file in tecplot format
 * - Output file in tecplot format
 * - All interior nodes are relaxed
 * - Relaxation is along Cartesian or randomly generated directions
 * during success iterations
 *
 * The CLI11 library is used to create and parse command line switches.
 *
 * Use the following switches:
 * - -d 
 *    (optional) print files at each iteration, defaulted to false
 * - -c or --cartesian
 *     (optional) relax along Cartesian directions if specified, otherwise along random directions
 *
 * - -n (integer)
 *   (required)  number of relaxation iterations
 *
 * - -o (string)
 *   (required)  output tec filename
 *
 * - -i (string)
 *   (required)  input tec filename
 *
 * - -j (integer)
 *   (optional) number of threads, defaulted to 1
 *
 * Running this example: 
 * - Build 
 *   \code{.sh}
 *   mkdir build; 
 *   cd build
 *   cmake $DVRDIR/tutorial/
 *   make dvr-cmdline
 *   \endcode
 *
 * - Help with command line options: <tt> ./dvr-cmdline --help </tt>
 * - Execute: <tt>./dvr-cmdline -i input-file.tec -o output-file.tec -n num-relax-iters -j num-threads -c </tt>
 * 
 * ----
 */

// DVR stuff
#include <DVRlib>

// SimpleMesh data structure
#include <dvr_utils_SimpleMesh.h>

// CLI for reading commandline arguments
#include <CLI/CLI.hpp>

#include <fstream>

// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality);

// Sequentially optimize a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void SeqOptimizeMesh(dvr::utils::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const bool flag_cartesian, const bool flag_details);

// Parallel optimizatione a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void ompOptimizeMesh(dvr::utils::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const int nThreads,
		     const bool flag_cartesian, const bool flag_details);


int main(int argc, char** argv)
{
  // Commandline options
  CLI::App app;  

  app.footer("Usage instructions:  \
             \n==================					\
             \n(i) relax along Cartesian directions: ./dvr-cmdline -i input-file.tec -o output-file.tec -n num-relax-iters -j num-threads -c \
             \n(ii) relax along random directions: ./dvr-cmdline -i input-file.tec -o output-file.tec -n num-relax-iters -j num-threads \
             \n(iii) save details of relaxation iterations: ./dvr-cmdline -i input-file.tec -o output-file.tec -n num-relax-iters -j num-threads -d");
    
  // Input filename
  std::string infile;
  app.add_option("-i", infile, "input file in tec format")->required()->check(CLI::ExistingFile);

  // Output filename
  std::string outfile;
  app.add_option("-o", outfile, "output file in tec format")->required();

  // Number of relaxation iterations
  int nR;
  app.add_option("-n", nR, "number of relaxation iterations")->required();

  // Number of threads
  int nThreads = 1;
  app.add_option("-j", nThreads, "number of threads");
  
  // Relax along Cartesian directions
  bool flag_cartesian = false;
  app.add_flag("-c", flag_cartesian, "relax along Cartesian directions, default = false. Otherwise along random directions");

  // Print details during iterations?
  bool flag_details = false;
  app.add_flag("-d", flag_details, "print mesh and qualities at each iteration, default = false");

  // Parse
  CLI11_PARSE(app, argc, argv);
  
  // #threads & openMP
#ifndef _OPENMP
  if(nThreads!=1)
    std::cout << std::endl << "OpenMP not found, resetting #threads = 1";
#endif

  // Print options
  std::cout << std::endl << "Options"
	    << std::endl << "========================"
	    << std::endl << "Input mesh file: "       << infile
	    << std::endl << "Output file: "           << outfile
	    << std::endl <<"# relaxation iterations: "<<nR
	    << std::endl <<"Relaxation directions: "  << (flag_cartesian? "Cartesian" : "Random")
	    << std::endl <<"# threads: "              << nThreads
	    << std::endl <<"Print mesh/quality at each iteration: " << (flag_details? "true" : "false")
	    <<std::endl;
  
  // Read the mesh
  dvr::utils::SimpleMesh MD(infile);

  // Nodes to relax
  const std::vector<int> Ir = MD.interior_nodes();
  std::cout << "\n\nNumber of relaxed nodes: "<<Ir.size()<<std::flush;
  switch(nThreads)
    {
    case 1:   // sequential
      {
	if(MD.spatial_dimension()==2) // Triangles
	  SeqOptimizeMesh<dvr::GeomTri2DQuality<decltype(MD)>,2>(MD, Ir, outfile, nR, flag_cartesian, flag_details);
	else // Tets
	  SeqOptimizeMesh<dvr::GeomTet3DQuality<decltype(MD)>,3>(MD, Ir, outfile, nR, flag_cartesian, flag_details);
	break;
      }
      
    default: // parallel?
      {
	if(MD.spatial_dimension()==2) // Triangles
	  ompOptimizeMesh<dvr::GeomTri2DQuality<decltype(MD)>,2>(MD, Ir, outfile, nR, nThreads, flag_cartesian, flag_details);
	else // Tets
	  ompOptimizeMesh<dvr::GeomTet3DQuality<decltype(MD)>,3>(MD, Ir, outfile, nR, nThreads, flag_cartesian, flag_details);
      }
    }
  
  std::cout<<"\n---done---\n"<<std::flush;
}

// Sequentially optimize a mesh of triangles or tetrahedra
template<class QType, const int SPD>
void SeqOptimizeMesh(dvr::utils::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const bool flag_cartesian, const bool flag_details)
{
  std::cout<<"\n\nExecuting sequential algorithm. "<<std::flush;

  // Mesh wrapper
  dvr::MeshWrapper<dvr::utils::SimpleMesh> mesh_wrapper(MD);
  
  // Mesh quality
  QType Quality(mesh_wrapper);

  // Solver
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality);

  // Relaxation directions
  dvr::RelaxationDirGenerator* dirGen;
  if(flag_cartesian==true)
    dirGen = new dvr::CartesianDirGenerator<SPD>;
  else
    dirGen = new dvr::RandomDirGenerator<SPD>;
  
  // Initial quality
  PrintMeshQuality("output-dvr-cmd/q-pre.dat", Ir, Quality);

  // Iteratively relax
  std::vector<double> dt(nR);
  for(int iter=1; iter<=nR; ++iter)
    {
      std::cout<<"\n\nIteration #"<<iter<<std::flush;

      if(flag_cartesian) dirGen->params = &iter;
      
      double tstart = omp_get_wtime();
      dvr::Optimize(Ir, solver, *dirGen);
      dt[iter-1] = omp_get_wtime()-tstart;

      if(flag_details)
	{
	  //MD.save(("MD-"+std::to_string(iter)+".tec");
	  PrintMeshQuality("output-dvr-cmd/q-"+std::to_string(iter)+".dat", Ir, Quality);
	  std::cout<<", dt = "<<dt[iter-1]<<std::flush;
	}
    }
  
  // Final mesh and its quality
  MD.save("output-dvr-cmd/"+outfile);
  PrintMeshQuality("output-dvr-cmd/q-post.dat", Ir, Quality);

  // Print average time per iteration
  double tavg = 0.;
  for(auto& t:dt) tavg += t;
  tavg /= static_cast<double>(nR);
  std::cout<<"\n\nAverage time per iteration: "<<tavg<<"\n"<<std::flush;

  delete dirGen;
  
  // -- done --
  return;
}

// Optimize a mesh of triangles or tetrahedra in parallel
template<class QType, const int SPD>
void ompOptimizeMesh(dvr::utils::SimpleMesh& MD, const std::vector<int>& Ir,
		     const std::string& outfile, const int nR,
		     const int nThreads,
		     const bool flag_cartesian, const bool flag_details)
{
  std::cout<<"\n\nExecuting parallel algorithm. "<<std::flush;

  // Mesh wrapper
  dvr::MeshWrapper<dvr::utils::SimpleMesh> mesh_wrapper(MD);
  
  // Mesh quality
  QType Quality(mesh_wrapper, nThreads);

  // Solver
  dvr::ReconstructiveMaxMinSolver<decltype(Quality)> solver(Quality, nThreads);

  // Vertex coloring
  dvr::VertexColoring<dvr::utils::SimpleMesh> vshades(MD, Ir);
  
  // Set the number of threads
#ifndef _OPENMP
  assert(false && "openMP not available");
#endif
  const int nMaxThreads = omp_get_max_threads()/2;
  assert(nThreads<=nMaxThreads && "Exceeded maximum number of threads");
  omp_set_num_threads(nThreads);
  
  // Relaxation directions
  dvr::RelaxationDirGenerator* dirGen;
  if(flag_cartesian==true)
    dirGen = new dvr::CartesianDirGenerator<SPD>;
  else
    dirGen = new dvr::RandomDirGenerator<SPD>(nThreads);
  
  // Initial quality
  PrintMeshQuality("output-dvr-cmd/q-pre.dat", Ir, Quality);

  // Iteratively relax
  std::vector<double> dt(nR);
  for(int iter=1; iter<=nR; ++iter)
    { std::cout<<"\n\nIteration #"<<iter<<std::flush;

      if(flag_cartesian)
	dirGen->params = &iter;
      
      double tstart = omp_get_wtime();
      dvr::Optimize(Ir, solver, *dirGen, vshades);
      dt[iter-1] = omp_get_wtime()-tstart;

      if(flag_details)
	{
	  //MD.save("MD-"+std::to_string(iter)+".tec");
	  PrintMeshQuality("output-dvr-cmd/q-"+std::to_string(iter)+".dat", Ir, Quality);
	  std::cout<<": dt = "<<dt[iter-1]<<std::flush;
	}
    } 

  // Print average time per iteration
  double tavg = 0.;
  for(auto& t:dt) tavg += t;
  tavg /= static_cast<double>(nR);
  std::cout<<"\n\nAverage time per iteration: "<<tavg<<"\n"<<std::flush;
  
  // Final mesh and its quality
  MD.save("output-dvr-cmd/"+outfile);
  PrintMeshQuality("output-dvr-cmd/q-post.dat", Ir, Quality);

  delete dirGen;
  // done
}



// Print the quality vector measuring mesh quality
template<class QType>
void PrintMeshQuality(const std::string& filename,
		      const std::vector<int>& Ir,
		      const QType& Quality)
{
  // Sorted vector of qualities at vertices
  const int nVerts = static_cast<int>(Ir.size());
  std::vector<double> qvec(nVerts);
  for(int i=0; i<nVerts; ++i)
    qvec[i] = dvr::ComputeVertexQuality(Ir[i], Quality);
  std::sort(qvec.begin(), qvec.end());

  // Write the minimum quality on screen
  std::cout<<"\nMinimum quality: "<<qvec[0]<<std::flush;
  
  // Print to file
  std::fstream qfile;
  qfile.open(filename.c_str(), std::ios::out);
  assert(qfile.good());
  qfile<<"# Index \t Vertex quality";
  for(int i=0; i<nVerts; ++i)
    qfile<<"\n"<<i+1<<" "<<qvec[i];  
  qfile.flush(); qfile.close();
  return;
}

// Read command line arguments
void ReadCommandLine(int argc, char** argv,
		     std::string& infile, std::string& outfile,
		     int& nR, int& nThreads, bool& flag_cartesian,
		     bool& flag_details)
{


  // -- done --
  return;
}
 
