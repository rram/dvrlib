
/** \file dvr_utils_SimpleMesh.cpp
 * \brief Implementation of the class SimpleMesh.
 * \author Ramsharan Rangarajan
 * \date November 30, 2020.
 */

#include <dvr_utils_SimpleMesh.h>
#include <dvr_assert.h>
#include <algorithm>
#include <filesystem>
#include <numeric>

namespace dvr
{
  namespace utils
  {
    // Construct from a tecplot file
    SimpleMesh::SimpleMesh(const std::string filename)
    {
      dvr_assert(std::string(std::filesystem::path(filename).extension())==".tec" && "Expected a .tec file");
      
      // lambda to positions cursor to get the data after 'word' occurs
      auto PositionCursor = [](std::istream &ifile, const std::string word)
	{
	  ifile.seekg(0, std::ios_base::beg);
	  while(ifile.good())
	    {
	      if(ifile.get() == word[0])
		{
		  unsigned int ipos=1;
		  while(ipos!= word.length())
		    if(ifile.get() == word[ipos]) ipos++;
		    else  break;
		  if(ipos == word.length())
		    return true;
		}
	    }
	  return false;
	};

      // open file
      std::fstream TecFile;
      TecFile.open(filename, std::ios::in);
      dvr_assert((TecFile.good() && TecFile.is_open()) && "Could not open file");

      // Read  number of nodes:
      bool flag;
      flag = PositionCursor(TecFile, std::string("N="));
      dvr_assert( flag && "Could not read number of nodes");
      TecFile >> nodes;
  
      // Read number of elements:
      flag = PositionCursor(TecFile, std::string("E="));
      dvr_assert( flag && "Could not read number of elements");
      TecFile >> elements;
  
      // Read Element type
      std::string ET;
      flag = PositionCursor(TecFile, std::string("ET="));
      dvr_assert( flag && "Could not read element type");
      TecFile>>ET;
      std::transform(ET.begin(), ET.end(), ET.begin(), ::toupper);
      dvr_assert(ET=="TRIANGLE" || ET=="TETRAHEDRON");
      if(ET=="TRIANGLE")
	{
	  spd = 2;
	  nodes_element = 3;
	}
      else
	{
	  spd = 3;
	  nodes_element = 4;
	}

      // Resize coordinates and connectivity vectors
      Coordinates.clear();
      Connectivity.clear();
      Coordinates.resize(nodes*spd);
      Connectivity.resize(elements*nodes_element);

      // Read coordinates
      for(int a=0; a<nodes; ++a)
	for(int k=0; k<spd; ++k)
	  TecFile >> Coordinates[spd*a+k];

      // Read connectivity
      for(int e=0; e<nodes_element*elements; ++e)
	{
	  TecFile >> Connectivity[e];
	  --Connectivity[e];
	} 
    
      // done
      TecFile.close();
    }


    // Identify all nodes lying on the boundary of a mesh
    std::vector<int> SimpleMesh::boundary_nodes() const
    {
      // Enumerate faces by sorted connectivities
      // If a duplicate face is found, erase it
      std::set<std::set<int>> freeFaces({});
  
      // Local numbering of faces
      const std::vector<int> locFaceNodes = (nodes_element==3) ?
	std::vector<int>{0,1, 1,2, 2,0} :             // Face numbering for triangles
      std::vector<int>{2,1,0, 2,0,3, 2,3,1, 0,1,3}; // Face numbering for tetrahedra

      // # faces per element, # nodes per face
      const int nFacesPerElm = nodes_element;
      const int nNodesPerFace = nodes_element-1;
  
      for(int e=0; e<elements; ++e)
	{
	  const int* conn = connectivity(e);
	  for(int f=0; f<nFacesPerElm; ++f)
	    {
	      // Sorted connectivity of this face
	      std::set<int> fconn({});
	      for(int a=0; a<nNodesPerFace; ++a)
		fconn.insert(conn[locFaceNodes[nNodesPerFace*f+a]]);

	      // Does it exist in the list
	      auto it = freeFaces.find(fconn);
	      if(it!=freeFaces.end())
		freeFaces.erase(it); // This is a duplicate
	      else
		freeFaces.insert(fconn); // Not a duplicate
	    }
	}
	
      // Accummulate the list of nodes along the boundary
      std::set<int> bdnodes({});
      for(auto& fconn:freeFaces)
	for(auto& n:fconn)
	  bdnodes.insert(n); 

      // done
      return std::vector<int>(bdnodes.begin(), bdnodes.end());
    }
    
    // Interior nodes
    std::vector<int> SimpleMesh::interior_nodes() const
    {
      // sorted list of boundary nodes
      auto bdnodes = boundary_nodes();

      // all nodes
      std::vector<int> allnodes(nodes);
      std::iota(allnodes.begin(), allnodes.end(), 0);

      // interior = allnodes \ bdnodes
      std::vector<int> int_nodes{};
      std::set_difference(allnodes.begin(), allnodes.end(),
			  bdnodes.begin(), bdnodes.end(),
			  std::inserter(int_nodes, int_nodes.begin()));

      // done
      return std::move(int_nodes);
    }


    // Save a mesh in tecplot format
    void SimpleMesh::save(const std::string filename) const
    {
      // Check extension
      dvr_assert(std::string(std::filesystem::path(filename).extension())==".tec" && "Expected filename with .tec extension");
      
      // Stream to write in
      std::fstream outfile;
      outfile.open(filename, std::ios::out);
      dvr_assert(outfile.good() && "Could not access write stream");
      outfile.seekp(0, std::ios_base::beg);
      outfile.precision(16);
      outfile.setf( std::ios::scientific );
  
      // Line 1:
      if( spd==2 )
	outfile<<"VARIABLES = \"X\", \"Y\" ";
      else 
	outfile<<"VARIABLES = \"X\", \"Y\", \"Z\" ";
      outfile << std::endl;

      // Line 2:
      const std::string ElmType = (spd==2)? "TRIANGLE" : "TETRAHEDRON";
      outfile << "ZONE t=\"t:0\", N="<<nodes
	      << ", E=" << elements
	     << ", F=FEPOINT, ET=" << ElmType;
  
      // Nodal coordinates
      for(int a=0; a<nodes; ++a)
	{
	  const auto* X = coordinates(a);
	  outfile << std::endl;
	  for(int k=0; k<spd; ++k)
	    outfile <<X[k] << " ";
	}
  
      // Element connectivity
      for(int e=0; e<elements; ++e)
	{
	  outfile << std::endl;
	  const auto* conn = connectivity(e);
	  for(int a=0; a<nodes_element; ++a)
	    outfile << conn[a]+1 << " ";
	}
      outfile.flush();
      outfile.close();
      
      // done
      return;
    }

    
  } // dvr::utils::
} // dvr::
