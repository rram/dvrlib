
/** \file dvr_utils_SimpleMesh.h
 * \brief Definition of the class dvr::utils::SimpleMesh.
 * \author Ramsharan Rangarajan
 * \date January 22, 2021.
 */

#pragma once

#include <vector>
#include <string>
#include <set>
#include <cassert>
#include <fstream>

namespace dvr
{
  //! Namespace for optional utilities in dvr
  namespace utils
  {
    /** \brief Class implementing a simple mesh data structure.
     * The class provides the following methods which are assumed in the dvr library:
     * \code{.cpp}
     * 
     * // Method providing access to nodal coordinates
     *  const double* coordinates(const int node_index) const; 
     *
     * // Method to update nodal coordinates
     * void update(const int node_index, const double* X); 
     *
     *  // Method providing access to element connectivities
     *  const int* connectivity(const int elm_index) const; 
     * 
     *  // Method to return the spatial dimension in which the mesh is embedded
     *  int spatial_dimension() const; 
     *
     *  // Method that returns the number of nodes
     * int n_nodes() const;
     *
     *  // Method that returns the number of elements
     * int n_elements() const;
     *
     * \endcode
     *
     * The class is very rudimentary:
     * <ul>
     * <li> Nodes are numbered sequentially, starting from 0 to SimpleMesh::nodes-1. </li>
     * <li> Elements are numbered sequentially, starting from 0 to SimpleMesh::elements-1. </li>
     * <li> Nodal coordinates are stored sequentially in the public vector SimpleMesh::Coordinates.
     *  Coordinates of the node <tt>n</tt> is located at SimpleMesh::Coordinates[spd*<tt>n</tt>],
     * where spd equals the embedding dimension SimpleMesh::spd. </li>
     * <li> Element connectivities are stored sequentially in the public vector SimpleMesh::Connectivity.
     * Connectivity of the element <tt>e</tt> is located at SimpleMesh::Connectivity[nodes_element*e],
     * where SimpleMesh::nodes_element equals 3 for a planar triangle and 4 for a tetrahdron. </li>
     * <li> The class should be used only with planar triangle and three-dimensional tetrahedral meshes. </li>
     * </ul>
     *
     * In addition to the methods required in the library (mentioned above), the class
     * provides a additional utilities:
     * <ul>
     * <li> To read a mesh from a tecplot file, through the SimpleMesh::ReadTecplotFile method. </li>
     * <li> To print a mesh in tecplot file, through the SimpleMesh::PlotTecFile method. </li>
     * <li> To identify nodes lying on the boundary of the mesh, through the SimpleMesh::GetBoundaryNodes method. </li>
     * </ul>
     *
     * To calculation of vertex 1-rings (vertices and elements) requires invoking the SimpleMesh::SetRelaxedVertices.
     * This is required before invoking the methods SimpleMesh::Get1RingVertices and SimpleMesh::Get1RingElements.
     *
     * This class is provided solely for the purpose of testing and examples.
     * <br>
     * It is neither carefully designed nor particularly efficient.
     * <br>
     * The class can be easily abused-- for example, it is possible to
     * add/delete nodes/elements through public access to the list of nodal coordinates and element connectivities,
     * which will invalidate all 1-ring vertex/element lists.
     * While useful for prototyping, SimpleMesh is not expected to be an integral part of production codes.
     *
     */
    class SimpleMesh
    {
    public:
      //! Constructor
      inline SimpleMesh(const std::vector<double>& coord,
			const std::vector<int>& conn,
			const int SPD)
	:Coordinates(coord),
	Connectivity(conn),
	spd(SPD),
	nodes_element(spd+1),
	nodes(static_cast<int>(Coordinates.size())/spd),
	elements(static_cast<int>(Connectivity.size())/nodes_element) {}

      //! Construct from a tecplot file
      SimpleMesh(const std::string filename);
      
      //! Destructor
      inline virtual ~SimpleMesh() {}

      //! Disable the copy and assignment
      SimpleMesh(const SimpleMesh&) = delete;
      SimpleMesh& operator=(const SimpleMesh&) = delete;
  
      //! Returns the coordinates of a specified node
      //! \param[in] node Node number
      //! \return Coordinates of the requested node
      //! \remark Required by dvr
      inline const double* coordinates(const int node) const 
      { return &Coordinates[spd*node]; }

      //! Overwrites nodal coordinates
      //! Hence node number should equal the current number of nodes.
      //! \param[in] node Node number
      //! \param[in] X Coordinates
      //! \remark Required by dvr
      inline void update(const int node, const double* X) 
      {
	assert(node<nodes);
	for(int k=0; k<spd; ++k)
	  Coordinates[spd*node+k] = X[k];
      }
  
      //! Returns the connectivity of a specified element
      //! \param[in] elm Element number
      //! \return Connectivity of the requested element.
      //! \remark Required by dvr
      inline const int* connectivity(const int elm) const 
      { return &Connectivity[nodes_element*elm]; }

      //! Returns the spatial dimension
      //! \return spatial dimension
      //! \remark Required by dvr
      inline int spatial_dimension() const
      { return spd; }
      
      //! Returns the number of elements
      //! \return Total number of elements in the mesh
      //! \remark Required by dvr
      inline int n_elements() const 
      { return elements; }

      //! Returns the number of nodes
      //! \return Number of nodes in the mesh
      //! \remark Required by dvr
      inline int n_nodes() const
      { return nodes; }

      //! \brief Save as a tecplot file
      //! \param[in] filename Filename under which to plot
      void save(const std::string filename) const;

      //! \brief Method to identify all nodes lying on the boundary of a mesh
      //! \return Set of boundary nodes (by value)
      std::vector<int> boundary_nodes() const;

      //! Interior nodes
      std::vector<int> interior_nodes() const;

    private:
      // Members
      std::vector<double> Coordinates; //!< Vector of coordinates
      std::vector<int> Connectivity;   //!< Vector of connectivity
      int spd;                         //!< Spatial dimension
      int nodes_element;               //!< Number of nodes per element
      int nodes;                       //!< Number of nodes in the mesh
      int elements;                    //!< Number of elements in the mesh
    };
  }
}

