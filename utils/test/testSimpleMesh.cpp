
/** \file testSimpleMesh.cpp
 * \brief Unit test for the class dvr::test::SimpleMesh.
 * \author Ramsharan Rangarajan
 *
 * Inputs: None
 *
 * Outputs: None if successful. Quits if any assertion checking correctness fails.
 */

#include <dvr_MeshTraits.h>
#include <dvr_utils_SimpleMesh.h>


int main()
{
  dvr::CheckMeshTraits<dvr::utils::SimpleMesh>();
}
