# DVRlib

![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/rram/dvrlib/joss) 
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/rram/dvrlib.svg)](https://Bitbucket.org/rram/dvrlib/issues/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![DOI](https://joss.theoj.org/papers/10.21105/joss.02372/status.svg)](https://doi.org/10.21105/joss.02372)


DVRlib is a header-only C++ library providing an implementation of the directional vertex relaxation (DVR) algorithm to improve qualities of triangle and tetrahedral meshes.

## Features
* DVRlib achieves mesh improvement by optimally and iteratively perturbing locations of a specified set of vertices along prescribed directions.
* Element connectivities are retained unaltered during mesh improvement. In particular, the library does not rely on any topology-altering operations such as edge/face swapping and vertex insertion/removal.
* Despite relying solely on geometric mesh optimization, DVRlib can achieve significant improvement in mesh quality.
* The DVR algorithm implemented by DVRlib is provably robust. The mesh quality, defined in a specific sense, is guaranteed to improve monotonically with each vertex perturbation at each iteration.
* DVRlib provides vertex-level and mesh-level optimization routines.
* DVRlib provides a lock-free thread-parallel mesh optimization routine.
* DVRlib provides functionalities to relax vertices constrained to lie on curvilinear manifolds.
* DVRlib provides flexibility in the choice of mesh data structures. All classes and functionalities requiring access to meshes are templated by the mesh type.

## Dependencies
* GNU GSL library
	- [Installation instructions](https://www.gnu.org/software/gsl/).
	- On Ubuntu 18.04: `sudo apt-get install  libgsl-dev`.
	- On macOS using [MacPorts](https://ports.macports.org/port/gsl/summary): `sudo port install gsl`. 
	- *Trouble including/linking GNU GSL?* 
         A toy example is discussed [here](docs/help-gnu-gsl.md) and
         is included as `test/dependencies/test_gsl.cpp`, which is built and run as one of the unit tests.
* C++ compiler with support for C++11 features and OpenMP.
	- GNU's `libgomp` library is [here](https://gcc.gnu.org/onlinedocs/libgomp/). 
	  g++-mp-5 and compilers released later support OpenMP 4.0 using the `-fopenmp` flag.
	- To use OpenMP with Apple's `clang++`, you may need to install `libomp` 
         ([homebrew](https://formulae.brew.sh/formula/libomp), 
	   [macports](https://ports.macports.org/port/libomp/summary)).
	- *Trouble using OpenMP?* A toy example is discussed [here](docs/help-openmp.md)
         to test compiler compatibility and compiler flags. 
         The example is included as `test/dependencies/test_openmp.cpp`, and is built and run as one of the unit tests.

* [**CMake**](https://cmake.org)
* Optional: [**Paraview**](https://www.paraview.org) for previewing mesh files in Tecplot format. 


## Installation 
* Install DVRlib and run unit tests
```
    mkdir build                    # create a build directory in a convenient location  
    cd build  
    cmake path-to-dvrlib/ .        # generate makefiles, package DVRlib
    make                           # optional, compiles unit tests
    ctest                          # or “make test”. optional, executes unit tests
    make install                   # copy headers to include/DVRlib, cmake files to share/DVRlib
```    
* Note that `make` just compiles unit tests, nothing else. Use ` make -j N` for quicker compilation.
  It is recommended that you compile and execute the unit tests before `make install`.
* `make install` installs DVRlib at a default location. To specify a custom location, use:
```
    cmake path-to-dvrlib/ -DCMAKE_INSTALL_PREFIX=custom-path
``` 
* To specify the C++ compiler for the unit tests, use:
```
    cmake path-to-dvrlib/ -DCMAKE_CXX_COMPILER=your-c++-compiler
```

#### Using DVRlib in a project
* Install DVRlib
* In the `CMakeLists.txt` file for your project, find DVRlib. This automatically
  adds the GSL package as a dependency. Hence `find_package(GSL)` is not required.
```
	find_package(DVRlib REQUIRED) 
```  
* Find OpenMP. 
```
	find_package(OpenMP)
	if(NOT OpenMP_CXX_FOUND)
 		message(FATAL_ERROR
		        "Could not locate OpenMP. Try adding -DCMAKE_CXX_FLAGS=-fopemp")
```
* Link DVRlib, GSL and OpenMP to the target:
```
	target_link_libraries(TARGET PUBLIC
                             DVRlib::DVRlib
                             GSL::gsl  GSL::gslcblas
                             OpenMP::OpenMP_CXX)
```
* Add compile features to the target:
```
	target_compile_features(TARGET PUBLIC
			      		   cxx_auto_type
			      		   cxx_constexpr
			      		   cxx_decltype
			      		   cxx_lambdas
			      		   cxx_nullptr)
```
* `tutorial/CMakeLists.txt` provides an example.

#### Tutorials
* Install DVRlib
* Build the examples in `tutorial/`
```
	mkdir build         # build in a convenient location
	cd build
	cmake path-to-DVRlib/tutorial 
	make
```
* To specify the C++ compiler:
``` 
	cmake path-to-DVRlib/tutorial  -DCMAKE_CXX_COMPILER=c++-compiler
```
* If you installed DVRlib at a custom location, tell CMake where to find it:
``` 
	cmake path-to-DVRlib/tutorial . -DCMAKE_PREFIX_PATH=custom-path
```
* Run any of the five examples:
```
	./dvr-triangle
```
which saves the output to the corresponding folder `output-dvr-triangle` within the build directory.  
* See the [**documentation pages**](https://rram.bitbucket.io/dvr-docs/html/index.html) 
for a detailed discussion of the tutorial examples.

## Using DVRlib without CMake
* DVRlib is a header-only library. It can be include directly in your project.
* Identify the paths for DVRlib and GSL:
```
	export DVRLIB_DIR=path-to-DVRlib    # Root directory for DVRlib. e.g.: /Users/name/CodeRepos/dvrlib/
	export GSLPATH=path-to-GSL          # ${GSLPATH}/include/gsl/ contains headers, ${GSLPATH}/lib contains libraries. e.g.: /usr/local
```
* Then:
	- Append include paths `-I${DVRLIB_DIR} -I${GSLPATH}/include`
	- link to the GSL libraries `-L${GSLPATH}/lib/ -lgsl -lgslcblas`	   
	- enable OpenMP with `-fopenmp`
* For example: Compile `dvr-triangle.cpp` in `${DVRLIB_DIR}/tutorial/` this way using:
```
	CXX -std=c++11 ${DVRLIB_DIR}/tutorial/dvr-triangle.cpp -o dvr-triangle -I${DVRLIB_DIR}/ -I${GSLPATH}/include -L${GSLPATH}/lib -lgsl -lgslcblas -fopenmp
```
* The unit tests in `test/` can also be compiled this.

## Tests and benchmarks
* Detailed documentation for the library with discussions of tutorial-style examples is [**here**](https://rram.bitbucket.io/dvr-docs/html/index.html).
* A repository of meshes to test the library and reproduce benchmark results is [**here**](https://rram.bitbucket.io/dvr-meshes/index.html).  


## Intended applications
* DVRlib is well-suited for integration with unstructured mesh generators to improve element qualities through post-processing operations.
* DVRlib is designed to help simulate moving boundary problems, which require computing numerical solutions over domains that evolve with time (e.g., fluid-structure interaction) or domains that change with each iteration (e.g., shape optimization).
![Improving triangle and tetrahedral meshes with DVRlib](/paper/readme-fig.jpg)

## Projects using DVRlib
* Simulating mechanical contact as a shape optimization problem: *A Sharma, R Rangarajan. A shape optimization approach for simulating contact of elastic membranes with rigid obstacles.
International Journal for Numerical Methods in Engineering 117 (4), 371-404, 2019.*

* Meshing evolving 3D domains immersed in nonconforming tetrahedral meshes: *R Rangarajan, H Kabaria, A Lew. An algorithm for triangulating smooth three‐dimensional domains immersed in universal meshes. International Journal for Numerical Methods in Engineering 117 (1), 84-117, 2019.*

## More about the algorithms behind DVRlib
* For a detailed description and analysis of the DVR algorithm, see *Rangarajan R and Lew A. Provably robust directional vertex relaxation for geometric mesh optimization. SIAM Journal on Scientific Computing, 39(6):A2438–A2471, 2017* .
* For a description and analysis of the algorithm used to resolve non smooth max-min problems defining vertex perturbations in DVRlib, see *Rangarajan R. On the resolution of certain discrete univariate max–min problems. Computational Optimization and Applications, 68(1):163–192, 2017.* 

## Contributing to DVRlib
Please check [here](CONTRIBUTING.md) for details on reporting bugs.  
The page also lists a few topics to consider if you would like to contribute to the library.

## License

MIT License

Copyright (c) [2020] [Ramsharan Rangarajan]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
