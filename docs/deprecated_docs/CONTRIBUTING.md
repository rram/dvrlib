# Contributing to DVRlib

We welcome contributions to improve and add functionality to **DVRlib**.

* Please file an issue to report a bug.
* An FAQ section and guidelines for code design/structure/documentation are under development.
* If you have a question (not an issue), or have suggestions for enhancements, get in touch with us by *[email](mailto:rram@iisc.ac.in)*.
* To begin contributing to the library, please discuss the changes you wish to make by creating an issue, or get in touch with us via email. A pull request can be sent soon after.

## Suggestions for contributions

* Add new element quality metrics to the library. Some possibilities can be found in  *Knupp, P., Algebraic mesh quality metrics. SIAM J. Scientific Computing, 23.1 (2001): 193-218*. It is known that the mean ratio metric provided currently is equivalent or weakly equivalent to a few commonly used metrics (e.g., aspect ratio).  
* Add new element types to the library (e.g., quadrilaterals).  
* Test usability and performance of  **DVRlib** with commonly used mesh data structures provided in  open source libraries (e.g., *[OpenMesh](www.openmesh.org)*).   
* Test integration of **DVRlib** with popular mesh generation libraries (e.g., *[TetGen](http://wias-berlin.de/software/index.jsp?id=TetGen&lang=1)*, *[Gmsh](https://gmsh.info/)*, *[CGAL](https://doc.cgal.org/latest/Mesh_3/index.html)*).  
* Include relaxation of nodes constrained to lie on parametric curves/manifolds (e.g, splines/NURBS).
* Identify calculations that can be ported to GPUs.  
* Create applications involving moving domain problems using  **DVRlib** in conjunction  with open source codes (e.g., fluid-structure interactions, shape optimization).
* Add python wrappers for the main mesh optimization functionalities provided by the library.
