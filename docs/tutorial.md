# Tutorial examples {#tutorial}

## List of examples

Tutorial example | Purpose |
:----|:-----|
dvr-triangle.cpp | Demonstrates the use of DVRlib to relax a mesh of planar triangles |
dvr-tetrahedra.cpp | Demonstrates the use of DVRlib to relax a mesh of 3D tetrahedra | 
dvr-omp-triangle.cpp |  Demonstrates relaxing a mesh of planar triangles using a thread-parallel implementation of the dvr algorithm |
dvr-cmdline.cpp |  Demonstrates using DVRlib with command line options |
dvr-constraint.cpp | Demonstrates using DVRlib to improve a mesh of planar triangles while constraining boundary vertices

## A first example: improving a triangle mesh {#sec_eg_1}

The example dvr-triangle.cpp provided in the folder <tt>tutorial</tt> is reproduced here
for a discussion of the basic usage of the library.

\include dvr-triangle.cpp 

### Discussion
- For convenience, all header files required to  use the library
     are listed in <tt> dvr_Module</tt>, which is included in the example as
     \snippet dvr-triangle.cpp doc_include_header 
     All paths to header files in <tt> dvr_Module</tt> are relative to <tt>$DVRDIR</tt>.
- The example uses the triangle mesh <tt>superior.mesh</tt> located in the <tt>$DVRDIR/mesh</tt> folder.
     The file is in Tecplot format and can be previewed using Paraview (free) or Tecplot (licensed).
     The file is an adaptation of a mesh generated using
     <a href=https://www.cs.cmu.edu/~quake/triangle.demo.html>Triangle</a>.
     The assumption of a file in Tecplot format is done solely for definiteness and convenience.
- The triangle mesh to be improved is read using the method
     dvr::utils::SimpleMesh::ReadTecplotFile.
     \snippet dvr-triangle.cpp doc_read_mesh
     Notice that the class dvr::utils::SimpleMesh is not under the  dvr namespace, 
     which serves as a reminder that it is not a part of the library, but is provided for testing purposes.
- Next, the list of vertices to be relaxed is set in the vector <tt>Ir</tt>.
     In this example, all vertices in the interior of the mesh, i.e., not lying on the
     boundary, are relaxed. The method dvr::utils::SimpleMesh::GetBoundaryNodes is invoked
     identifies all nodes lying on the boundary. The list <tt> Ir</tt> is defined to be the
     complement of the set of all nodes in the mesh and the set of boundary nodes.
     For simplicity, vertex indices are listed in ascending order in <tt>Ir</tt>.
     \snippet dvr-triangle.cpp doc_IR
- Only the mean ratio element quality metric is supported by the library at this time.
     For planar triangles, element quality calculations are implemented by the class
     dvr::GeomTri2DQuality, which is templated by the mesh type.
     Though not relevant for this example, note that the constructor allocates memory for thread-safe calculations
     if the number of threads is specified.
     \snippet dvr-triangle.cpp doc_quality
- The max-min solver serves to compute the optimal coordinate \f$\lambda^{\rm opt}\f$ to perturb
     a vertex along a prescribed direction. All tutorial examples use the dvr::ReconstructiveMaxMinSolver class
     for unconstrained directional vertex relaxation.
     It is recommended over the other solvers in the library for the purpose of unconstrained vertex relaxation.
     \snippet dvr-triangle.cpp doc_solver
- The example relaxes nodes along Cartesian directions (the coordinate system is implicitly defined by the
     vertex coordinates) during successive iterations. Hence, all vertices in the list
     <tt>Ir</tt> are relaxed along \f${\bf e}_x\f$ during the first iteration, then along
     \f${\bf e}_y\f$ during the second iteration, and so on. The struct dvr::RelaxationDirGenerator
     is provided to facilitate specifying iteration-- and vertex--dependent relaxation directions for vertices in <tt>Ir</tt>.
     In this example, the relaxation direction generator is set using
     a derived type dvr::CartesianDirGenerator, which is templated by the spatial dimension and implements the 
     alternation between Cartesian directions.
     The iteration count is passed through a member, and is used to alternate between the cardinal directions.
     \snippet dvr-triangle.cpp doc_rdir
- The local function PrintMeshQuality prints the vector-valued mesh quality to help visualize the
     the evolution of element qualities with each relaxation iteration. The
     quality of the input mesh is printed before the start of relaxation iterations. The printed file
     has two columns: \f$i\f$ and \f${\rm Q}_i\f$ for \f$1\leq i\leq \#{\rm I}_{\rm R}\f$.
     \snippet dvr-triangle.cpp doc_mesh_quality
- Vertices in the list <tt>Ir</tt> are relaxed iteratively by invoking the
     dvr::Optimize routine. This example uses a fixed number of relaxation iterations (<tt>Nr=10</tt>). Monitoring
     the evolution of the mesh quality, and in particular, convergence of the poorest few element qualities,
     helps make a more informed choice in general.
     At the end of each iteration, the improved mesh and its quality vector are printed to file.
     \snippet dvr-triangle.cpp doc_iteration

## Improving a 3D mesh of tetrahedra{#sec_eg_2}

The file dvr-tetrahedra.cpp provided in the folder <tt>$DVRDIR/tutorial</tt> contains an example
showing how to use  DVRlib to improve the quality of a tet mesh.
The steps involved in the example are virtually identical to the
case of improving a triangle mesh discussed above.
In the following, just the main differences are highlighted.

- A mesh of tetrahedra is read from the file "P.mesh" in Tecplot format.
     The file is an adaptation of a mesh provided with 
     <a href=https://people.eecs.berkeley.edu/~jrs/stellar/webpage>Stellar</a>.
     \snippet dvr-tetrahedra.cpp doc_read_mesh
- The mean ratio metric for tetrahedra is defined by the class dvr::GeomTet3DQuality, which is
     templated by the mesh type.
     \snippet dvr-tetrahedra.cpp doc_quality
- Either one of the max-min solvers dvr::PartitionedMaxMinSolver or dvr::ReconstructiveMaxMinSolver
     can be used. For tet meshes, the latter is expected to be far more efficient.
     Both solvers compute identical sequences of meshes. 
     \snippet dvr-tetrahedra.cpp doc_solver
- In this example, vertices are relaxed along randomly generated directions. The helper struct
     dvr::RandomDirGenerator is templated by spatial dimension and implements thread-safe
     generation of random directions. Unlike dvr::CartesianDirGenerator, dvr::RandomDirGenerator
     does not require knowledge of the iteration count.
     \snippet dvr-tetrahedra.cpp doc_rdir

## Parallel mesh improvement{#sec_eg_3}

The file dvr-omp-triangle.cpp provides an example using the thread-parallel implementation
of the  dvr algorithm provided in  DVRlib. In the following, just the main distinctions from the (serial)
example in dvr-triangle.cpp are highlighted.

- Vertices in the list <tt>Ir</tt> are partitioned using a coloring algorithm:
     \snippet dvr-omp-triangle.cpp doc_coloring
- The classes for element quality metrics (dvr::GeomTri2DQuality, dvr::GeomTet3DQuality) and the classes
      for max-min solvers (dvr::PartitionedMaxMinSolver, dvr::ReconstructiveMaxMinSolver, dvr::SamplingMaxMinSolver)
     take the number of threads as a parameter (defaulted to 1) during
     construction to pre-allocate thread-safe private data structures.
     \snippet dvr-omp-triangle.cpp doc_num_threads
     \snippet dvr-omp-triangle.cpp doc_quality
     \snippet dvr-omp-triangle.cpp doc_solver
- The parallelized dvr::Optimize routine can be executed on as many (or fewer) threads as that
     specified to the quality and max-min-solver classes. The number of threads to use is specified
     prior to optimization iterations using an OpenMP routine.
     \snippet dvr-omp-triangle.cpp doc_omp_threads
- The parallelized dvr::Optimize routine additionally takes the dvr::VertexColoring object <tt>vshades</tt> 
     compared to its serial counterpart.
     \snippet dvr-omp-triangle.cpp doc_iteration
     The example times each optimization iteration using an OpenMP routine to get the wall clock time.
      DVRlib, by itself, does not provide any utilities for profiling.

## Mesh improvement from the command line {#sec_eg_4}
For convenience, the example dvr-cmdline.cpp enables using  DVRlib with command line switches.

The example assumes that: 
- the input mesh file is provided in Tecplot format
- the output file is to be printed in Tecplot format 
- All interior nodes are to be relaxed 
- Relaxation directions are randomly generated. 

The example uses the [CLI11](https://github.com/CLIUtils/CLI11) library to create and parse
the following command line options:


Switch  | Type | Purpose | Required/Optional| 
:-----------|:-----:|:---------:|:---------|
-i | string |  input tec file | required |
-o | string | output tec file | required |
-n | integer | number of relaxation iterations | required |
-j | integer | number of threads | optional, defaulted to 1 |
-c | flag | Cartesian or random relaxation direction | optional, defaulted to false |
-d | flag | write detailed output | optional, defaulted to false |

The example does not provide  new functionality compared to the ones discussed above.
For instance, 
- <tt>./dvr-cmdline -i $DVRDIR/mesh/superior.mesh -o opt-superior.tec -c -d -n 10 </tt> reproduces the example dvr-triangle.cpp. 
- <tt>./dvr-cmdline -i $DVRDIR/mesh/P.mesh -o opt-P.tec -d -n 10 </tt> reproduces the example dvr-tetrahedra.cpp 
- <tt>./dvr-cmdline -i $DVRDIR/mesh/superior.mesh -o opt-superior.tec -c -d -n 10 -j 3 </tt> reproduces the example dvr-omp-triangle.cpp. 

## Relaxing nodes constrained to lie on a curved boundary {#sec_eg_5}

It is often necessary to constrain nodes to lie on a curvilinear manifold during relaxation. This is the case, for instance, 
when relaxing nodes that lie on the boundary of a mesh over a curvilinear domain. The file dvr-constraint.cpp provides an example
of how  DVRlib can be used while incorporating such constraints. 
<br>
The example improves a triangle mesh over a circular domain centered at the origin and having radius 1/2 units.
Interior nodes are relaxed along radial and circumferential directions using a custom relaxation direction generator.
Nodes along the boundary are relaxed while constraining them to lie on a circle of radius 1/2.

\include dvr-constraint.cpp

### Discussion
- The mesh to be improved is read from the file <tt>$DVRDIR/mesh/circle-rad-0p5.mesh</tt> in Tecplot format.
     \snippet dvr-constraint.cpp doc_read_mesh
- All nodes in the mesh are relaxed. 
     The set of boundary nodes are relaxed along directions tangential to the curved boundary,
     while the set of interior nodes are relaxed alternately along radial and circumferential directions.
     \snippet dvr-constraint.cpp doc_IR
- The mean ratio metric is used for element qualities.
     \snippet dvr-constraint.cpp doc_quality
- The class dvr::ReconstructiveMaxMinSolver is used for unconstrained directional relaxation of interior nodes. 
     \snippet dvr-constraint.cpp doc_int_solver
- The dvr::SamplingMaxMinSolver class is used to compute perturbations for boundary nodes 
     that are constrained to lie on a circle of radius 1/2. 
     <br>
     Given vertex \f$i\f$ located at \f${\bf x}\f$ lying on the the circular boundary \f$\Gamma\f$, 
     the solver generates a uniformly spaced discrete set of sample points \f$\Lambda\f$ 
     contained in the interval \f$[-h,h]\f$, where \f$h\f$ is the local mesh size. The
     number of sample points is specified during construction, in this case, to be 20.

     The solver determines the (sub)optimal perturbation for \f$i\f$ as 
     \f[ \lambda^{\rm opt} = \arg \max_{\lambda\in \Lambda} \min\{{\rm Q}(\tilde{K}^\lambda)\,:\,K\in \stackrel{\star}{e}(i;{\cal T}) \}\f]
     where \f$\tilde{K}^\lambda\f$ is the element resulting from perturbing its vertex \f$i\f$
     from \f${\bf x}\f$ to \f$\pi({\bf x}+\lambda{\bf t}({\bf x}))\f$. 
     The relaxation direction \f${\bf t}\f$ is chosen to be tangential to \f$\Gamma\f$ at \f${\bf x}\f$
     and \f$\pi:{\mathbb R}^2\rightarrow \Gamma\f$ denotes the closest point projection onto \f$\Gamma\f$.

     Given the signed distance function to \f$\Gamma\f$, 
     the helper struct dvr::TangentialDirGenerator furnishes relaxation directions \f${\bf t}\f$,
     and dvr::ClosestPointStruct computes the mapping \f$\pi\f$.
     The instance <tt>cpt</tt> of the latter is passed to the dvr::Optimize routines, 
     which in turn forwards it to the max-min solver.
     During each vertex relaxation, the max-min solver generates a 
     sample point along the specified tangent direction \f${\bf x}\rightarrow {\bf x}+\lambda{\bf t}({\bf x})\f$.
     It then invokes the closest point projection to map
     \f${\bf x}+\lambda{\bf t}({\bf x})\f$ to \f$\pi({\bf x}+\lambda{\bf t}({\bf x}))\in \Gamma\f$, 
     thus constraining node \f$i\f$ to remain on \f$\Gamma\f$.
     In this way, the solver compares element qualities only at locations that satisfy the constraint 
     on the location of the vertex.
     \snippet dvr-constraint.cpp doc_bd

     The signed distance function to \f$\Gamma\f$ is implemented locally by the function <tt>CircleSDFunc</tt>
     as \f$\phi(x,y) =\sqrt{x^2+y^2}-0.5\f$. Except at the origin, \f$\nabla\phi={\bf e}_r\f$.  
     Consequently, \f${\bf t}({\bf x})\f$ computed by the instance <tt>tgtDirs</tt> coincides
     with \f${\bf e}_\theta\f$. The closest point projection computed by <tt>cpt</tt> equals 
     \f[ \pi({\bf x})={\bf x}-\phi({\bf x})\nabla\phi({\bf x}) = 0.5\,{\bf e}_r({\bf x}).\f]
     \snippet dvr-constraint.cpp doc_sdfunc
- Interior vertices are relaxed along radial and circumferential directions during alternate iterations.
     A custom relaxation direction generating function is defined to this end, which returns \f${\bf e}_r\f$
     and \f${\bf e}_\theta\f$ during even and odd-numbered iterations. Notice that relaxation directions defined
     this way depend on the position of the vertex. Since these directions are not well defined at the origin, 
     a vertex located at the center of the circular domain is relaxed along \f${\bf e}_x\f$
     and \f${\bf e}_y\f$ during even and odd iterations.
     \snippet dvr-constraint.cpp doc_polar
     The relaxation direction generator for interior vertices is then defined by specifying the 
     generating function to point to <tt>PolarDirGenerator</tt>. The iteration counter is passed as a 
     parameter.
     \snippet dvr-constraint.cpp doc_polar_dir
- During each relaxation iteration, the locations of all interior vertices are optimized,
     followed by the locations of boundary vertices. The choice of this ordering is arbitrary.
     \snippet dvr-constraint.cpp doc_iteration

This example highlights the simplicity and flexibility that  DVRlib affords in
the choice of relaxation directions and max-min solvers for relaxing specific vertex sets in a mesh.
For the particular mesh used in this example, relaxing just the interior vertices yields very little improvement
(try it by commenting the line `dvr::Optimize(bdNodes, MD, bdSolver, tgtDirs, &cpt)`) .
By including relaxations of boundary nodes, the mesh quality improves substantially.
