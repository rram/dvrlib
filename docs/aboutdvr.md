
# The DVR Algorithm {#aboutdvr}

### Main idea: optimizing the location of a vertex

 \image html imgs/dvr-img1.png "Figure 3" width=750px

The images in Figure 3 illustrate the main idea behind dvr using an example of a triangle mesh.
For simplicity, consider relaxing just the vertex \f$v\f$, and choose the relaxation direction
to be along the vertical \f$({\bf e}_y)\f$ and along the horizontal \f$({\bf e}_x)\f$ directions
during odd and even numbered iterations, respectively.
Assume that the element quality indicator \f$({\rm Q})\f$
assigns higher qualities to more regularly-shaped triangles.
During the first iteration, the dvr algorithm inspects the qualities of the four triangles \f$K_1,K_2,K_3\f$ and \f$K_4\f$
incident at the vertex \f$v\f$. It
computes the coordinate \f$\lambda^{\rm opt}\f$ such that relocating \f$v\f$ to \f$v'=v+\lambda^{\rm opt}\,{\bf e}_y\f$ maximizes the minimum among the qualities of the resulting
four triangles \f$K_1',K_2',K_3'\f$ and \f$K_4'\f$.
The shape of triangle \f$K_4\f$ is visibly improved as a result. During the second iteration, dvr computes \f$\lambda^{\rm opt}\f$ such that
relocating \f$v'\f$ to \f$v'' = v+\lambda^{\rm opt}\,{\bf e}_x\f$ maximizes the poorest quality among the resulting triangles \f$K_1'',K_2'',K_3''\f$ and \f$K_4''\f$. In principle, this procedure
can be repeated indefinitely; in the example shown, no improvement is possible beyond two iterations.

The dvr algorithm is a generalization of the procedure just described--- given a mesh,
an ordered subset of its vertices that can be relaxed,
and the directions along
which these vertices can be relaxed, each dvr iteration consists in repeating the procedure illustrated in Figure 3, one vertex at a time.

### Algorithm inputs{#sec_algo_inputs}

- A triangulation \f${\cal T}\f$ to be improved.
  <br>
  For the ensuing discussions, it is useful to
  introduce some notation. The triangulation \f${\cal T}\f$ in \f${\mathbb R}^{\rm d}\f$ for \f${\rm d}=2\f$ or \f$3\f$
  is understood to be a collection of \f${\rm d}\f$-simplices and is identified by:
  - An index set \f${\rm I}\f$ for its \f${\rm N}\f$ vertices, 
  - A corresponding multiset \f${\rm V}\f$ in \f$[{\mathbb R}^{\rm d}]^{\rm N}\f$ for their nodal coordinates, and 
  - A set of connectivities \f${\rm C}\f$ consisting of \f$({\rm d}+1)\f$--tuples of indices in \f${\rm I}\f$. 

  Hence, \f${\cal T}\f$ is defined as the 3-tuple \f${\cal T}=({\rm V},{\rm I},{\rm C})\f$. The case \f${\rm d}=2\f$ corresponds 
  to a mesh of planar triangles and the case \f${\rm d}=3\f$ corresponds to a mesh of tetrahedra.
  - The position of a vertex with index \f$i\in {\rm I}\f$ in \f${\mathbb R}^{\rm d}\f$ is denoted by
       \f${\rm V}_i\in {\rm V}\f$ and a simplex \f$K\f$ in \f${\cal T}\f$ by \f$K\in {\cal T}\f$.
  - For \f$i\in {\rm I}\f$, \f$\stackrel{\star}{e}(i;{\cal T})\f$ denotes the set of simplices in \f${\cal T}\f$ sharing the vertex \f$i\f$.
       For example, in figure 4a, \f$\stackrel{\star}{e}(1;{\cal T}) = \{K_1,K_2,K_3,K_4\}\f$. It is convenient to refer to
       \f$\stackrel{\star}{e}(i;{\cal T})\f$ as the elements in the 1-ring of vertex \f$i\f$. 
  - Vertices in the 1-ring of \f$i\f$ refers to the collection of vertices of the simplices in
        \f$\stackrel{\star}{e}(i; {\cal T})\f$ while omitting \f$i\f$.
       For example, in figure 4a, the set of vertices in the 1-ring of  vertex 1 is \f$\{2,3,4,5\}\f$.

- \f${\rm I}_{\rm R}\subseteq {\rm I}\f$ is a user-specified list of vertices which can be relaxed.
     Its choice depends on the purpose of mesh relaxation.
     In moving boundary problems, for example, it usually suffices to relax just the vertices in the vicinity of the moving boundary.

- \f${\rm N}_{\rm R}\f$ is the user-specified number of relaxation iterations.
     In practice, monitoring convergence of the poorest few element qualities suggests useful criteria for termination.

- The perturbation direction for each vertex in \f${\rm I}_{\rm R}\f$ at each iteration of the algorithm.
     The relaxation direction prescribed for the vertex \f$i\in {\rm I}_{\rm R}\f$ at the \f$k^{\rm th}\f$
     iteration is denoted by \f${\bf d}_{k,i}\f$. Choices for these directions
     plays a critical role in the quality of the computed mesh.
     While guarantees for mesh improvement are independent of the choices for these directions,
     the magnitude of improvement in mesh quality generally does.

### The Algorithm{#sec_algo_2}

\image html imgs/dvr-img3.png "Figure 4" width=750px

DVR is an iterative algorithm in which:
- The connectivity \f${\rm C}\f$ of the given triangulation \f${\cal T}\f$ is maintained.
- Locations of the given ordered subset \f${\rm I}_{\rm R}\subseteq {\rm I}\f$ are altered along prescribed directions,
     while the remaining vertices in \f${\rm I}\setminus {\rm I}_{\rm R}\f$ are held fixed.

The algorithmic description provided in Figure 5 uses the shorthand \f${\cal T}^{i,\lambda}\f$ for the mesh resulting from
perturbing vertex \f$i\in {\rm I}_{\rm R}\f$ in \f${\cal T}\f$ along the prescribed direction by the coordinate \f$\lambda\f$, and
uses the shorthand 
\f[ q_{i}(\lambda,{\cal T}) = \min\{{\rm Q}(K)\,:\, K\in \stackrel{\star}{e}(i;{\cal T}^{i,\lambda})\} \text{ for } \lambda\in {\mathbb R}~\text{and}~i\in {\rm I}_{\rm R}\f]
to refer to the minimum among the qualities of simplices in the 1-ring of the vertex \f$i\f$ in the mesh \f${\cal T}^{i,\lambda}\f$. The
element quality metric, a scalar-valued function defined over the set of simplices in \f${\mathbb R}^{\rm d}\f$, is denoted by \f${\rm Q}\f$
and is assumed to assign larger values to better-shaped simplices.

Figure 5 summarizes the steps involved in dvr assuming \f${\rm Q}\f$ to be the mean ratio metric,
which is adopted for measuring triangle and tetrahedron quality in this library,
The choice of the mean ratio metric ensures that the function
\f$\lambda\mapsto q_{i}(\lambda,{\cal T})\f$ has a unique maximizer (see Proposition 4.9 in \cite rangarajan2017provably).
A  general version of the dvr algorithm, that is
suitable for a broader class of quality metrics for which
 \f$\lambda\mapsto q_{i}(\lambda,{\cal T})\f$ may have multiple maximizers, can be found in \cite rangarajan2017provably.

\image html imgs/algo.png "Figure 5." width=650px

The crux of the algorithm  lies in identifying the maximizer \f$\lambda^{\rm opt}\f$ in Step 4.
It is instructive to examine the main challenge in computing \f$\lambda^{\rm opt}\f$ using an example, say
the one in Figure 4.


In the figure, perturbing vertex 1
along the prescribed direction by the coordinate \f$\lambda\f$ transforms the triangles
\f$K_1,K_2,K_3\f$ and \f$K_4\f$ into triangles \f$K_1^\lambda,K_2^\lambda,K_3^\lambda\f$
and \f$K_4^\lambda\f$, respectively. Since \f${\rm Q}\f$ is a smooth function of vertex coordinates of a triangle,
\f$\lambda\mapsto {\rm Q}(K_\alpha^\lambda)\f$ is a smooth map for each \f$\alpha=1,\ldots,4\f$.
As the minimum among these smooth maps,
\f[ q_{1}(\lambda,{\cal T}) = \min\{{\rm Q}(K_1^\lambda),Q(K_2^\lambda),Q(K_3^\lambda),Q(K_4^\lambda)\}\f]
is expected to be continuous, but not differentiable with respect to \f$\lambda\f$.
Consequently, \f$\lambda^{\rm opt}\f$ cannot be identified by straightforward derivative-based methods.

\image html imgs/dvr-img6.png "Figure 6" width=700px 

Figure 6 shows another example in which a vertex \f$v\f$ is relaxed along the \f$45^\circ\f$ direction \f$({\bf d}=(1/\sqrt{2},1/\sqrt{2})\f$.
The 1-ring of \f$v\f$ contains six elements \f$K_1,\ldots,K_6\f$.
The plot in the figure shows the qualities of the triangles \f$K_1^\lambda,\ldots,K_6^\lambda\f$ that are
realized when \f$v\f$ is perturbed to \f$v+\lambda{\bf d}\f$, with  \f$f_\alpha(\lambda) = {\rm Q}(K_\alpha^\lambda)\f$ for
\f$\alpha=1,\ldots,6\f$. The minimum among these six functions, i.e.,
\f[ F(\lambda) = \min\{f_1(\lambda),\ldots,f_6(\lambda)\} \f]
is shown in black. Notice that \f$\lambda\mapsto F(\lambda)\f$ is continuous, but not differentiable everywhere.
The main calculation in the dvr algorithm consists in identifying \f$\lambda^{\rm opt}\f$ as the
maximizer of the non differentiable function \f$F\f$.

Indeed, the main purposes of this library is to provide efficient and robust ways of
computing the optimal perturbation coordinate \f$\lambda^{\rm opt}\f$
by exploiting a detailed understanding of non smooth functions of the form
\f$\lambda\mapsto q_{i}(\lambda,{\cal T})\f$. In these calculations,
it is also important to account for the fact that the calculation of \f$\lambda^{\rm opt}\f$ in Step 4 is likely to be
an approximation of the correct one.

### When does dvr work{#sec_algo_3}

Theorem 4.2 in \cite rangarajan2017provably identifies sufficient conditions for dvr to succeed, i.e.,
for the algorithm to be well defined and to guarantee improvement in mesh quality. The key assumption concerns the input mesh \f${\cal T}\f$
whose vertices are to be relaxed. Given a list \f${\rm I}_{\rm R}\f$ of vertices in \f${\cal T}\f$ to be relaxed,
it is assumed that
\f[ Q(K)>0 ~ \forall\,K\in \cup_{i\in {\rm I}_{\rm R}} \stackrel{\star}{e}(i;{\cal T}).\f]
The assumption is automatically satisfied if every element in the input mesh \f${\cal T}\f$ has strictly positive quality.

The assumption is not an academic one-- optimal perturbations required by the algorithm and computed
by the library 
may not even be well defined if \f${\rm Q}(K)\leq 0\f$ for some \f$K\f$ in the 1-ring of a vertex in \f${\rm I}_{\rm R}\f$.
If the assumption is violated, it is possible that \f$\lambda^{\rm opt}=\pm \infty\f$.
For this reason, a check for positivity of element qualities is explicitly incorporated in the implementation of the algorithm.

In particular, this assumption implies that dvr 
- \b cannot be used with meshes that contain degenerate triangles/tets (i.e., elements with zero areas or volumes). 
- \b cannot be used with (tangled) meshes that contain overlapping elements (i.e., elements with negative qualities). 

DVR is therefore a useful tool for mesh improvement, not for mesh untangling. 

The assumption of positive element qualities is sufficient, but not necessary.
It is therefore possible that the the algorithm's implementation provided by the library
executes successfully and returns an improved
mesh when given a triangulation with degenerate or tangled elements.	  
