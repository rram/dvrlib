# Unit tests {#unittests}

Here is a list of unit tests for classes provided in the library.

All tests run silently with `ctest -j`, and quit through assertions if an incorrect calculation is encountered.

These tests should be interpreted as checking necessary conditions for correctness of functionalities.

Unit test  | Purpose |
:-----------|:-----|
testSimpleMesh.cpp | Verifies that the class dvr::utils::SimpleMesh satisfies the required traits. |
testDesCartes.cpp | Tests for the class dvr::DesCartesPolyRootCounter |
testGNUPolySolver.cpp |  Tests for the class dvr::GNUPolySolver |
testGeomTri2DQuality.cpp | Tests for the class dvr::GeomTri2DQuality |
testGeomTet3DQuality.cpp | Tests for the class dvr::GeomTet3DQuality |
testTri_MaxMinSolverCompare.cpp | Test checking equality of perturbations computed by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver for a triangle mesh |
testTet_MaxMinSolverCompare.cpp | Test checking equality of perturbations computed by the classes dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver for a tetrahedral mesh |
testTri_VertexOptimize.cpp | Tests the dvr::Optimize routine for vertex-wise optimization in a planar triangle mesh |
testTri_SeqMeshOptimize.cpp | Tests the dvr::Optimize routine for sequentially relaxing a set of vertices in a planar triangle mesh |
testTet_SeqMeshOptimize.cpp | Tests the dvr::Optimize routine for sequentially relaxing a set of vertices in a tetrahedral mesh |
testTri_ompMeshOptimize.cpp | Tests the dvr::Optimize routine for thread-parallel relaxation of a set of vertices in a planar triangle mesh |
testTri_VertexColoring.cpp | Tests consistency (correctness) of the thread-wise coloring of vertices computed by the dvr::VertexColoring class using a planar triangle mesh


