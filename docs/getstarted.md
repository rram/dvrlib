# Get Started{#getstarted}

Install the dependencies:
- C++ compiler with support for support for C++11 features.

- [CMake](https://cmake.org)(Version 3.19 or later)

- [GNU GSL](https://www.gnu.org/software/gsl/)(Mac: `brew install gsl`, Ubuntu: `apt install libgsl-dev`)
	- *Having trouble with GNU GSL?*  Use \ref test_gsl.cpp for testing. The example is built and run during compilation

- [Boost](https://www.boost.org) (Mac: `brew install boost`, Ubuntu: `apt install libboost-all-dev`)


- **Optional**
  - [Paraview](https://www.paraview.org) for previewing mesh files in Tecplot format. 
  - OpenMP:
	- With GNU compilers, use [libgomp](https://gcc.gnu.org/onlinedocs/libgomp/) along with the `-fopenmp` flag
	- With `clang++` on a Mac, use  [libomp](https://formulae.brew.sh/formula/libomp)(`brew install libomp`)
	- *Having trouble with OpenMP?* Use \ref test_openmp.cpp for testing. The example is built
         and run during compilation.
		 

Clone the repository:
```sh
git clone https://rram@bitbucket.org/rram/DVRlib.git
```


Configure and build:
```sh
cd DVRlib && mkdir build && cd build && cmake .. && make -j
```

Install (to a default location):
```sh
sudo make install 
```

### Build options

Specify a custom installation path:
```sh
cmake ../ -DCMAKE_INSTALL_PREFIX=destination-folder
```

Build without unit tests (default=ON):
```sh
cmake ../ -DBUILD_TESTS=OFF
```


Run unit tests:
```sh
ctest -j
```

Build documentation (default=OFF, requires  [Doxygen](https://doxygen.nl)):
```sh
cmake ../ -DBUILD_DOCS=ON
make docs
```


### Build tutorial examples
A set of five tutorial examples discussed in the @ref tutorial page are provided
in the `tutorial/` folder of the repository. The tutorial is built as
an independent project.

Build the tutorial examples:
```sh
cd tutorial && mkdir build && cmake ../ 
make -j
```

Run any of the five examples. For instance:
```sh
./dvr-triangle
```
which saves the output to the corresponding folder `output-dvr-triangle` within the build directory. 


### Linking DVRlib to a project

Find the DVRlib package:
```
# add to CMakeLists.txt
find_package(DVRlib REQUIRED)
```
- if DVRlib is installed in a custom location, use the `CMAKE_MODULE_PATH` flag to specify the path
- `find_package(DVRlib)` will automatically look for its dependencies (GSL, OpenMP)

Result variables:  `DVRlib_INCLUDE_DIRS, DVRlib_LIBRARY_DIRS, DVRlib_LIBRARIES`
	
Link DVRlib to a target:
```
# add to CMakeLists.txt
target_include_directories(TARGET PUBLIC ${DVRlib_INCLUDE_DIRS})
target_link_directories(TARGET PUBLIC ${DVRlib_LIBRARY_DIRS})
target_link_libraries(TARGET PUBLIC ${DVRlib_LIBRARIES})
```

## Tests and benchmarks
- Detailed documentation for the library with discussions of tutorial-style examples is [here](https://rram.bitbucket.io/DVRlib-docs/html/index.html).
- A repository of meshes to test the library and reproduce benchmark results is [here](https://rram.bitbucket.io/DVRlib-meshes/index.html).


