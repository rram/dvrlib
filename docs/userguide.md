# User guide {#userguide}


## Functionalities provided by DVRlib {#sec_lib}

### Optimization routines {#sec_lib_1}
DVRlib provides three overloaded dvr::Optimize routines implementing the  dvr algorithm.
- `dvr::Optimize(const int vert_index, double* rdir, const MMSType& MMSolver, void* solparams)`
      computes and returns the value of the optimal perturbation \f$\lambda^{\rm opt}\f$ for relaxing a vertex with index
      <tt>vert_index</tt> along a prescribed direction <tt>rdir</tt> using the provided max-min solver <tt>MMSolver</tt>.
       The routine does \b not update the coordinates of the vertex.
- `dvr::Optimize(const std::vector<int>& Ir, MeshType& MD, const MMSType& MMSolver,  RelaxationDirGenerator& rdir_gen, void* solparams)`
      sequentially computes and updates locations of all vertices in the list <tt>Ir</tt>, along directions
      specified through the direction generator <tt>rdir_gen</tt>.
- `dvr::Optimize(const std::vector<int>& Ir, MeshType& MD, const MMSType& MMSolver,  RelaxationDirGenerator& rdir_gen, const VertexColoring<MeshType>& vshades, void* solparams)`
       provides a thread-parallel implementation of the dvr algorithm for execution on shared memory
	 architectures using openMP directives. The implementation uses a coloring of vertices (accessed through <tt>vshades</tt>), rather than locks, to avoid data races.

The three routines serve different purposes. The first one provides the most flexibility in
the choice of vertices to relax, the order in which to relax vertices, directions along which to relax vertices,
control over whether or not to update the location of a vertex, the possibility of performing intermediate operations
before updating the location of the vertex, and deciding the number of relaxation iterations.

The second and third routines are ready to use sequential and parallel implementations of dvr.
Further details are discussed in the section on \ref sec_inputs.

### Mean ratio element quality metrics{#sec_lib_2}
DVRlib provides thread-safe classes implementing the mean ratio metric for planar triangles and three-dimensional tetrahedra.
- dvr::GeomTri2DQuality implements the mean ratio metric for planar triangles. The metric is defined as
    \f[ {\rm Q}(K) = 4\sqrt{3}\,\frac{\text{Signed Area}(K)}{\ell_{12}^2+\ell_{23}^2+\ell_{13}^2}, \f]
    where \f$\ell_{pq}\f$ is the length of the edge joining the \f$p^{\rm th}\f$ and \f$q^{\rm th}\f$ vertices of \f$K\f$.
- dvr::GeomTet3DQuality implements the mean ratio metric for tetrahedra. The metric is defined as 
         \f[ {\rm Q}(K) = 72\sqrt{3}\,\frac{{\text{Signed volume}(K)}}{(\ell_{12}^2+\ell_{13}^2+\ell_{14}^2 + \ell_{23}^2 + \ell_{24}^2 + \ell_{34}^2)^{3/2}}.\f]
  
Methods in these classes facilitate computing functions of the form \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$,
their level sets \f$ \{\lambda\in {\mathbb R}\,:\,{\rm Q}(K^\lambda)=Q_0\}\f$ for a given \f$Q_0\in(0,1]\f$,
their derivatives \f$d{\rm Q}(K^\lambda)/d\lambda\f$, and pairwise intersections \f$\{\lambda\,:\,{\rm Q}(K_\alpha^\lambda)={\rm Q}(K_\beta^\lambda)\}\f$ for \f$\alpha\neq\beta\f$.
These methods are extensively used by the max-min solver classes for computing optimal vertex perturbations.

### Max-Min solvers for unconstrained directional vertex relaxation{#sec_lib_3}
DVRlib provides two thread-safe classes for resolving the unconstrained 1-dimensional max-min problem to compute optimal vertex perturbations.
- dvr::ReconstructiveMaxMinSolver computes
 	\f[\lambda^{\rm opt} = \arg\max_{\lambda\in {\mathbb R}} q_{i}(\lambda,{\cal T}^{i,\lambda})\f]
	by partially reconstructing the non-smooth function \f$\lambda\mapsto q_{i}(\lambda,{\cal T}^{i,\lambda})\f$. The algorithm
      underlying the solver is introduced and analyzed in \cite rangarajan2017resolution.
- dvr::PartitionedMaxMinSolver computes \f$\lambda^{\rm opt}\f$ 
	by explicitly computing all extrema and pairwise intersections of element quality curves
	of the form \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$.

In general, dvr::ReconstructiveMaxMinSolver is the recommended solver to use for unconstrained directional vertex relaxation.
<br>
Both dvr::ReconstructiveMaxMinSolver and dvr::PartitionedMaxMinSolver compute identical vertex perturbations.
The former is much more efficient, with the difference in run times becoming more apparent when
optimizing vertex locations in tetrahedral meshes, where the number of elements in the 1-ring of a vertex
can be large (e.g., 30 on average in meshes created with commonly used mesh generators). Numerical experiments
consistently indicate that dvr::ReconstructiveMaxMinSolver is approximately 200X faster than dvr::PartitionedMaxMinSolver
when optimizing tet meshes. A comparison of the
operation counts for the two solvers is provided in the section on \ref sec_perf_4.

Despite its poorer efficiency, dvr::PartitionedMaxMinSolver is provided because it can be used with a
more general class of element quality metrics compared to dvr::ReconstructiveMaxMinSolver.
For successful execution, dvr::PartitionedMaxMinSolver only requires the element quality metric to be a sufficiently
smooth function of the vertex coordinates, while the dvr::ReconstructiveMaxMinSolver requires that the mapping \f$\lambda\mapsto
{\rm Q}({\rm K}^\lambda)\f$ be quasi-concave. dvr::PartitionedMaxMinSolver will 
help introduce and test new quality metrics in the library, including ones that may not satisfy the assumptions underlying
the algorithm implemented by dvr::ReconstructiveMaxMinSolver. See the documentation pages of these classes for more details.

### Constrained directional vertex relaxation{#sec_lib_5}
DVRlib provides the thread-safe class dvr::SamplingMaxMinSolver to compute suboptimal vertex perturbations
by sampling vertex locations along a prescribed perturbation direction, while possibly imposing constraints on vertex locations. 
The solver simply restricts the search for the perturbation coordinate \f$\lambda\f$ to a finite number of candidates.
It does not rely on any property of the quality metric for successful execution or termination.

To relax a vertex \f$i\f$ located at \f${\bf x}\f$, the solver
generates a uniformly spaced discrete set of sample points \f$\Lambda\f$
contained in the interval \f$[-h,h]\f$, where \f$h\f$ is the local mesh size.
The number of sample points is user-specified.
The solver determines the (sub)optimal perturbation to relax vertex \f$i\f$ as 
\f[ \lambda^{\rm opt} = \arg \max_{\lambda\in \Lambda} \min\{{\rm Q}(\tilde{K}^\lambda)\,:\,K\in \stackrel{\star}{e}(i;{\cal T}) \} \f]
where \f$\tilde{K}^\lambda\f$ is the element resulting from perturbing its vertex \f$i\f$
from \f${\bf x}\f$ to \f$\pi({\bf x}+\lambda{\bf d})\f$, \f${\bf d}\f$ is the prescribed relaxation direction,
and \f$\pi\f$ is a given perturbation map, possibly to impose constraints on the location of the vertex. If not specified,
\f$\pi\f$ is assumed to be the identity map, in which case, the solver computes a suboptimal solution to the unconstrained problem.

The expected use case for the solver is to constrain nodes to lie on a curvilinear boundary/manifold during relaxation.
The helper struct dvr::TangentialDirGenerator generates relaxation directions that tangential to the constraint manifold,
while dvr::ClosestPointStruct restricts candidate vertex locations to the constraint manifold by
defining \f$\pi\f$ to the closest point projection map. Both dvr::TangentialDirGenerator and dvr::ClosestPointStruct
are thread-safe, and require an oracle that computes the signed distance function to the constraint manifold.
\ref dvr-constraint.cpp provides a tutorial-style example demonstrating the usage of dvr::SamplingMaxMinSolver,
dvr::TangentialDirGenerator and dvr::ClosestPointStruct to relax vertices that are constrained to lie on a circular boundary.

### Relaxation direction generators{#sec_lib_4}
 DVRlib provides thread-safe helper structs dvr::CartesianDirGenerator and dvr::RandomDirGenerator
 for relaxing vertices along Cartesian and randomly-generated directions, respectively.
 The library imposes no restrictions on the choice of relaxation directions,
 which can be vertex, position and iteration-dependent.
 The rudimentary struct dvr::RelaxationDirGenerator helps pass function pointers implementing user-defined relaxation directions
 to the mesh optimization routines.


### Vertex coloring{#sec_lib_6}

For lock-free thread-parallel implementation of the dvr algorithm, the library provides
a simple vertex coloring algorithm implemented by the class dvr::VertexColoring.
The class implements a greedy algorithm
to partition vertices in \f${\rm I}_{\rm R}\f$ into subsets which can be optimized concurrently.
The coloring computed by the class is not expected to be optimal--
it may require more colors/partitions than necessary
(although this number is bounded),
and the number of vertices in each partition can have large variability.
The latter can affect scalability of the parallel optimization routine.

### Wrappers for polynomial root finding{#sec_lib_7}
For the mean ratio metric \f${\rm Q}\f$, the element quality functions \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$
are rational functions. In the case of planar triangles, \f${\rm Q}(K^\lambda)\f$ is of the form
\f$f(\lambda)/g(\lambda)\f$, where \f$f\f$ is a affine and \f$g\f$ is quadratic in \f$\lambda\f$. Similarly, in the
case of tetrahedra, \f${\rm Q}(K^\lambda)\f$ is of the form \f$f(\lambda)/g^{3/2}(\lambda)\f$, where \f$f\f$ and \f$g\f$
are affine and quadratic functions, respectively.
Consequently, computing level sets of the functions \f$\lambda\mapsto {\rm Q}(K^\lambda)\f$,
their extrema, and their pairwise intersections reduce to problems of computing roots of univariate polynomials.
For this purpose, DVRlib interfaces with <a href="https://www.gnu.org/software/gsl/">GNU GSL</a>
for polynomial solving through the wrapper class dvr::GNUPolySolver.
The library also provides utilities to check for the existence of roots based on a simple des Cartes
rule through the class dvr::DesCartesPolyRootCounter.
Instances of these classes are used internally by dvr::GeomTri2DQuality and dvr::GeomTet3DQuality.

## Mesh data structure {#sec_meshes}

### Flexibility through templated class
DVRlib provides flexibility in the choice of the mesh data structure.
Recognizing the rich variety of data structures used for representing
simplicial meshes, *the mesh data structure is retained as a type template parameter throughout the library*.
Assumptions on the class methods are detailed below.
For the purpose of testing and providing examples, the dvr::utils::SimpleMesh class is provided in the <tt>$DVR/test</tt> folder.

### Assumed methods

To provide flexibility in the choice of mesh data structures, 
all classes and routines requiring access to the mesh are templated by the mesh type.


The following public methods are assumed to be provided by the mesh class:
 \code{.cpp}

 // Method providing access to nodal coordinates
 // Input: node_index Index of vertex.
 // Return: pointer to coordinates of "node_index"
 const double* coordinates(const int& node_index) const;

 // Method to update nodal coordinates
 // Input: node_index Index of vertex.
 // Input: X Pointer to new coordinates of "node_index"
 // Expected functionality: overwrite copy coordinates of node_index from X.
 void update(const int node_index, const double* X);

 // Method providing access to element connectivities
 // Input: elm_index Index to element
 // Return: Pointer to the connectivity of element "elm_index"
 const int* connectivity(const int elm_index) const;

 // Method to return the spatial dimension in which the mesh is embedded
 // Return value: 2 for a planar triangle and 3 for a tet
 int spatial_dimension() const;

 // Method to return the number of elements
 int n_elements() const;

 // Method to return the number of nodes
 int n_nodes() const;
 
 \endcode


It is expected that commonly used mesh data structures can be easily adapted/extended to provide these methods.
<br>
Since all access to vertex coordinates and element connectivities are through a user-defined mesh data structure:
- The library makes no assumptions about vertex and element numbering, besides these being integer-valued. 
- Vertex and element numberings need not be sequential. They need not begin with 0 or 1. 
- Only the `update()` method can alter the mesh, by overwriting vertex coordinates. 
- DVRlib invokes the `coordinates()` and `update()` methods to access and overwrite vertex coordinates in a thread-safe manner. These methods need not be thread-safe themselves.
- The `coordinates()` and `connectivity()` methods are invoked during each vertex optimization call.
   Efficient implementations of these methods will directly help reduce execution times.

These assumptions on the mesh class are checked by the dvr::CheckMeshTraits function that it templated by the mesh type. For instance, a *compile time check* of the form
```
static_assert(dvr::CheckMeshTraits<M>());
```
is recommended to ensure that `M` satisfies the assumptions on the mesh data type.

**Warning:** The traits checks on the mesh type is implemented using the Boost TTI library and does not correctly test for inherited methods.


### The dvr::utils::SimpleMesh class{#sec_simple_mesh}
A simple class with the methods listed above, called dvr::utils::SimpleMesh, is provided in the folder
<tt>$DVRDIR/test</tt> under the dvr::utils namespace.
It is used in all the tutorials and unit tests provided with the source code and can be used for quick prototyping.

In addition to the assumed methods detailed above, a few  utility methods are also included:
- a constructor dvr::utils::SimpleMesh::SimpleMesh is provided to populate an instance by reading from a file in Tecplot format.
   The method can be easily extended to read from files in alternative formats (e.g., OFF, PLY).
- dvr::utils::SimpleMesh::save to write the mesh stored by an instance of the class to a file in Tecplot format.
      The method can be extended in a straightforward manner to write to files in alternative formats (e.g., OFF, PLY).
- dvr::utils::SimpleMesh::boundary_nodes to identify the set of vertices lying on the boundary of the mesh.
      This method is used in tests and tutorials to identify the set of nodes to relax. 
- dvr::utils::SimpleMesh::interior_nodes to identify the set of vertices *not* lying on the boundary of the mesh.
      This method is used in tests and tutorials to identify the set of nodes to relax. 


**Disclaimer**: dvr::utils::SimpleMesh is provided solely for the purpose of including tests and tutorials in the library.
While useful for prototyping, it may not be optimal for HPC codes.


### Meshes for testing

Sample triangle and tetrahedral meshes are provided in the <tt>test/</tt> and <tt>tutorial/</tt> folders.
A small subset of these meshes are used by the unit tests and tutorial examples.

Meshes to reproduce the benchmark examples (see \ref performance) are available
[here](https://rram.bitbucket.io/dvr-meshes/index.html). Some of these meshes can be used to reproduce the results from \cite rangarajan2017provably.

All meshes are provided in Tecplot format and can be previewed using [Paraview](https://www.paraview.org/).


## Optimization routines{#sec_inputs}

DVRlib provides three routines for vertex-- and mesh--level optimization:
- `dvr::Optimize(const int vert_index, double* rdir, const MMSType& MMSolver, void* solparams)`
  <br> which is templated by the type of the max-min solver (<tt>MMSType</tt>),
  computes and returns the value of the optimal perturbation \f$\lambda^{\rm opt}\f$ for relaxing a vertex with index
  <tt>vert_index</tt> along the prescribed direction <tt>rdir</tt>, using the provided max-min solver <tt>MMSolver</tt>.
  The routine does \b not update the coordinates of the vertex. 
  It provides flexibility in:
  - the choice of vertices to relax 
  - the order in which to relax vertices
  - the directions along which to relax vertices 
  - control of whether or not to update the location of a vertex
  - the possibility of intermediate operations before updating the location of the vertex, and 
  - control over the number of relaxation iterations to perform. 

    The routine is well-suited for applications in which detailed information can be leveraged to guide mesh improvement.

- `dvr::Optimize(const std::vector<int>& Ir, MeshType& MD, const MMSType& MMSolver, RelaxationDirGenerator& rdir_gen, void* solparams)`
     <br> which is templated by the type of the max-min solver (<tt>MMSType</tt>) and the mesh type (<tt>MeshType</tt>).
     It sequentially computes and updates locations of vertices in the list <tt>Ir</tt> by optimally perturbing them
     along directions specified through the direction generator <tt>rdir_gen</tt>.
     The implementation of the routine consists in:
	- looping over vertices in <tt>Ir</tt>, 
   	- generating a relaxation direction for each vertex <tt>vert</tt> in <tt>Ir</tt>
    - computing the optimal perturbation \f$\lambda^{\rm opt}\f$ for <tt>vert</tt> along the generated direction, and 
   	- updating the location of <tt>vert</tt> in the mesh. 

	\snippet dvr_MeshOptimizer_Impl.h doc_seq_optimize_mesh


- `dvr::Optimize(const std::vector<int>& Ir, MeshType& MD, const MMSType& MMSolver,  RelaxationDirGenerator& rdir_gen, const VertexColoring<MeshType>& vshades, void* solparams)`
     <br> which is templated by the type of the max-min solver (<tt>MMSType</tt>) and the mesh type (<tt>MeshType</tt>).
     It provides an implementation of the dvr algorithm for parallel execution on shared memory architectures using OpenMP directives.
     <br>
     The implementation avoids the use of locks to prevent data races possible when updating and accessing vertex coordinates,
     by using a partitioning of the list <tt>Ir</tt> (of vertices to be relaxed) using a vertex coloring algorithm.
     The class dvr::VertexColoring provided by the library implements a simple sequential greedy coloring algorithm for this purpose. The routine
     accesses the partitioning of <tt>Ir</tt> through an instance <tt>vshades</tt> of the dvr::VertexColoring class.

	\image html "imgs/dvr-img7.png" "Figure 7" width=600px"

	Figure 7 illustrates the idea behind the parallelization using an example of a triangle mesh.
	The example assumes that all interior vertices are to be relaxed. The object <tt>vshades</tt> 
	contains a partitioning of the list of the interior vertices into four subsets.
	The figure depicts the partitioning using colors--- vertices shaded in the same color belong to the same partition.
	<br>
	The dvr::VertexColoring::GetPartitions method provides access to the partitioning through a two-dimensional (integer) vector, say <tt>VertColors</tt>. 
	The number of colors/partitions is given by <tt>VertColors.size()</tt>, which in the example shown, equals four.
	<br>
	For <tt>0</tt>\f$\leq\f$ <tt>c</tt> \f$<\f$ <tt>VertColors.size()</tt>, the vector <tt>VertColors[c]</tt> equals the subset of <tt>Ir</tt> 
	that is assigned the color/partition number <tt>c</tt>. 
	<br>
	The partitioning satisfies the property that if <tt>i,j</tt> are distinct vertices in <tt>Ir</tt>, and
	if <tt>j</tt> belongs to the 1-ring of <tt>i</tt>, then <tt>i</tt> and <tt>j</tt> are assigned different colors.
	This property is evident in the example as well--- notice that no two adjacent vertices (i.e., joined by an edge in the mesh) are assigned the same color. 
	<br>
	Such a partitioning of vertices is useful in parallelizing the dvr algorithm because vertices belonging to the same partition (or of the same color)
	can be relaxed concurrently without the possibility of a data race. In the example shown, the location of a red vertex can be updated/optimized
 	without accessing the coordinates of another red vertex. Hence, all red vertices can be perturbed concurrently over multiple threads.
	Similarly, all the yellow vertices, and so on.

	The code snippet shown below from dvr_MeshOptimizer_Impl.h shows the realization of the partition-wise concurrent vertex relaxation model implemented in the optimization routine.

	\snippet dvr_MeshOptimizer_Impl.h doc_omp_algorithm

	The routine assumes that the max-min solver class is thread-safe, which in turn, assumes that the class implementing element qualities is thread-safe.
	<br> The element quality classes dvr::GeomTri2DQuality and dvr::GeomTet3DQuality, and the max-min solver classes
	dvr::PartitionedMaxMinSolver, dvr::ReconstructiveMaxMinSolver and dvr::SamplingMaxMinSolver provided in the library are all thread-safe.
	They each create as many copies of local data structures during construction as the number of threads,
	which incurs a small increase in memory usage.

	The order in which vertices of a specific partition are relaxed during parallel execution is
	non deterministic. This feature therefore, introduces variability in the sequence of meshes computed.
	In particular, relaxing the same mesh using a different number of threads, can, in general, result in
	different optimized meshes. The guarantees of mesh improvement provided by dvr hold nevertheless.

	For good parallel scaling, experiments suggest that adopting a dynamic schedule when assigning vertices (of the same color)
	to threads is preferable to a static schedule.
	This is due to the fact that the expense of computing perturbations
	varies from vertex to vertex. In general, computing perturbations for
	a vertex having a larger number of elements in its 1-ring is more expensive.


The optional parameter <tt>solparams</tt> appearing in all three optimization routines is forwarded to the max-min solver.
While dvr::PartitionedMaxMinSolver and dvr::ReconstructiveMaxMinSolver do not use them, the dvr::SamplingMaxMinSolver class uses it to
impose constraints on vertex perturbations through an instance of dvr::PointPerturbationStruct, if provided. 


## Mesh quality in DVR {#sec_quality}

To use DVRlib effectively, it is important to understand the sense in which the  dvr algorithm
improves mesh qualities. While the quality of a triangle or a tetrahedron \f$K\f$ is defined by the
scalar-valued quality metric \f${\rm Q}\f$, for a given choice of the list \f${\rm I}_{\rm R}=(i_1,\ldots,i_n)\f$ of vertices being relaxed,
the quality of the mesh \f${\cal T}\f$ is defined to be vector-valued:
\f[ {\bf Q}({\cal T}) = {\bf asc}\left(q_{i_1}({\cal T}),q_{i_2}({\cal T}),\ldots,q_{i_n}({\cal T})\right), \f]
where \f$q_{i}({\cal T}) = \min\{{\rm Q}(K)\,:\, K \in \stackrel{\star}{e}(i;{\cal T}) \}\f$
is the minimum among the qualities of triangles in the 1-ring of vertex \f$i\f$ in the mesh \f${\cal T}\f$, 
and \f${\bf asc}\f$ is an ascending sort operator.
In essence, \f${\bf Q}({\cal T})\f$ is obtained by sorting the list containing the
poorest element qualities incident at each vertex in \f${\rm I}_{\rm R}\f$.

\image html imgs/dvr-img4.jpg "Figure 8." width=900px

It is instructive to examine the definition of the mesh quality using an example.
Figure 8 shows a triangle mesh \f${\cal T}\f$ in which the set of relaxed vertices is chosen to be \f${\rm I}_{\rm R}=(1,2,3,4)\f$.
For each \f$i\in {\rm I}_{\rm R}\f$, the figure shows the set of elements \f$\stackrel{\star}{e}(i;{\cal T})\f$ belonging to the 1-ring of \f$i\f$.
For instance, \f$\stackrel{\star}{e}(3;{\cal T})=\{K_1,K_2,K_8,K_9\}\f$. For each \f$i\in {\rm I}_{\rm R}\f$, the figure also highlights
the triangle in \f$\stackrel{\star}{e}(i;{\cal T})\f$ having the poorest quality. For \f$i=1,2,3\f$ and \f$4\f$, these triangles are
\f$K_1,K_1,K_1\f$ and \f$K_{12}\f$, respectively. Hence,
\f[\left(q_{1}({\cal T}),q_{2}({\cal T}),q_{3}({\cal T}),q_{4}({\cal T})\right) = \left(Q(K_1),Q(K_1),Q(K_1),Q(K_{12})\right).\f]
Assuming that \f$Q(K_1)\leq Q(K_{12})\f$, the quality of \f${\cal T}\f$ is defined to be the vector
\f[ {\bf Q}({\cal T}) = {\bf asc}(Q(K_1),Q(K_1),Q(K_1),Q(K_{12})) = (Q(K_{12}),Q(K_1),Q(K_1),Q(K_{1})).\f]

In general:
- \f${\bf Q}({\cal T})\f$ is a vector of length equal to that of \f${\rm I}_{\rm R}\f$, with components arranged in ascending order. 
- The first element of \f${\bf Q}({\cal T})\f$ represents the poorest quality among all triangles/tetrahedra that are considered
 	for improvement. Hence, \f${\rm Q}_1({\cal T})\f$ is of direct significance in scientific computing (e.g., choice of time
	step, conditioning number of a system of equations).
- It is possible that the quality of the same triangle/tetrahedron appears multiple times in the mesh quality vector \f${\bf Q}({\cal T})\f$.
     Indeed, the definition of \f${\bf Q}({\cal T})\f$ naturally accentuates the significance of elements with poor qualities during mesh improvement.
- The quality vector \f${\bf Q}({\cal T})\f$ is different from the (sorted) list of qualities of all elements in the mesh \f${\cal T}\f$.

A natural ordering of quality vectors of meshes that differ from each other only in the locations of
vertices in \f${\rm I}_{\rm R}\f$ follows as:
\f[ {\bf Q}({\cal T}')>{\bf Q}({\cal T})~\text{if there exists }m \leq \#{\rm I}_{\rm R}~\text{such that }
{\rm Q}_j({\cal T}')>{\rm Q}_j({\cal T})~\text{for}~1\leq j\leq m.\f]
Since  entries in the mesh quality vector are arranged in ascending order, the ordering relation
above implies that the smallest \f$m\f$ qualities appearing in \f${\bf Q}({\cal T}')\f$ are respectively 
larger than the smallest \f$m\f$ qualities in \f${\bf Q}({\cal T})\f$.
In this sense, the mesh \f${\cal T}'\f$ is of better quality than the mesh \f${\cal T}\f$.

> <em>DVR guarantees that the mesh quality vector is non decreasing with each vertex perturbation during each iteration, irrespective of the choice of the vertex list \f${\rm I}_{\rm R}\f$, the relative ordering of vertices in \f${\rm I}_{\rm R}\f$, and the choice of relaxation directions. In particular, denoting the mesh realized at the end of the \f$k^{\rm th}\f$ dvr iteration to improve a given triangulation \f${\cal T}\f$ by \f${\cal T}_k\f$, it is guaranteed that \f[{\bf Q}({\cal T})\leq {\bf Q}({\cal T}_1) \leq {\bf Q}({\cal T}_2) \leq \ldots \f] </em>


\image html imgs/dvr-img5.png "Figure 9." width=500px

Figure 9 shows an example to help visualize the improvement in quality of a tetrahedral mesh realized using dvr.
The figure shows a plot of the component \f${\rm Q}_i\f$ versus the index \f$i\f$, where \f$1\leq i\leq \#{\rm I}_{\rm R}=6849\f$.
The components of the quality vector for the input mesh \f${\cal T}\f$ are shown in red, while that for the mesh \f${\cal T}^{\rm opt}\f$
computed at the end of \f$25\f$ dvr iterations
are shown in blue. Notice that both curves are non decreasing, owing to the fact that components of mesh quality vectors
are sorted in ascending order.
The plot helps to understand why \f${\bf Q}({\cal T}^{\rm opt})>{\bf Q}({\cal T})\f$.
From index \f$i=1\f$ to about \f$5000\f$ (where the two curves
intersect for the first time), \f${\rm Q}_i({\cal T}^{\rm opt})>{\rm Q}_i({\cal T})\f$. 
The ordering \f${\bf Q}({\cal T}^{\rm opt})>{\bf Q}({\cal T})\f$ implies that
the blue curve lies above the red one until the two intersect for the first time.
Hence, the poorest element qualities
realized at (approximately) \f$5000\f$ of the \f$6849\f$ vertices in \f${\rm I}_{\rm R}\f$
in the optimized mesh \f${\cal T}^{\rm opt}\f$ are better than
in the input mesh.
In numerical computations using unstructured meshes, elements with poorer qualities are a greater concern.
In fact, it is prudent to improve the qualities of poorly shaped elements,
even if at the expense of a few well-shaped ones--- this is precisely what dvr achieves.

